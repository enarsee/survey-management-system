﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;

namespace SofaDev
{
    public partial class Register : System.Web.UI.Page
    {
       AccountController ac1;
       //AccountController ac1;
       protected override void OnPreInit(EventArgs e)
       {
           ac1 = (AccountController)Session["account"];
           if (ac1 == null) // Switches to public masterpage
               this.Page.MasterPageFile = "~/Public.master";
           else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
               this.Page.MasterPageFile = "~/Admin.master";
           else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
               this.Page.MasterPageFile = "~/Creator.master";
           else
               this.Page.MasterPageFile = "~/Public.master";
           base.OnPreInit(e);
       }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            ac1 = new AccountController();

            // Check whether email exists in database
            if (Account.GetAccountID(TextBox1.Text) == -1)
            {
                // Call controller and pass in email and password
                if (ac1.CreateAccount(TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox5.Text, male.Checked ? "Male" : "Female", TextBox6.Text, TextBox7.Text, "1") == 1)
                {
                    Session["account"] = ac1;
                    Session["email"] = TextBox1.Text;
                    Response.Redirect("ViewAllSurvey.aspx");
                }
                else
                    Response.Redirect("Default.aspx");
            }
            else
                Label2.Text = "Email already exists!";
        }
    }
}