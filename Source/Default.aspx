﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SofaDev.Default"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <% String error = Request.QueryString["error"];
      switch (error)
      {
          case "0": Response.Write("<div id='error'>Wrong Username/Password</div><script type='text/javascript'>$('#error').fadeOut(4000);</script>");
              break;
          case "1": Response.Write("<div id='error'>Access Denied ! Please Login</div><script type='text/javascript'>$('#error').fadeOut(4000);</script>");
              break;
          
      }    
       
          %>
   <!-- <div id="content"> -->
		<div id="leftblock">
			<h1>Login</h1>
			
				<div id="loginbox">
                    <asp:TextBox ID="TextBox1" runat="server" placeholder="E-mail"/>
				<br />
					<asp:TextBox ID="TextBox2" runat="server" placeholder="Password" TextMode="Password"/><br /><br />
                    <asp:Button ID="Button3" runat="server" Text="Login" OnClick="Button3_Click" style="margin:0px auto 20px;" />
					
				    <asp:Button ID="Button4" runat="server" Text="Forgot Password" style="margin:auto;" OnClick="Button4_Click" />
					<br /><br />
                    <asp:HyperLink runat="server" Text="Log in with Facebook" id="fbLogin" >
                    <img src="Images/facebook_login.png" /></asp:HyperLink>
				</div>
			
		</div>

		<div id="rightblock">
			<h1>Register</h1>
			
				<div id="registerbox">
					<asp:TextBox ID="TextBox3" runat="server" placeholder="E-mail"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox3" ErrorMessage="Enter Email" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox3" ErrorMessage="Please enter a valid email" Font-Size="Smaller" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <br />
                    <asp:TextBox ID="TextBox4" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox4" ErrorMessage="Enter Password" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox ID="TextBox9" runat="server" TextMode="Password" placeholder="Confirm Password"></asp:TextBox>
                    <br />
                    
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox4" ControlToValidate="TextBox9" ErrorMessage="Passwords do not match" Font-Size="Smaller" ForeColor="Red"></asp:CompareValidator>
                    
                    <br />
                    <asp:TextBox ID="TextBox5" runat="server" placeholder="Name"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox5" ErrorMessage="Enter Name" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <br />
                    <asp:RadioButton id="male" Text="Male" Checked="True" GroupName="gender" runat="server"/>
                    <asp:RadioButton id="female" Text="Female" GroupName="gender" runat="server"/>
                    <br />
                    <br />
                    <asp:TextBox ID="TextBox6" runat="server" placeholder="Age"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox6" ErrorMessage="Enter Age" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Font-Size="Smaller" ControlToValidate="TextBox6" ErrorMessage="Enter numbers only" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                    <br />
                    <asp:TextBox ID="TextBox7" runat="server" placeholder="Address"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox7" ErrorMessage="Enter Address" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox ID="TextBox8" runat="server" placeholder="Contact"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox8" ErrorMessage="Enter Contact" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Font-Size="Smaller" ControlToValidate="TextBox8" ErrorMessage="Enter numbers only" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                    <br />
                    <asp:Label ID="Label2" runat="server" Font-Size="Smaller" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click" style="margin:auto;" />
				</div>
			
		</div>
		<div class="clearfix"></div>
	
</asp:Content>
