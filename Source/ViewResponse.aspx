﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="ViewResponse.aspx.cs" Inherits="SofaDev.ViewResponse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:LinkButton ID="editButton" cssclass="link-button" runat="server" OnClick="editButton_Click">Edit this response</asp:LinkButton>
&nbsp;&nbsp;
    <asp:LinkButton ID="deleteButton" cssclass="link-button" runat="server" OnClick="deleteButton_Click">Delete this response</asp:LinkButton>
    &nbsp;
    
    <br />
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    <asp:LinkButton ID="ViewAllResponse" cssclass="link-button caps" runat="server" OnClick="ViewAllResponse_Click">Back</asp:LinkButton>
    <div class="clearfix"></div>
</asp:Content>
