﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;

namespace SofaDev
{
    public partial class ViewUser : System.Web.UI.Page
    {
        AccountController acc1;
        Account ac1;
        AccountController ac2;

        protected override void OnPreInit(EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            acc1 = (AccountController)Session["account"];
            int userID = int.Parse(Request.QueryString["id"]);

            ac1 = acc1.AdminGetAccount(userID);
            if (acc1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (acc1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (acc1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            DeleteUser.CausesValidation = false;
            Button2.CausesValidation = false;
            acc1 = (AccountController)Session["account"];
            
            if (!IsPostBack)
            {
                TextBox1.Text = ac1.Age.ToString();
                TextBox2.Text = ac1.Contact;
                //TextBox3.Text = ac1.Gender.ToString();
                RadioButtonList1.Items.Insert(0, "Male");
                RadioButtonList1.Items.Insert(1, "Female");
                if (ac1.Gender.ToString() == "Male")
                {
                    RadioButtonList1.SelectedIndex = 0;
                    RadioButtonList1.Items[0].Selected = true;
                }
                else if (ac1.Gender.ToString() == "Female")
                {
                    RadioButtonList1.SelectedIndex = 1;
                    RadioButtonList1.Items[1].Selected = true;
                }
                TextBox4.Text = ac1.Address;
                TextBox5.Text = ac1.Name;
                TextBox6.Text = ac1.Email;
            }

        }
        protected void DeleteUser_Click(object sender, EventArgs e)
        {
            if (acc1.AdminDeleteAccount(ac1.AId) == 1)
                Response.Redirect("Useradmin.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox5.Text != "")
            {
                acc1.AdminEditAccount(ac1.AId, "Name", TextBox5.Text);
            }
            if (TextBox4.Text != "")
            {
                acc1.AdminEditAccount(ac1.AId, "Address", TextBox4.Text);
            }
            if (RadioButtonList1.SelectedItem.Text == "Male") {
                acc1.AdminEditAccount(ac1.AId, "Gender", "Male");
            }
            else if (RadioButtonList1.SelectedItem.Text == "Female")
            {
                acc1.AdminEditAccount(ac1.AId, "Gender", "Female");
            }
            if (TextBox2.Text != "")
            {
                acc1.AdminEditAccount(ac1.AId, "Contact", TextBox2.Text);
            }
            if (TextBox1.Text != "")
            {
                acc1.AdminEditAccount(ac1.AId, "Age", TextBox1.Text);
            }
            if (TextBox6.Text != "")
            {
                acc1.AdminEditAccount(ac1.AId, "Password", TextBox6.Text);
            }

            //if (TextBox5.Text != "")
            //    ac1.Name = TextBox5.Text;// ac1.UpdateAccount("name", TextBox5.Text);
            //if (TextBox4.Text != "")
            //    ac1.Address = TextBox4.Text;
            //if (RadioButtonList1.SelectedItem.Text == "Male")
            //    ac1.Gender = (GenderType.Male);
            //// acc1.UpdateAccount("gender", "Male");
            //else if (RadioButtonList1.SelectedItem.Text == "Female")
            //    ac1.Gender = (GenderType.Female);
            //    //acc1.UpdateAccount("gender", "Female");
            //if (TextBox2.Text != "")
            //    ac1.Contact = TextBox2.Text;
            //    //acc1.UpdateAccount("contact", TextBox2.Text);
            //if (TextBox1.Text != "")
            //    ac1.Age = Convert.ToInt16(TextBox1.Text);
            //   // acc1.UpdateAccount("age", TextBox1.Text);
            
            //   // acc1.UpdateAccount("email", TextBox6.Text);
            //if (TextBox6.Text != ac1.Password && TextBox6.Text != "")
            //    ac1.Password=Account.MakeHashString(TextBox6.Text);
            //ac1.UpdateAccount();
        }
    }
}