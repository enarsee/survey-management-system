using SofaDev.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SofaDev {
    public partial class ViewAllSurvey : System.Web.UI.Page {

        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Load(object sender, EventArgs e) {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            SurveyController surCon = new SurveyController();
            
            if (ac1.getAccType() == AccountType.Admin)
            {
                GridView1.DataSource = surCon.ViewSurvey();
                GridView1.DataBind();
            }
            else if(ac1.getAccType() == AccountType.Creator)
            {
                GridView1.DataSource = surCon.ViewSurveyForUser(ac1.getAccountID());
                GridView1.DataBind();
            }

        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateSurveyDetails.aspx");
        }
    }
}