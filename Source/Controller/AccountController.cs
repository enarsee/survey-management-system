﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SofaDev.Class;
using System.Runtime;
using SofaDev;
using System.Data;

namespace SofaDev.Controller
{
    /// <summary>
    /// Connects Account Entity to View
    /// </summary>
    public class AccountController
    {
        private Account acc1;
        private SofaDev.Class.Admin admin1;

        public AccountController() {
            acc1 = null;
            admin1 = null;
        }

        /// <summary>
        /// Login using facebook
        /// </summary>
        /// <param name="inUId"></param>
        /// <param name="inAccessToken"></param>
        /// <returns></returns>
        public int FacebookLogin(FacebookUser fb)
        {
            string userID = fb.id + "";
            int result = Account.AuthenticateFB(userID);
            if (result == 1)
            {
                if (Account.IsAdmin(userID))
                    admin1 = new SofaDev.Class.Admin(fb.id+"");
                else
                    acc1 = new Account(fb.id+"");
                System.Diagnostics.Debug.WriteLine("Name: " + acc1.Name + "\nPassword: " + acc1.Password + "\nUsername: " + acc1.FBUserName + "\nFBID: " + acc1.FBId + "\n");
            }
            else
            {
                acc1 = new Account();
                acc1.CreateFacebookAccount(fb.first_name + " " + fb.last_name, fb.name, fb.email, userID);
                System.Diagnostics.Debug.WriteLine("Last Name: " + fb.last_name + "\nFirst Name: " + fb.first_name + "\nPassword: " + acc1.Password + "\nUsername: " + acc1.FBUserName + "\nFBID: " + acc1.FBId + "\n");
            }
            return result;
        }
       
        /// <summary>
        /// Create a new account
        /// </summary>
        /// <param name="inEmail"></param>
        /// <param name="inPassword"></param>
        /// <param name="inName"></param>
        /// <param name="inAge"></param>
        /// <param name="inGender"></param>
        /// <param name="inAddress"></param>
        /// <param name="inContact"></param>
        /// <param name="inAccType"></param>
        /// <returns></returns>
        public int CreateAccount(string inEmail, string inPassword, 
            string inName, string inAge, string inGender, string inAddress, string inContact, string inAccType)
        {
            int result;     
            int iAge = 0;
            AccountType iAcc = AccountType.Nil;
            GenderType iGender = GenderType.Nil;
            
            try {
	            iAge = int.Parse(inAge);
                inAccType = inAccType.ToLower();
                inGender = inGender.ToLower();
                switch (inAccType)
                {
                    case "admin":
                        iAcc = AccountType.Admin;
                        admin1 = new SofaDev.Class.Admin();
                        break;
                    default:
                        iAcc = AccountType.Creator;
                        acc1 = new Account();
                        break;
                }
                switch (inGender)
                {
                    case "male":
                        iGender = GenderType.Male;
                        break;
                    default:
                        iGender = GenderType.Female;
                        break;
                }
	        }
	        catch (Exception e) {
                System.Diagnostics.Debug.WriteLine(e.Message);
	        }
            //acc1 = new Account();
            result = acc1.CreateAccount(inEmail, inPassword, inName, iAge, iGender, inAddress, inContact, iAcc);
            return result;
        }

        /// <summary>
        /// Logs in the user with the username and password.
        /// </summary>
        /// <param name="inEmail"></param>
        /// <param name="inPassword"></param>
        /// <returns></returns>
        public int Login(string inEmail, string inPassword)
        {
            int result = Account.Authenticate(inEmail, inPassword);
            if (result == 1)
            {
                System.Diagnostics.Debug.WriteLine("Logging in " + inEmail);
                if (Account.IsAdmin(inEmail))
                    admin1 = new SofaDev.Class.Admin(inEmail, inPassword);
                else
                    acc1 = new Account(inEmail, inPassword);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Login failed for " + inEmail + ", " + inPassword);
            }
            return result;
        }

        /// <summary>
        /// Removes the account from database
        /// </summary>
        /// <returns>0 if account not available, 1 if available</returns>
        public int DeleteAccount()
        {
            if (acc1 == null)
            {
                if (admin1 != null)
                {
                    admin1.DeleteAccount();
                    return 1;
                }
                return 0;
            }
            int result = acc1.DeleteAccount();
            acc1 = null;
            return result;
        }

        /// <summary>
        /// Updates a specific value of the account info.
        /// </summary>
        /// <param name="inType">Value type. Name, password, etc.</param>
        /// <param name="inValue">Value to be changed</param>
        /// <returns>1 if success, 0 if unsuccessful</returns>
        public int UpdateAccount(string inType, string inValue)
        {
            if (inValue == null) return 0;

            int result = 0;
            DBDataType iType = DBDataType.Nil;
            inType = inType.ToLower();
            switch (inType)
            {
                case "name":
                    iType = DBDataType.Name;
                    break;
                case "address":
                    iType = DBDataType.Address;
                    break;
                case "email":
                    iType = DBDataType.Email;
                    break;
                case "gender":
                    iType = DBDataType.Gender;
                    break;
                case "password":
                    iType = DBDataType.Password;
                    break;
                case "contact":
                    iType = DBDataType.Contact;
                    break;
                case "age":
                    iType = DBDataType.Age;
                    break;
                case "type":
                    iType = DBDataType.AccType;
                    break;
            }
            if (acc1 != null)
                result = acc1.UpdateAccount(iType, inValue);
            else
                result = admin1.UpdateAccount(iType, inValue);
            return result;
        }

        /// <summary>
        /// Update all fields of current account.
        /// </summary>
        /// <returns></returns>
        public int UpdateAccount()
        {
            if (acc1 == null) return admin1.UpdateAccount();
            return acc1.UpdateAccount();
        }
        
        /// <summary>
        /// Sends an email for password reset.
        /// </summary>
        /// <returns>0 if fail, 1 if success.</returns>
        public int ResetPassword(string inEmail) {
            return Account.SendPWEmail(inEmail);
        }

        /// <summary>
        /// Logs out the account.
        /// </summary>
        public void Logout()
        {
            acc1 = null;
            admin1 = null;
        }

        public List<Account> AdminGetAccountList()
        {
            //if (!acc1.IsAdmin()) return null;
            return admin1.AccountList;
        }

        public int AdminDeleteAccount(int accountID)
        {
            return admin1.DeleteAccount(accountID);
        }

        public Account AdminGetAccount(int accountID)
        {
            return admin1.GetAccount(accountID);
        }

        public int AdminEditAccount(int accountID, string inType, string inValue)
        {
            if (inValue == null) return 0;

            int result = 0;
            DBDataType iType = DBDataType.Nil;
            inType = inType.ToLower();
            switch (inType)
            {
                case "name":
                    iType = DBDataType.Name;
                    break;
                case "address":
                    iType = DBDataType.Address;
                    break;
                case "email":
                    iType = DBDataType.Email;
                    break;
                case "gender":
                    iType = DBDataType.Gender;
                    break;
                case "password":
                    iType = DBDataType.Password;
                    break;
                case "contact":
                    iType = DBDataType.Contact;
                    break;
                case "age":
                    iType = DBDataType.Age;
                    break;
                case "type":
                    iType = DBDataType.AccType;
                    break;
            }
            Account _tempAcc = AdminGetAccount(accountID);
            if (_tempAcc != null)
                result = _tempAcc.UpdateAccount(iType, inValue);
            return result;
        }

        public int getAccountID()
        {
            if (acc1 == null) return admin1.AId;
            return acc1.AId;
        }

        public string getEmail()
        {
            if (acc1 == null) return admin1.Email;
            return acc1.Email;
        }

        public string getPassword()
        {
            if (acc1 == null) return admin1.Password;
            return acc1.Password;
        }

        public string getName()
        {
            if (acc1 == null) return admin1.getUserInfo().Name;
            return acc1.getUserInfo().Name;
        }

        public int getAge()
        {
            if (acc1 == null) return admin1.getUserInfo().Age;
            return acc1.getUserInfo().Age;
        }

        public string getGender()
        {
            if (acc1 == null) return admin1.getUserInfo().Gender == GenderType.Male ? "Male" : "Female";
            return acc1.getUserInfo().Gender == GenderType.Male ? "Male" : "Female";
        }

        public string getAddress()
        {
            if (acc1 == null) return admin1.getUserInfo().Address;
            return acc1.getUserInfo().Address;
        }

        public string getContact()
        {
            if (acc1 == null) return admin1.getUserInfo().Contact;
            return acc1.getUserInfo().Contact;
        }

        public string getFBId()
        {
            if (acc1 == null) return admin1.FBId;
            return acc1.FBId;
        }

        public string getFBUserName()
        {
            if (acc1 == null) return admin1.FBUserName;
            return acc1.FBUserName;
        }

        public AccountType getAccType()
        {
            if (acc1 == null) return admin1.AccType;
            return acc1.AccType;
        }

        public DataTable viewAccounts() {
            
            Account ac1 = null;
            //AccountController acc1 = new AccountController();
			List<Account> accList = this.AdminGetAccountList();
            // Initialize DataTable
            DataTable dt = new DataTable();
            // Initialize DataColumn
            DataColumn dc = new DataColumn();
            // Initialize DataRow
            DataRow dr = null;

            // Add No. Column
            // Set AutoIncrement to No. Column
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;
            dc.AutoIncrementStep = 1;
            // Set Column Name
            dc.ColumnName = "#";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Set Column to be unique
            dc.Unique = true;
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add SurveyID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "User ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add StartDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Name";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add EndDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "E-mail";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Description Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Address";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);
			
			// Add Title Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Contact";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);
			
			// Add Title Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Gender";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
			
            dt.Columns.Add(dc);
			// Add Title Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Age";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

             // Add AccountID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Account Type";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc); 

           // accounts = (ArrayList)acc1.ViewUsers();

            //DateTime dtTemp;

            //Iterate through surveys
           for (int i = 0; i < (accList.Count); i++) {
                //ac1 = new Account();
				//ac1 = new Account();
                ac1 = accList[i];
                dr = dt.NewRow();
                // Set values into data row
                dr["User ID"] = ac1.AId;
                dr["E-mail"] = ac1.Email;
                dr["Name"] = ac1.Name;
                dr["Address"] = ac1.Address;
                dr["Contact"] = ac1.Contact;
                dr["Gender"] = ac1.Gender;
                dr["Age"] = ac1.Age;
                dr["Account Type"] = ac1.AccType;
                //dr["Account ID"] = sur.getAccountID();

                // Add new data row to data table
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}