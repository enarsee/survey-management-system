﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SofaDev.Class;

namespace SofaDev.Controller
{
    public class QuestionController
    {
        private Question question;
        private QuestionOption qOption;
        public QuestionController()
        {

        }
        public QuestionController(string title, int questionTypeID)
        {
            question = new Question(title, questionTypeID);
            qOption = new QuestionOption();
        }
        public int createQuestion()
        {
            question.createQuestion();
            return question.getQuestionID();
        }
        public void createQuestionOption(string Option)
        {
            qOption.setqOption(Option);
            qOption.setquestionID(question.getQuestionID());
            qOption.createOption();
        }
    }
}