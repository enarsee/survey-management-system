﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SofaDev.Controller
{
    public class LoginController
    {
        Account a1;

        public LoginController() { }

        // Connects to Account entity to login user
        public int login(String inEmail, String inPassword)
        {
            int result;
            a1 = new Account();
            a1.Email = inEmail;
            a1.Password = inPassword;
            result = a1.login(a1);
            return result;
        }
    }
}