﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using SofaDev.Class;
using System.Data;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Text;

namespace SofaDev.Controller
{
    public class ResponseController
    {
        private Response response;
        public ResponseController()
        {
        }
        public ResponseController(string location, string browserType,
                        DateTime startTime, DateTime endTime, string ipAddress)
        {
            ConvertDateTime convert = new ConvertDateTime();
            response = new Response(location, browserType, convert.convertDate(startTime), convert.convertDate(endTime), ipAddress);
            response.createResponse();
        }
        public int getResponseID()
        {
            return response.getResponseID();
        }

        public DataTable retrieveAllResponse(int surveyID)
        {
            ArrayList allResponse = new ArrayList();
            Response res = null;
            Response res1 = new Response();
            // Initialize DataTable
            DataTable dt = new DataTable();
            // Initialize DataColumn
            DataColumn dc = new DataColumn();
            // Initialize DataRow
            DataRow dr = null;

            // Add No. Column
            // Set AutoIncrement to No. Column
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;
            dc.AutoIncrementStep = 1;
            // Set Column Name
            dc.ColumnName = "No.";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Set Column to be unique
            dc.Unique = true;
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add ResponseID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Response ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Location Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Location";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add BrowserType Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Browser";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add StartDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Start Date Time";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.DateTime");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add EndDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "End Date Time";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.DateTime");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add ipAddress Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "IP Address";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            allResponse = (ArrayList)res1.retrieveAllResponse(surveyID);
            // Iterate through responses
            for (int i = 0; i < allResponse.Count; i++)
            {
                res = new Response();
                res = (Response)allResponse[i];
                dr = dt.NewRow();
                // Set values into data row
                dr["Response ID"] = res.getResponseID();
                dr["Location"] = res.getLocation();
                dr["Browser"] = res.getBrowserType();
                dr["Start Date Time"] = res.getStartDateTime();
                dr["End Date Time"] = res.getEndDateTime();
                dr["IP Address"] = res.getIpAddress();
                // Add new data row to data table
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void deleteResponse(int responseID)
        {
            Response res = new Response();
            res.deleteResponse(responseID);
        }

        public List<int> getQuestionTypes(int surveyID)
        {
            Result res = new Result();
            List<int> aL = res.retrieveQuestionTypes(surveyID);
            return aL;
        }

        public List<string> getQuestionAnswers(int reponseID)
        {
            Result res = new Result();
            List<String> ans = res.retrieveReponseResult(reponseID);
            return ans;
        }

        public List<int> getAllQuestionIDBySurvey(int surveyID)
        {
            Result res = new Result();
            List<int> aL = res.retrieveAllQuestionIDForThisSurvey(surveyID);
            return aL;
        }

        public List<string> getQuestionOptions(int questionID)
        {
            Result res = new Result();
            List<String> ans = res.retrieveQuestionOptions(questionID);
            return ans;
        }

        public int checkWhatIsTheCorrectOption(List<String> allQuestionOptions, String compare)
        {
            int finalResult = 0;
            for (int i = 0; i < allQuestionOptions.Count; i++)
            {
                String check = allQuestionOptions[i];
                if (check.Equals(compare))
                {
                    finalResult = i;
                    return finalResult;
                }
            }
            return finalResult;
        }

        // Retrieves questions
        public ArrayList retrievesQuestionList(int surveyID)
        {
            ArrayList questionSummary = new ArrayList();
            ArrayList surveyQuestionIDList = new ArrayList();
            ArrayList questionIDList = new ArrayList();
            Response resp = new Response();
            Result res = new Result();

            // Retrieves question IDs
            questionIDList = resp.retrieveQuestionID(surveyID);

            // Iterates through question ID and retrieve respective question titles and options
            for (int i = 0; i < questionIDList.Count; i++)
            {
                // Store into question summary arraylist
                questionSummary.Add(res.retrieveQuestions((int)questionIDList[i]));
            }

            // Return question summary arraylist
            return questionSummary;
        }

        // Retrieves survey responses summary
        public ArrayList retrieveSurveyResponseSummary(int surveyID)
        {
            ArrayList surveyResponseSummary = new ArrayList();
            ArrayList questionOptionResultCount = null;
            ArrayList questionNonOptionResultCount = null;
            Response resp = new Response();
            Result res = new Result();
            ArrayList surveyQuestionIDList = new ArrayList();
            ArrayList questionIDList = new ArrayList();
            ArrayList questionOptionList = new ArrayList();
            ArrayList resultOptionList = new ArrayList();
            ArrayList nonQuestionOptionResultList = new ArrayList();
            int questionTypeID = 0, questionID = 0, optionCount = 0, nonOptionCount = 0;

            // Retrieves survey question IDs
            surveyQuestionIDList = resp.retrieveQuestionID(surveyID);
            // Retrieves question IDs
            questionIDList = resp.retrieveQuestionID(surveyID);
            // Iterates through list of question IDs
            for (int i = 0; i < surveyQuestionIDList.Count; i++)
            {
                // Sets question ID
                questionID = (int)surveyQuestionIDList[i];
                // Sets question type ID
                questionTypeID = res.retrieveQuestionTypeID(questionID);

                // Checks for question type ID 5 (Check Box List)
                if (questionTypeID == 5)
                {
                    // Sets result options
                    resultOptionList = res.retrieveResultBySurveyQuestionID(questionID, surveyID);
                    // Initialize question option result count arraylist
                    questionOptionResultCount = new ArrayList();
                    // Retrieves question options
                    questionOptionList = res.retrieveQuestionOption(questionID);
                    // Iterate through options (questions)
                    for (int p = 0; p < questionOptionList.Count; p++)
                    {
                        // Iterate through options (results) and count number of occurence in each option
                        for (int q = 0; q < resultOptionList.Count; q++)
                        {
                            if (resultOptionList[q].ToString().Contains('|'))
                            {
                                // Get the multiple answers for each answer
                                string[] multipleSameOptions = resultOptionList[q].ToString().Split('|');
                                // Iterate through the multiple answers in each answer
                                for (int a = 0; a < multipleSameOptions.Length; a++)
                                {
                                    // Checks for same answer in each multiple option compared to option
                                    if (((string)(questionOptionList[p])).Equals((string)(multipleSameOptions[a])))
                                        // Incremenet option count when same occurence is found
                                        optionCount++;
                                }
                            }
                            else
                            {
                                // Checks for same answer in each multiple option compared to option
                                if (((string)(questionOptionList[p])).Equals((string)(resultOptionList[q])))
                                    // Incremenet option count when same occurence is found
                                    optionCount++;
                            }
                        }
                        // Add option counter into arraylist
                        questionOptionResultCount.Add(optionCount);
                        // Reset option count when we are done with one option
                        optionCount = 0;
                    }
                    // Add count into response summary arraylist
                    surveyResponseSummary.Add(questionOptionResultCount);
                }
                // Checks that question type ID is 3,4
                else if ((questionTypeID == 3) || (questionTypeID == 4))
                {
                    // Sets result options
                    resultOptionList = res.retrieveResultBySurveyQuestionID(questionID, surveyID);
                    // Initialize question option result count arraylist
                    questionOptionResultCount = new ArrayList();
                    // Retrieves question options
                    questionOptionList = res.retrieveQuestionOption(questionID);
                    // Iterate through options (questions)
                    for (int p = 0; p < questionOptionList.Count; p++)
                    {
                        // Iterate through options (results) and count number of occurence in each option
                        for (int q = 0; q < resultOptionList.Count; q++)
                        {
                            // Checks for same option selected as answer
                            if (((string)(questionOptionList[p])).Equals((string)(resultOptionList[q])))
                                // Increment option count when same occurence is found
                                optionCount++;
                        }
                        // Add option counter into arraylist
                        questionOptionResultCount.Add(optionCount);
                        // Reset option count when we are done with one option
                        optionCount = 0;
                    }
                    // Add count into response summary arraylist
                    surveyResponseSummary.Add(questionOptionResultCount);
                }
                // Else just count number of result
                else
                {
                    // Initialize question non option result count
                    questionNonOptionResultCount = new ArrayList();
                    // Retrieves result for question
                    nonQuestionOptionResultList = res.retrieveResultBySurveyQuestionID(questionID, surveyID);
                    // Iterates and count number of answers
                    for (int j = 0; j < nonQuestionOptionResultList.Count; j++)
                        // Increment non option count when answer is found for question
                        nonOptionCount++;
                    // Add non option counter into arraylist
                    questionNonOptionResultCount.Add(nonOptionCount);
                    // Add count into response summary arraylist
                    surveyResponseSummary.Add(questionNonOptionResultCount);
                    // Reset non option count when are done with one answer
                    nonOptionCount = 0;
                }
            }
            // Finally return surveyResponseSummary
            return surveyResponseSummary;
        }

        public DataTable generateSurveySummaryTable(ArrayList surveyResponseSummary, ArrayList questionSummary)
        {
            ArrayList responseNonList = null;
            int resultCount = 0;
            string resultString = "";
            string optionString = "";
            ArrayList questionOptionResultAL = null;
            ArrayList questionAL = null;
            ArrayList questionIDAL = null;
            ArrayList questionOptionAL = null;
            // Initialize DataTable
            DataTable dt = new DataTable();
            // Initialize DataColumn
            DataColumn dc = new DataColumn();
            // Initialize DataRow
            DataRow dr = null;

            // Add No. Column
            // Set AutoIncrement to No. Column
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;
            dc.AutoIncrementStep = 1;
            // Set Column Name
            dc.ColumnName = "No";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Set Column to be unique
            dc.Unique = true;
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add QuestionTitle Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Question";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Option Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Option";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Result Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Result";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Both questionSummary and surveyResponseSummary arraylist will have the same size
            // Question summary        -> arraylist (multiple arraylist (question title & question option))
            // Survey response summary -> no. of count in each option / responses in text box questions
            // Iterate through result count
            for (int i = 0; i < questionSummary.Count; i++)
            {
                dr = dt.NewRow();
                // Initialize resultString & optionString
                resultString = "";
                optionString = "";
                // Initialize questionAL & questionOptionAL & questionOptionResultAL & responseNonList
                questionAL = new ArrayList();
                questionIDAL = new ArrayList();
                questionOptionAL = new ArrayList();
                questionOptionResultAL = new ArrayList();
                questionOptionAL = new ArrayList();
                responseNonList = new ArrayList();
                // Gets question from questionSummary ArrayList
                questionAL = (ArrayList)questionSummary[i];
                // Gets question option from questionAL ArrayList
                questionOptionAL = (ArrayList)questionAL[1];
                // Now questionAL[0] = question title & questionAL[1] = options & questionAL[2] = questionID
                dr["Question"] = (string)questionAL[0];
                //dr["QuestionID"] = questionAL[2].ToString();
                // Then we check for questions with no options
                // questionOptionAL[0] is "" then its a question with no option
                if (questionOptionAL[0].Equals(" "))
                {
                    dr["Option"] = " ";
                    responseNonList = (ArrayList)surveyResponseSummary[i];
                    resultCount = (int)responseNonList[0];
                    dr["Result"] = resultCount.ToString();
                    // Reset resultCount
                    resultCount = 0;
                }
                else
                {
                    questionOptionResultAL = (ArrayList)surveyResponseSummary[i];
                    // Else iterate through the options
                    for (int k = 0; k < questionOptionAL.Count; k++)
                    {
                        optionString = optionString + (string)questionOptionAL[k] + "_";
                        resultCount = (int)questionOptionResultAL[k];
                        resultString = resultString + resultCount.ToString() + "_";
                        // Reset resultCount
                        resultCount = 0;
                    }
                    // Trim last char
                    optionString = optionString.Remove(optionString.Length - 1, 1);
                    resultString = resultString.Remove(resultString.Length - 1, 1);
                    // Add to data row
                    dr["Option"] = optionString;
                    dr["Result"] = resultString;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable retrieveAnalyseResult(int inSurveyID)
        {
            ResponseController rc = new ResponseController();
            return rc.generateSurveySummaryTable(rc.retrieveSurveyResponseSummary(inSurveyID), rc.retrievesQuestionList(inSurveyID));
        }

        // Method to generate bar chart (question name, number of responses for each, chart ID)
        public StringBuilder generateBarChart(ArrayList inQuestions, ArrayList inQuestionCounts, String inChartID)
        {
            String questionString = "";
            String questionCountString = "";
            StringBuilder sb = new StringBuilder();
            int highestValue = 0;


            // Open ended questions will not have any question options in them so checking here
            if (inQuestions != null)
            {
                // Append questions string
                for (int i = 0; i < inQuestions.Count; i++)
                {
                    // Checks for last item
                    if (inQuestions[i].ToString() == "")
                        questionString = questionString + ", ";
                    else
                        questionString = questionString + "'" + inQuestions[i].ToString() + "', ";
                }

                // Append question counts string
                for (int k = 0; k < inQuestionCounts.Count; k++)
                {
                    // Checks for last item 
                    if (inQuestionCounts[k].ToString() == "")
                        questionCountString = questionCountString + ", ";
                    else
                    {
                        // Gets the highest value amount all counts
                        if (int.Parse(inQuestionCounts[k].ToString()) > highestValue)
                            highestValue = int.Parse(inQuestionCounts[k].ToString());
                        questionCountString = questionCountString + inQuestionCounts[k].ToString() + ", ";
                    }
                }
                // Removes last two unwanted characters
                questionString = questionString.Remove(questionString.Length - 2, 2);
                questionCountString = questionCountString.Remove(questionCountString.Length - 2, 2);
            }
            else
            {
                questionString = "'Responses'";
                questionCountString = inQuestionCounts[0].ToString();
                highestValue = int.Parse(inQuestionCounts[0].ToString());
            }
            // Increment highest value by 1 so that it will look proportionate
            highestValue = highestValue + 1;

            sb.Append("<script>");
            sb.Append("$(document).ready(function(){");
            sb.Append("$.jqplot.config.enablePlugins = true;");
            sb.Append("var s1 = [" + questionCountString + "];");
            sb.Append("var ticks = [" + questionString + "];");
            sb.Append("plot1 = $.jqplot('" + inChartID + "', [s1], {");
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            sb.Append("animate: !$.jqplot.use_excanvas,");
            sb.Append("seriesDefaults:{");
            sb.Append("    renderer:$.jqplot.BarRenderer,");
            sb.Append("    pointLabels: { show: true }");
            sb.Append("},axesDefaults: {");
            sb.Append("     tickRenderer: $.jqplot.CanvasAxisTickRenderer,");
            sb.Append("     tickOptions: {");
            sb.Append("        angle: -30,");
            sb.Append("         fontSize: '10pt'");
            sb.Append("   }");
            sb.Append("},");
            sb.Append("axes: {");
            sb.Append("    xaxis: {");
            sb.Append("        renderer: $.jqplot.CategoryAxisRenderer,");
            sb.Append("        ticks: ticks");
            sb.Append("    },yaxis: {tickInterval: 5,ticks: [0, " + highestValue + "]},");
            sb.Append("},");
            sb.Append("highlighter: { show: false }");
            sb.Append("});");
            sb.Append("$('#" + inChartID + "').bind('jqplotDataClick', ");
            sb.Append("function (ev, seriesIndex, pointIndex, data) {");
            sb.Append("    $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);");
            sb.Append("}");
            sb.Append(");");
            sb.Append("});");
            sb.Append("</script>");
            return sb;
        }

        // Method to generate pie chart (question name, number of responses for each, chart ID)
        public StringBuilder generatePieChart(ArrayList inQuestions, ArrayList inQuestionCounts, String inChartID)
        {
            String questionString = "";
            StringBuilder sb = new StringBuilder();
            int highestValue = 0;

            // Open ended questions will not have any question options in them so checking here
            if (inQuestions != null)
            {
                // Append questions string and question counts
                for (int i = 0; i < inQuestionCounts.Count; i++)
                {
                    // Checks for last item so don't need to add , for last item
                    if ((inQuestionCounts[i].ToString() == "") && (inQuestions[i].ToString() == ""))
                        //questionString = questionString + "[null, null], ";
                        questionString = questionString + "";
                    else
                    {
                        // Gets the highest value amount all counts
                        if (int.Parse(inQuestionCounts[i].ToString()) > highestValue)
                            highestValue = int.Parse(inQuestionCounts[i].ToString());
                        questionString = questionString + "['" + inQuestions[i].ToString() + "', " + inQuestionCounts[i].ToString() + "], ";
                    }
                }
                // Removes last two unwanted characters
                questionString = questionString.Remove(questionString.Length - 2, 2);
            }
            else
            {
                // Default only one highest value
                highestValue = int.Parse(inQuestionCounts[0].ToString());
                questionString = questionString + "['Responses', " + inQuestionCounts[0].ToString() + "]";
            }
            // Increment highest value by 1 so that it will look proportionate
            highestValue = highestValue + 1;

            sb.Append("<script>$(document).ready(function () {var data = [" + questionString + "];var plot1 = " +
                "jQuery.jqplot('" + inChartID + "', [data], {seriesDefaults: {" +
                "renderer: jQuery.jqplot.PieRenderer,rendererOptions: {showDataLabels: true,dataLabels: 'value'}}," +
                "legend: { show: true, location: 'e' }});});</script>");

            return sb;
        }

        // Method to generate bar chart (question name, number of responses for each, chart ID)
        public StringBuilder generateLeftBarChart(ArrayList inQuestions, ArrayList inQuestionCounts, String inChartID)
        {
            String questionString = "";
            StringBuilder sb = new StringBuilder();
            int highestValue = 0;
            String chartHeight = "";

            // Open ended questions will not have any question options in them so checking here
            if (inQuestions != null)
            {
                // Append questions string and question counts
                for (int i = 0; i < inQuestionCounts.Count; i++)
                {
                    // Checks for last item so don't need to add , for last item
                    if ((inQuestionCounts[i].ToString() == "") && (inQuestions[i].ToString() == ""))
                        questionString = questionString + ", ";
                    else
                    {
                        // Gets the highest value amount all counts
                        if (int.Parse(inQuestionCounts[i].ToString()) > highestValue)
                            highestValue = int.Parse(inQuestionCounts[i].ToString());
                        questionString = questionString + "[" + inQuestionCounts[i].ToString() + ", '" + inQuestions[i].ToString() + "'], ";
                    }
                }
                // Removes last two unwanted characters
                questionString = questionString.Remove(questionString.Length - 2, 2);
                // Resize chart height
                chartHeight = "300";
            }
            else
            {
                // Resize chart height
                chartHeight = "150";
                // Default only one highest value
                highestValue = int.Parse(inQuestionCounts[0].ToString());
                questionString = questionString + "[" + inQuestionCounts[0].ToString() + ", 'Responses']";
            }
            // Increment highest value by 1 so that it will look proportionate
            highestValue = highestValue + 1;

            sb.Append("<script>" +
            "$(document).ready(function () {" +
            "   document.getElementById(\"" + inChartID + "\").style.height = \"" + chartHeight + "px\";" +
            "    $.jqplot.config.enablePlugins = true;" +
            "    plot = $.jqplot('" + inChartID + "', [[" + questionString + "]], {" +
            "        animate: !$.jqplot.use_excanvas," +
            "        seriesDefaults: {" +
            "            renderer: $.jqplot.BarRenderer," +
            "            shadowAngle: 135," +
            "            rendererOptions: {" +
            "                barDirection: 'horizontal'," +
            "            }" +
            "        }," +
            "        axes: {" +
            "            yaxis: {" +
            "                renderer: $.jqplot.CategoryAxisRenderer," +
            "                tickOptions: {" +
            "                    showGridline: true," +
            "                    markSize: 0" +
            "                }" +
            "            }," +
            "            xaxis: {" +
            "                tickInterval: 5,ticks: [0, " + highestValue + "]," +
            "                tickOptions: { formatString: '%d' }" +
            "            }" +
            "        }" +
            "    });" +
            "});" +
            "</script>");
            return sb;
        }

        // Method to generate line chart (question name, number of responses for each, chart ID)
        public StringBuilder generateLineChart(ArrayList inQuestions, ArrayList inQuestionCounts, String inChartID)
        {
            String questionString = "";
            StringBuilder sb = new StringBuilder();
            int highestValue = 0;

            // Open ended questions will not have any question options in them so checking here
            if (inQuestions != null)
            {
                // Append questions string and question counts
                for (int i = 0; i < inQuestionCounts.Count; i++)
                {
                    // Checks for last item so don't need to add , for last item
                    if ((inQuestionCounts[i].ToString() == "") && (inQuestions[i].ToString() == ""))
                        //questionString = questionString + "[null, null], ";
                        questionString = questionString + "";
                    else
                    {
                        // Gets the highest value amount all counts
                        if (int.Parse(inQuestionCounts[i].ToString()) > highestValue)
                            highestValue = int.Parse(inQuestionCounts[i].ToString());
                        questionString = questionString + "['" + inQuestions[i].ToString() + "', " + inQuestionCounts[i].ToString() + "], ";
                    }
                }
                // Removes last two unwanted characters
                questionString = questionString.Remove(questionString.Length - 2, 2);
            }
            else
            {
                // Default only one highest value
                highestValue = int.Parse(inQuestionCounts[0].ToString());
                questionString = questionString + "['Responses', " + inQuestionCounts[0].ToString() + "]";
            }
            // Increment highest value by 1 so that it will look proportionate
            highestValue = highestValue + 1;

            sb.Append("<script>" +
                "$(document).ready(function () {var line2 = [" + questionString + "];var plot2 = $.jqplot('" + inChartID + "', [line2], {" +
                "axes: {xaxis: {renderer: $.jqplot.CategoryAxisRenderer,labelRenderer: $.jqplot.CanvasAxisLabelRenderer," +
                "tickRenderer: $.jqplot.CanvasAxisTickRenderer, tickOptions: {angle: -30}},yaxis: {tickInterval: 5,ticks: [0, " + highestValue + "],labelRenderer: $.jqplot.CanvasAxisLabelRenderer}}});});" +
                "</script>");
            return sb;
        }

        public String getSurveyTitle(int inSurveyID)
        {
            String rSurveyTitle = "";
            Survey s = new Survey();
            rSurveyTitle = s.retrieveSurveyTitle(inSurveyID);
            return rSurveyTitle;
        }

        public String getSurveyDescription(int inSurveyID)
        {
            String rQuestionDescription = "";
            Survey s = new Survey();
            rQuestionDescription = s.retrieveSurveyDescription(inSurveyID);
            return rQuestionDescription;
        }

       
    }
}