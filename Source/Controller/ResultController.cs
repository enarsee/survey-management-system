﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SofaDev.Class;
using System.Collections;
using System.Data;

namespace SofaDev.Controller
{
    public class ResultController
    {
        public DataTable retrieveResult(int responseID)
        {
            Result res = new Result();
            ArrayList al = new ArrayList();
            // Initialize DataTable
            DataTable dt = new DataTable();
            // Initialize DataColumn
            DataColumn dc = new DataColumn();
            // Initialize DataRow
            DataRow dr = null;

            // Add No. Column
            // Set AutoIncrement to No. Column
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;
            dc.AutoIncrementStep = 1;
            // Set Column Name
            dc.ColumnName = "No.";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Set Column to be unique
            dc.Unique = true;
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add ResultID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Result ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Title Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Question";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Answer Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Answer";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            int count = 0;
            al = (ArrayList)res.retrieveResult(responseID);
            for (int i = 0; i < al.Count; i++)
            {
                if(count == 0)
                    dr = dt.NewRow();
                // Set values into data row
                //dr["Result ID"] = al[i];
                //dr["Question"] = al[i];
                //dr["Answer"] = al[i];

                if(count == 0)
                    dr["Result ID"] = al[i];
                else if(count == 1)
                    dr["Question"] = al[i];
                else if (count == 2)
                    dr["Answer"] = al[i];

                // Add new data row to data table
                if (count == 2)
                    dt.Rows.Add(dr);

                if (count == 2)
                    count = 0;
                else
                    count++;
            }
            
            return dt;
        }

        public void deleteResult(int responseID)
        {
            Result res = new Result();
            res.deleteResult(responseID);

        }

        public void updateResult(int questionID, int surveyID, int responseID, String answer)
        {
            Result res = new Result();
            res.updateResult(questionID, surveyID, responseID, answer);
        }

    }
}