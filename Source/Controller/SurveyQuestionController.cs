﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using SofaDev.Class;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Drawing;

namespace SofaDev.Controller
{
    public class SurveyQuestionController
    {
        private SurveyQuestion surveyQuestion;
        private Question[] question;
        private Result result;
        private DynamicControl control = new DynamicControl();
        private int surveyID;
        private int questionID;
        public SurveyQuestionController() { }

        public SurveyQuestionController(int surveyID, string questionID, string answer, int responseID)
        {
            // TODO: Complete member initialization
            surveyQuestion = new SurveyQuestion(surveyID, int.Parse(questionID));
            surveyQuestion.GetSurveyQuestionID();
            result = new Result(answer, surveyQuestion.getsurveryQuestionID(), responseID);
            result.createResult();
        }
        public SurveyQuestionController(int surveyID, int questionID)
        {
            // Create Survey usage
            this.surveyID = surveyID;
            this.questionID = questionID;
        }

        //Create a dynamic placeholder based on values in question
        private PlaceHolder CreatePlaceHolder(Question question, PlaceHolder holder, int count, int type)
        {
            //Type 1 is view survey , type 2 is do survey
            //Create Standard Controls needed

            // CheckBox check = control.CreateCheckBox(question.getQuestionID());
            Label qns = control.CreateLabel(question.getQuestionID(), count + ". ");
            Label labelTitle = control.CreateLabel(question.getQuestionID(), question.getTitle());
            HiddenField questionID = control.CreateHiddenField(question.getQuestionID(), question.getquestionTypeID());
            Button button = control.CreateButton(question.getQuestionID(), "Delete this question");
            qns.ID = "lblQns" + question.getQuestionID();
            labelTitle.ID = "lblTitle" + question.getQuestionID();
            if (count == 1)
                holder.Controls.Add(new LiteralControl("<br />"));
            holder.Controls.Add(new LiteralControl("<div class='question'>"));
            holder.Controls.Add(new LiteralControl("<div class='display-question'>"));
            holder.Controls.Add(qns);
            holder.Controls.Add(labelTitle);
            holder.Controls.Add(new LiteralControl("</div>"));
            holder.Controls.Add(new LiteralControl("<div class='delete-question-button'>"));
            if (type == 1)
                holder.Controls.Add(button);
            holder.Controls.Add(questionID);
            //    holder.Controls.Add(check);
            holder.Controls.Add(new LiteralControl("</div>"));
            holder.Controls.Add(new LiteralControl("<div class='clearfix'></div>"));
            holder.Controls.Add(new LiteralControl("<div class='display-question-options'>"));
            //holder.Controls.Add(new LiteralControl("<br />"));
            //Create other controls.
            switch (question.getquestionTypeID())
            {
                case 1:
                    TextBox textbox = control.CreateTextBox(question.getQuestionID());
                    holder.Controls.Add(textbox);
                    RequiredFieldValidator r1 = new RequiredFieldValidator();
                    r1.ControlToValidate = textbox.ID;
                    r1.Display = ValidatorDisplay.Dynamic;
                    r1.ErrorMessage = "Please enter a question!";
                    r1.ForeColor = Color.Red;
                    r1.ID = question.getQuestionID().ToString();
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(r1);
                    if (type == 1)
                    {
                        r1.Enabled = false;
                        textbox.Enabled = false;
                    }
                    break;
                case 2:
                    TextBox textarea = control.CreateTextArea(question.getQuestionID());
                    holder.Controls.Add(textarea);
                    RequiredFieldValidator r2 = new RequiredFieldValidator();
                    r2.ControlToValidate = textarea.ID;
                    r2.Display = ValidatorDisplay.Dynamic;
                    r2.ErrorMessage = "Please enter a question!";
                    r2.ForeColor = Color.Red;
                    r2.ID = question.getQuestionID().ToString();
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(r2);
                    if (type == 1)
                    {
                        r2.Enabled = false;
                        textarea.Enabled = false;
                    }
                    break;
                case 3:
                    RadioButtonList radBtn = control.CreateRadioButtonList(question.getQuestionID(), question.getquestionOption());
                    holder.Controls.Add(radBtn);
                    RequiredFieldValidator r3 = new RequiredFieldValidator();
                    r3.ControlToValidate = radBtn.ID;
                    r3.Display = ValidatorDisplay.Dynamic;
                    r3.ErrorMessage = "Please select an option!";
                    r3.ForeColor = Color.Red;
                    r3.ID = question.getQuestionID().ToString();
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(r3);
                    if (type == 1)
                    {
                        r3.Enabled = false;
                        radBtn.Enabled = false;
                    }
                    break;
                case 4:
                    DropDownList ddlList = control.CreateDropDownList(question.getQuestionID(), question.getquestionOption());
                    holder.Controls.Add(ddlList);
                    RequiredFieldValidator r4 = new RequiredFieldValidator();
                    r4.ControlToValidate = ddlList.ID;
                    r4.Display = ValidatorDisplay.Dynamic;
                    r4.ErrorMessage = "Please choose an option!";
                    r4.ForeColor = Color.Red;
                    r4.ID = question.getQuestionID().ToString();
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(r4);
                    if (type == 1)
                    {
                        r4.Enabled = false;
                        ddlList.Enabled = false;
                    }
                    break;
                case 5:
                    CheckBoxList chkBoxLst = control.CreateCheckBoxList(question.getQuestionID(), question.getquestionOption());
                    holder.Controls.Add(chkBoxLst);
                    Label error1 = new Label();
                    error1.ID = "error" + question.getQuestionID();
                    error1.Text = "Please select at least one option!";
                    error1.ForeColor = Color.Red;
                    error1.Visible = false;
                    
                    //r5.ServerValidate += new ServerValidateEventHandler(ValidateModuleList);
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));
                    holder.Controls.Add(new LiteralControl("&nbsp"));                  
                    holder.Controls.Add(error1);
                    if (type == 1)
                    {
                        error1.Visible = false;
                        chkBoxLst.Enabled = false;
                    }
                    break;
                default:
                    break;
            }
            holder.Controls.Add(new LiteralControl("</div>"));
            holder.Controls.Add(new LiteralControl("</div>"));
            return holder;
        }

        public int createSurveyQuestion()
        {
            surveyQuestion = new SurveyQuestion(surveyID, questionID);
            return surveyQuestion.createSurveyQuestion();
        }
        

        //Generate Place Holder
        public PlaceHolder getAllQuestionID(int sqSurveyID, int type)
        {
            PlaceHolder holder = new PlaceHolder();
            surveyQuestion = new SurveyQuestion(sqSurveyID);
            //Get all the question ID related to survey ID passed in and assign to a datatable
            DataTable table = surveyQuestion.GetAllQuestionID();
            question = new Question[table.Rows.Count];
            for (int i = 0; i < question.Length; i++)
            {
                DataRow myRow = table.Rows[i];
                question[i] = new Question((int)(myRow["sqQuestionID"]));
            }
            for (int i = 0; i < question.Length; i++)
            {
                holder = CreatePlaceHolder(question[i], holder, i + 1, type);
            }
            return holder;
        }
        public void getSurveyQuestionID(int sqSurveyID, int sqQuestionID)
        {
            surveyQuestion = new SurveyQuestion(sqSurveyID, sqQuestionID);
            surveyQuestion.GetSurveyQuestionID();
        }

        public void deleteSurveyQuestion(int sqSurveyID, int sqQuestionID)
        {
            surveyQuestion = new SurveyQuestion(sqSurveyID, sqQuestionID);
            surveyQuestion.DeleteSurveyQuestionResults();
            surveyQuestion.DeleteSurveyQuestion();
        }
    }
}