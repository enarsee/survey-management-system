using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using SofaDev.Class;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace SofaDev.Controller {
    public class SurveyController {

        private Survey survey;
        public SurveyController()
        {
        }
        public void setSurveyValues(DateTime startDate, DateTime endDate, string description, string title, int ownerID)
        {
            survey = new Survey(startDate, endDate, description, title, ownerID);
        }
        public int createSurvey()
        {
            survey.createSurvey();
            return survey.getSurveyID();
        }
        public Boolean Validate(int ownerID)
        {
            if (survey.getOwnerID() == ownerID)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<string> initQuestionTypeDDL()
        {
            List<string> types = new List<string>();
            string query = "SELECT comment FROM QuestionType";

            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                    types.Add(dataReader.GetString("Comment"));
                dataReader.Close();
                db.closeConnection();
            }
            return types;
        }

        public string getTitle()
        {
            return survey.getTitle();
        }

        public string getDescription()
        {
            return survey.getDescription();
        }

        public DateTime getStartDate()
        {
            return survey.getStartDateTime();
        }

        public DateTime getEndDate()
        {
            return survey.getEndDateTime();
        }

        public DataTable ViewSurvey() {
            ArrayList allSurvey = new ArrayList();
            Survey sur = null;
            Survey sur1 = new Survey();
            // Initialize DataTable
            DataTable dt = new DataTable();
            // Initialize DataColumn
            DataColumn dc = new DataColumn();
            // Initialize DataRow
            DataRow dr = null;
            
            // Add SurveyID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Survey ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add StartDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Start";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add EndDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "End";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);
			
			// Add Title Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Title";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add AccountID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Account ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            allSurvey = (ArrayList)sur1.ViewSurvey();

            DateTime dtTemp;

            // Iterate through surveys
            for (int i = 0; i < allSurvey.Count; i++)
            {
                sur = new Survey();
                sur = (Survey)allSurvey[i];
                dr = dt.NewRow();
                // Set values into data row
                dr["Survey ID"] = sur.getSurveyID();
                dtTemp = sur.getStartDateTime();
                dr["Start"] = dtTemp.ToString("dd/MM/yyyy");
                dtTemp = sur.getEndDateTime();
                dr["End"] = dtTemp.ToString("dd/MM/yyyy");
                //dr["Description"] = sur.getDescription();
                dr["Title"] = sur.getTitle();
                dr["Account ID"] = sur.getAccountID();

                // Add new data row to data table
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable ViewSurveyForUser(int accountID)
        {
            ArrayList allSurvey = new ArrayList();
            Survey sur = null;
            Survey sur1 = new Survey();
            // Initialize DataTable
            DataTable dt = new DataTable();
            // Initialize DataColumn
            DataColumn dc = new DataColumn();
            // Initialize DataRow
            DataRow dr = null;

            // Add No. Column
            // Set AutoIncrement to No. Column
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;
            dc.AutoIncrementStep = 1;
            // Set Column Name
            dc.ColumnName = "No.";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Set Column to be unique
            dc.Unique = true;
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add SurveyID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Survey ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add StartDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Start Date Time";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add EndDateTime Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "End Date Time";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Description Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Description";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add Title Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Title";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.String");
            // Add Column into datatable
            dt.Columns.Add(dc);

            // Add AccountID Column
            dc = new DataColumn();
            // Set Column Name
            dc.ColumnName = "Account ID";
            // Set Column Type
            dc.DataType = System.Type.GetType("System.Int32");
            // Add Column into datatable
            dt.Columns.Add(dc);

            allSurvey = (ArrayList)sur1.ViewSurveyForUser(accountID);
            // Iterate through surveys
            for (int i = 0; i < allSurvey.Count; i++)
            {
                sur = new Survey();
                sur = (Survey)allSurvey[i];
                dr = dt.NewRow();
                // Set values into data row
                dr["Survey ID"] = sur.getSurveyID();
                dr["Start Date Time"] = sur.getStartDateTime();
                dr["End Date Time"] = sur.getEndDateTime();
                dr["Description"] = sur.getDescription();
                dr["Title"] = sur.getTitle();
                dr["Account ID"] = sur.getAccountID();

                // Add new data row to data table
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void DeleteSurvey(int surveyID)
        {
            Survey temp = new Survey();
            temp.deleteSurveyResponse(surveyID);
            temp.deleteSurveyResult(surveyID);
            temp.deleteSurveyQuestion(surveyID);
            temp.deleteSurvey(surveyID);
        }
    }
}