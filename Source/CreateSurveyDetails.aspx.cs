﻿using SofaDev.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SofaDev
{
    public partial class SetSurveyDates : System.Web.UI.Page
    {
        protected SurveyController control;

        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (Session["Survey"] != null)
            {
                control = (SurveyController)Session["Survey"];
                if(!Page.IsPostBack)
                {
                    SurveyTitleTB.Text = control.getTitle();
                    SurveyDescriptionTB.Text = control.getDescription();
                    StartDateCal.SelectedDate = control.getStartDate();
                    EndDateCal.SelectedDate = control.getEndDate();
                }    
            }
            if (!Page.IsPostBack)
            {
                Session["startDate"] = DateTime.Now;
                Session["endDate"] = DateTime.Now;
            }
        }
        protected void StartDateCal_SelectionChanged1(object sender, EventArgs e)
        {
            Session["title"] = SurveyTitleTB.Text;
            Session["description"] = SurveyDescriptionTB.Text;
            if (DateTime.Compare(StartDateCal.SelectedDate, DateTime.Now) < 0)
            {
                StartDateCal.SelectedDate = DateTime.Now;

                errorMsg.Text = "Please ensure that Start Date is earlier than today's date ";
                ErrorPanel.Update();
                this.EnterTitle_ModalPopupExtender.Show();
            }
            else if (Session["endDate"] != null && DateTime.Compare(StartDateCal.SelectedDate, EndDateCal.SelectedDate) > 0)
            {
                StartDateCal.SelectedDate = DateTime.Now;

                errorMsg.Text = "Please ensure that Start Date is earlier than EndDate ";
                ErrorPanel.Update();
                this.EnterTitle_ModalPopupExtender.Show();
            }
            else
            {
                Session["startDate"] = StartDateCal.SelectedDate;
            }
        }

        protected void EndDateCal_SelectionChanged(object sender, EventArgs e)
        {
            Session["title"] = SurveyTitleTB.Text;
            Session["description"] = SurveyDescriptionTB.Text;
            if (DateTime.Compare(EndDateCal.SelectedDate, (DateTime)Session["startDate"]) < 0)
            {
                EndDateCal.SelectedDate = StartDateCal.SelectedDate;

                errorMsg.Text = "Please ensure that End Date is earlier than Start date ";
                ErrorPanel.Update();
                this.EnterTitle_ModalPopupExtender.Show();
            }
            else
            {
                Session["endDate"] = EndDateCal.SelectedDate;
            }
        }

        protected void CreateSurvey_Click(object sender, EventArgs e)
        {
            string error = "";
            if (SurveyTitleTB.Text.Trim().Equals(""))
                error += "Please enter a Survey title<br>";
            if (SurveyDescriptionTB.Text.Trim().Equals(""))
                error += "Please enter a Survey Description<br>";
            if (Session["startDate"] == null)
                Session["startDate"] = DateTime.Now;
            if (Session["endDate"] == null)
                Session["endDate"] = DateTime.Now.AddYears(1);
            if (error.Equals(""))
            {
                control = new SurveyController();
                control.setSurveyValues((DateTime)Session["startDate"], (DateTime)Session["endDate"], SurveyDescriptionTB.Text,SurveyTitleTB.Text,ac1.getAccountID());
                Session["Survey"] = control;
                Session["Validate"] = ac1.getAccountID();
                Response.Redirect("AddQuestion.aspx");
            }
            else
            {
                errorMsg.Text = error;
                ErrorPanel.Update();
                this.EnterTitle_ModalPopupExtender.Show();
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.EnterTitle_ModalPopupExtender.Hide();
        }


    }
}