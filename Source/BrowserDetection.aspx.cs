﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Class;

namespace SofaDev
{
    public partial class BrowserDetection : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //This will be done before the page loads up and redirect to another page or uses another master page.
            //if (Request.Browser.IsMobileDevice)
            //{
            //    Response.Redirect("ViewAllResponse.aspx");
            //}
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //System.DateTime.Now.ToString() for the date and time of this computer
            //Request.Browser
            lblBrowser.Text = Request.Browser.Type.ToString();
            lblipAddr.Text = Request.UserHostAddress.ToString();
            if (Request.Browser.IsMobileDevice)
            {
                RadioButtonList1.SelectedValue = "Yes";
            }
            else
            {
                RadioButtonList1.SelectedValue = "No";
            }
        }
    }
}