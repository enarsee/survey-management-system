﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;

namespace SofaDev
{
    public partial class Success : System.Web.UI.Page
    {

        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }
    }
}