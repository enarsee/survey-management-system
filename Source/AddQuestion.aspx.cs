﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;

namespace SofaDev
{
    public partial class AddQuestion : System.Web.UI.Page
    {
        private int questionCounter = 1, countOption = 1;
        SurveyController survey;

        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["Survey"] != null && Session["Validate"] != null)
            {
                survey = (SurveyController)Session["Survey"];
                bool validate = survey.Validate((int)Session["Validate"]);
                if (validate == false)
                {
                    Response.Redirect("CreateSurveyDetails.aspx");
                }
            }
            else
            {
                Response.Redirect("CreateSurveyDetails.aspx");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (Page.IsPostBack)
            {
                if (Session["phOption"] != null)
                {
                    PlaceHolder place2 = (PlaceHolder)Session["phOption"];
                    GeneratePlaceHolder(place2, "phOption");
                    InitializeButtonEvent();
                }
                if (Session["phQuestion"] != null)
                {
                    PlaceHolder place = (PlaceHolder)Session["phQuestion"];
                    GeneratePlaceHolder(place, "phQuestion");
                }
            }
            else
            {
                InitializeQuestionTypeDDL();
                lblqns.Text = "Question " + questionCounter + ". ";
                Session["countQns"] = questionCounter;
                Session["countOption"] = countOption;
            }
        }
        private void InitializeButtonEvent()
        {
            for (int i = 0; i < phOptions.Controls.Count; i++)
            {
                Control c = phOptions.Controls[i];
                if (c.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                {
                    LinkButton newbutton = new LinkButton();
                    newbutton = (LinkButton)c;
                    newbutton.Click += new EventHandler(Delete_Click);
                }
            }
        }
        private void InitializeQuestionTypeDDL()
        {
            List<string> typeList;
            if (Session["TypeDDL"] != null)
            {
                typeList = (List<string>)Session["TypeDDL"];
            }
            else
            {
                SurveyController temp = new SurveyController();
                typeList = temp.initQuestionTypeDDL();
            }
            int x = 1;
            foreach (string type in typeList)
            {
                ddlQuestionType.Items.Add(new ListItem(type, x++.ToString()));

            }
            Session["TypeDDL"] = typeList;
        }
        private void GeneratePlaceHolder(PlaceHolder holder, string type)
        {
            int count = holder.Controls.Count;
            for (int i = 1; i <= count; i++)
            {
                Control c = holder.Controls[0];
                if (type == "phOption")
                {
                    phOptions.Controls.Add(c);
                }
                else
                    phQuestions.Controls.Add(c);
                holder = (PlaceHolder)Session[type];
            }
            if (type == "phOption")
                Session["phOption"] = phOptions;
            else
                Session["phQuestion"] = phQuestions;
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlQuestionType.SelectedIndex == 2 || ddlQuestionType.SelectedIndex == 3 || ddlQuestionType.SelectedIndex == 4)
            {
                Panel2.Visible = true;
            }
            else
            {
                Panel2.Visible = false;
            }
        }
        protected void btnAddQuestion_Click(object sender, EventArgs e)
        {
            if (txtQns.Text == String.Empty)
            {
                lblQnsError.Visible = true;
                lblSurveyError.Visible = false;
               lblQnsError.Text = "Please enter a question";
            }
            else if (txtQns.Text != String.Empty && phOptions.Controls.Count==0 
                && (ddlQuestionType.SelectedIndex!=0 && ddlQuestionType.SelectedIndex!=1))
            {
                lblQnsError.Visible = false;
                lblSurveyError.Visible = false;
                lblOptionError.Visible = true;
                lblOptionError.Text = "Please enter a question option";
            }
            else
            {
                questionCounter = (int)Session["countQns"];
                Label lblQuestion = new Label();
                //Get qnsType
                HiddenField qnsType = new HiddenField();
                qnsType.Value = (ddlQuestionType.SelectedIndex + 1).ToString();
                qnsType.ID = "hidden_" + questionCounter;
                phQuestions.Controls.Add(qnsType);
                phQuestions.Controls.Add(new LiteralControl("<div class='question'>"));
                phQuestions.Controls.Add(new LiteralControl("<div class='display-question'>"));

                //Add Question to place holder
                lblQuestion.Text = questionCounter + ") " + txtQns.Text;
                lblQuestion.ID = "lbl" + questionCounter;
                phQuestions.Controls.Add(lblQuestion);

                phQuestions.Controls.Add(new LiteralControl("</div>"));
                phQuestions.Controls.Add(new LiteralControl("<div class='display-question-options'>"));

                //Choose right control
                switch (ddlQuestionType.SelectedIndex)
                {
                    case 0:
                        TextBox text = new TextBox();
                        text.ID = "txtBox" + questionCounter;
                        phQuestions.Controls.Add(text);
                        break;
                    case 1:
                        TextBox textArea = new TextBox();
                        textArea.ID = "textArea" + questionCounter;
                        textArea.TextMode = TextBoxMode.MultiLine;
                        textArea.Width = new Unit(260);
                        textArea.Height = new Unit(100);
                        phQuestions.Controls.Add(textArea);
                        break;
                    case 2:
                        RadioButtonList radio = new RadioButtonList();
                        radio.ID = "radBtnLst" + questionCounter;
                        for (int i = 0; i < phOptions.Controls.Count; i++)
                        {
                            Control c = phOptions.Controls[i];
                            if (c.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                            {
                                Label newLabel = (Label)c;
                                radio.Items.Add(new ListItem(newLabel.Text.Split('-')[1].ToString(), (i + 1).ToString()));
                            }
                        }
                        phQuestions.Controls.Add(radio);
                        break;
                    case 3:
                        DropDownList drop = new DropDownList();
                        drop.ID = "ddl" + questionCounter;
                        for (int i = 0; i < phOptions.Controls.Count; i++)
                        {
                            Control c = phOptions.Controls[i];
                            if (c.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                            {
                                Label newLabel = (Label)c;
                                drop.Items.Add(new ListItem(newLabel.Text.Split('-')[1].ToString(), (i + 1).ToString()));
                            }
                        }
                        phQuestions.Controls.Add(drop);
                        break;
                    case 4:
                        CheckBoxList check = new CheckBoxList();
                        check.ID = "boxList" + questionCounter;
                        for (int i = 0; i < phOptions.Controls.Count; i++)
                        {
                            Control c = phOptions.Controls[i];
                            if (c.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                            {
                                Label newLabel = (Label)c;
                                check.Items.Add(new ListItem(newLabel.Text.Split('-')[1].ToString(), (i + 1).ToString()));
                            }
                        }
                        phQuestions.Controls.Add(check);
                        break;
                }
                phQuestions.Controls.Add(new LiteralControl("</div>"));
                phQuestions.Controls.Add(new LiteralControl("</div>"));
                questionCounter++;
                lblqns.Text = "Question " + questionCounter + ". ";
                Session["countQns"] = questionCounter;
                Session["phQuestion"] = phQuestions;
                resetOption();
            }
            
        }
        protected void btnAddOptions_Click(object sender, EventArgs e)
        {
            if (txtOption.Text == String.Empty)
            {
                lblOptionError.Visible = true;
                lblSurveyError.Visible = false;
                lblQnsError.Visible = false;
                lblOptionError.Text = "Please enter an option";
            }
            else
            {
                lblOptionError.Visible = false;
                //Get the current number of options in phOption
                countOption = (int)Session["countOption"];

                //Create the new options label
                Label newLabel = new Label();

                //Create Delete Button
                LinkButton delete = new LinkButton();
                delete.ID = "btn" + countOption;
                delete.Text = "Delete";

                //Create Option Name
                newLabel.ID = "lblOp" + countOption;
                newLabel.Text = "-" + txtOption.Text;

                phOptions.Controls.Add(newLabel);
                phOptions.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
                phOptions.Controls.Add(delete);
                phOptions.Controls.Add(new LiteralControl("<br />"));

                //Increment option counter and save all 
                countOption++;
                txtOption.Text = "";
                Session["countOption"] = countOption;
                Session["phOption"] = phOptions;
                Session["phQuestion"] = phQuestions;


            }
        }
        private void resetOption()
        {
            //Whenever new question added, go back to original
            Session["phOption"] = null;
            Session["countOption"] = 1;
            txtOption.Text = "";
            txtQns.Text = "";
            phOptions.Controls.Clear();
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            LinkButton clicked = (LinkButton)sender;
            for (int i = 0; i < phOptions.Controls.Count; i++)
            {
                Control c = phOptions.Controls[i];
                if (c.ID == clicked.ID)
                {
                    //need to delete controls at -2 of current position for 4 times for 4 controls.
                    phOptions.Controls.RemoveAt(i - 2);
                    phOptions.Controls.RemoveAt(i - 2);
                    phOptions.Controls.RemoveAt(i - 2);
                    phOptions.Controls.RemoveAt(i - 2);
                    break;
                }
            }
        }
        protected void btnCreateSurvey_Click(object sender, EventArgs e)
        {
            if (txtOption.Text == String.Empty && txtQns.Text == String.Empty && phQuestions.Controls.Count == 0)
            {
                lblSurveyError.Visible = true;
                lblSurveyError.Text = "Error Survey Creation";
            }
            else
            {
                survey = (SurveyController)Session["Survey"];
                //get surveyID
                int surveyID = survey.createSurvey();
                for (int i = 0; i < phQuestions.Controls.Count; i++)
                {
                    Control c = phQuestions.Controls[i];
                    if (c.GetType().ToString().Equals("System.Web.UI.WebControls.HiddenField"))
                    {
                        HiddenField hidden = (HiddenField)c;
                        string qnsType = hidden.Value;
                        createQuestion(surveyID, int.Parse(qnsType), int.Parse(hidden.ID.Split('_')[1]));
                    }
                }
                resetOption();
                Session["phQuestion"] = null; ;
                Session["Survey"] = null;
                Response.Redirect("ViewAllSurvey.aspx");
            }
        }
        private void createQuestion(int surveyID, int qnsType, int controlID)
        {
            Label newLabel = (Label)phQuestions.FindControl("lbl" + controlID);
            QuestionController qControl;
            SurveyQuestionController sqControl;
            int questionID, validate = -1;
            string questionTitle = newLabel.Text.Split(')')[1];
            questionTitle = questionTitle.Remove(0, 1);
            switch (qnsType)
            {
                case 1:
                    qControl = new QuestionController(questionTitle, qnsType);
                    //Create Question and Get ID
                    questionID = qControl.createQuestion();
                    sqControl = new SurveyQuestionController(surveyID, questionID);
                    validate = sqControl.createSurveyQuestion();
                    TextBox text = (TextBox)phQuestions.FindControl("txt" + controlID);
                    break;
                case 2:
                    qControl = new QuestionController(questionTitle, qnsType);
                    //Create Question and Get ID
                    questionID = qControl.createQuestion();
                    sqControl = new SurveyQuestionController(surveyID, questionID);
                    validate = sqControl.createSurveyQuestion();
                    TextBox textArea = (TextBox)phQuestions.FindControl("textArea" + controlID);
                    break;
                case 3:
                    qControl = new QuestionController(questionTitle, qnsType);
                    //Create Question and Get ID
                    questionID = qControl.createQuestion();
                    //Create Question Option
                    RadioButtonList radio = (RadioButtonList)phQuestions.FindControl("radBtnLst" + controlID);
                    for (int i = 0; i < radio.Items.Count; i++)
                    {
                        qControl.createQuestionOption(radio.Items[i].Text);
                    }
                    sqControl = new SurveyQuestionController(surveyID, questionID);
                    validate = sqControl.createSurveyQuestion();
                    break;
                case 4:
                    qControl = new QuestionController(questionTitle, qnsType);
                    //Create Question and Get ID
                    questionID = qControl.createQuestion();
                    //Create Question Option
                    DropDownList drop = (DropDownList)phQuestions.FindControl("ddl" + controlID);
                    for (int i = 0; i < drop.Items.Count; i++)
                    {
                        qControl.createQuestionOption(drop.Items[i].Text);
                    }
                    sqControl = new SurveyQuestionController(surveyID, questionID);
                    validate = sqControl.createSurveyQuestion();
                    break;
                case 5:
                    qControl = new QuestionController(questionTitle, qnsType);
                    //Create Question and Get ID
                    questionID = qControl.createQuestion();
                    //Create Question Option
                    CheckBoxList check = (CheckBoxList)phQuestions.FindControl("boxList" + controlID);
                    for (int i = 0; i < check.Items.Count; i++)
                    {
                        qControl.createQuestionOption(check.Items[i].Text);
                    }
                    sqControl = new SurveyQuestionController(surveyID, questionID);
                    validate = sqControl.createSurveyQuestion();
                    break;
            }
            if (validate == 1)
            {
                Response.Redirect("Failure.aspx");
            }
        }
    }
}