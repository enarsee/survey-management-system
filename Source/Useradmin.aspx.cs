﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Security;

namespace SofaDev
{
    public partial class Useradmin : System.Web.UI.Page
    {
        AccountController ac1;

        protected override void OnPreInit(EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
          
        }
        
		//AccountController ac1 = new AccountController();
        protected void Page_Load(object sender, EventArgs e)
        {
 
            if (ac1.getAccType() != AccountType.Admin)
                Response.Redirect("User.aspx");
            else
            {
                //AccountController ac2 = new AccountController();

                GridView1.DataSource = ac1.viewAccounts();
                GridView1.DataBind();
            }
        }
		
    }
}