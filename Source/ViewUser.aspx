﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="ViewUser.aspx.cs" Inherits="SofaDev.ViewUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="Button2" runat="server" PostBackUrl="~/Useradmin.aspx" Text="Back" />
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Edit" />    <asp:Button ID="DeleteUser" runat="server" Text="Delete User" OnClick="DeleteUser_Click" />
    <div class="clearfix"></div>
<table>
   <tr>
     <td>Name: </td>
     <td><asp:TextBox ID="TextBox5" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox5" Font-Size="Smaller" ErrorMessage="Enter Name" ForeColor="Red"></asp:RequiredFieldValidator>
       </td>
   </tr>
   <tr>
     <td>Address: </td>
     <td><asp:TextBox ID="TextBox4" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox4" Font-Size="Smaller" ErrorMessage="Enter Address" ForeColor="Red"></asp:RequiredFieldValidator>
       </td>
   </tr>
    <tr>
     <td>Gender: </td>
     <td><asp:RadioButtonList ID="RadioButtonList1" runat="server"></asp:RadioButtonList></td>
   </tr>
    <tr>
     <td>Contact: </td>
     <td><asp:TextBox ID="TextBox2" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox2" Font-Size="Smaller" ErrorMessage="Enter Contact" ForeColor="Red"></asp:RequiredFieldValidator>
         <br />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter numbers only" Font-Size="Smaller" ControlToValidate="TextBox2" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
        </td>
   </tr>
    <tr>
     <td>Age: </td>
     <td><asp:TextBox ID="TextBox1" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox1" Font-Size="Smaller" ErrorMessage="Enter Age" ForeColor="Red"></asp:RequiredFieldValidator>
         <br />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter numbers only" Font-Size="Smaller" ControlToValidate="TextBox1" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
        </td>
   </tr>
    <tr>
     <td>Password: </td>
     <td><asp:TextBox ID="TextBox6" runat="server" Font-Size="Smaller" TextMode="Password"></asp:TextBox></td>
   </tr>
    <tr>
        <td colspan="2">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Font-Size="Smaller" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
</table>
</asp:Content>
