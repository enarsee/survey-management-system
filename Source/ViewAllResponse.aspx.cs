﻿using SofaDev.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SofaDev
{
    public partial class ViewAllResponse : System.Web.UI.Page
    {
        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            int surveyID = int.Parse(Request.QueryString["sid"]);
            ResponseController resCon = new ResponseController();
            GridView1.DataSource = resCon.retrieveAllResponse(surveyID);
            GridView1.DataBind();
            
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridView1.SelectedRow;
            String resID = row.Cells[2].Text;
            Response.Redirect("ViewResponse.aspx?id=" + resID + "&sid=" + Request.QueryString["sid"]);
        }

        protected void viewSurveyBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewSurvey.aspx?id=" + Request.QueryString["sid"]);
        }


    }
}