﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="ViewSurvey.aspx.cs" Inherits="SofaDev.ViewSurvey1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="DeleteSurvey" runat="server" Text="Delete this survey" OnClick="DeleteSurvey_Click" CssClass="delete-survey-button"/>
    <asp:Button ID="viewAllResponseBtn" runat="server" Text="View All Response" OnClick="viewAllResponseBtn_Click" CssClass="delete-survey-button"/>
    <asp:Button ID="responseSummaryBtn" runat="server" OnClick="responseSummaryBtn_Click" Text="Analyze Response" CssClass="delete-survey-button" />
    <a href="javascript://" class="link-button caps" onclick="window.open('DoSurvey.aspx?sid=<%=Request.QueryString["id"].ToString()%>', 'Test', 'menubar=0,resizable=0,width=1000,height=700,scrollbars=1', '').moveTo(100,100);" >Do Survey</a>
    <div class="clearfix"></div>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    <br />
    <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" CssClass="delete-survey-button" />
    <br />
</asp:Content>
