﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace SofaDev.Class
{
    public class Admin : Account
    {
        List<Account> accList;

        public Admin()
            : base()
        {
            this.AccType = AccountType.Admin;
            accList = new List<Account>();
            this.RetrieveAllAccounts();
        }

        public Admin(string fbID)
            : base(fbID)
        {
            this.AccType = AccountType.Admin;
            accList = new List<Account>();
            this.RetrieveAllAccounts();
        }


        public Admin(string inEmail, string inPassword)
            : base(inEmail, inPassword)
        {
            this.AccType = AccountType.Admin;
            accList = new List<Account>();
            this.RetrieveAllAccounts();
        }

        /// <summary>
        /// Deletes the specified account
        /// </summary>
        /// <param name="inID"></param>
        /// <returns>Returns the account id of account deleted, -1 if account not found</returns>
        public int DeleteAccount(int inID)
        {
            DBConnect db = new DBConnect();
            int result = 0;

            for (int i = 0; i < accList.Count(); i++)
            {
                if (accList[i].AId == inID)
                {
                    result = 1;
                    accList.RemoveAt(i);
                    break;
                }
            }
            // if object not found, end.
            if (result == 0) return 0;
            string query = "DELETE FROM account WHERE accountID = '" + inID + "'";
            //Open connection
            if (db.openConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());

                try
                {
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }

                //close connection
                db.closeConnection();
            }
            return result;
        }

        /// <summary>
        /// Returns the number of accounts stored in the list. Calls RetrieveAllAccounts() if not done.
        /// </summary>
        /// <returns></returns>
        public int GetAccountCount()
        {
            if (accList.Count == 0)
            {
                RetrieveAllAccounts();
            }
            return accList.Count;
        }

        /// <summary>
        /// Returns the account associated with the index of the array
        /// </summary>
        /// <param name="inIndex">Array index</param>
        /// <returns>Returns Account ID if found, -1 if not found.</returns>
        public Account GetAccount(int inAccID)
        {
            foreach (Account a in accList) {
                if (a.AId == inAccID) return a;
            }
            //if (inIndex < accList.Count())
            //    return accList[inIndex];
            return null;
        }

        /// <summary>
        /// Retrieves all accounts from database.
        /// </summary>
        private void RetrieveAllAccounts()
        {
            string queryAcc = "SELECT * FROM account";
            string queryInfo = "SELECT * FROM userinfo";
            accList.Clear();

            DBConnect db = new DBConnect();
            Account _tempAcc = null;

            //Open connection
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(queryAcc, db.getConnection());

                try
                {
                    //Execute queryAcc command
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    if (!dataReader.HasRows) return;
                    while (dataReader.Read())
                    {
                        _tempAcc = new Account();
                        _tempAcc.AId = dataReader.GetInt32("accountID");
                        _tempAcc.Email = dataReader.GetString("email");
                        _tempAcc.Password = dataReader.GetString("password");
                        _tempAcc.AccType = (AccountType)dataReader.GetInt32("type");
                        accList.Add(_tempAcc);
                        _tempAcc = null;
                    }
                    dataReader.Close();

                    //Execute queryInfo command
                    cmd.CommandText = queryInfo;
                    dataReader = cmd.ExecuteReader();
                    if (!dataReader.HasRows) return;
                    int i = 0, tid = 0;
                    while (dataReader.Read())
                    {
                        tid = dataReader.GetInt32("userInfoAccountID");
                        // fail safe to ensure the user info is retrieved properly
                        if (accList[i].AId != tid)
                        {
                            for (int j = 0; j < accList.Count(); j++)
                            {
                                if (accList[j].AId == tid)
                                {
                                    i = j;
                                    break;
                                }
                            }
                        }
                        accList[i].Name = dataReader.GetString("name");
                        accList[i].Age = dataReader.GetInt32("age");
                        accList[i].Gender = (GenderType)dataReader.GetInt32("gender");
                        accList[i].Address = dataReader.GetString("address");
                        accList[i].Contact = dataReader.GetString("contact");
                        i = (i + 1) % accList.Count();
                    }
                    dataReader.Close();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                finally
                {
                    db.closeConnection();
                }
            }
        }

        public List<Account> AccountList
        {
            get {
                if (accList.Count == 0)
                {
                    RetrieveAllAccounts();
                }
                return accList;
            }
        }

        static public Admin Cast(Account account)
        {
            try
            {
                return (Admin)account;
            }
            catch (InvalidCastException e)
            {
                throw new InvalidCastException("Invalid Admin Cast\n");
            }
        }
    }
}