﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SofaDev.Class
{
    public enum GenderType {
    	Female = 0,
        Male,
        Nil
    }

    public class UserInfo {
        // Variables
        private string name;
        private int age;
        private GenderType gender;
        private string address;
        private string contact;

        public UserInfo()
        {
            name = "";
            age = 0;
            gender = GenderType.Nil;
            address = "";
            contact = "";
        }

        // Methods
        public string Name {
            get { return name; }
            set { name = value; }
        }

        public GenderType Gender {
            get { return gender; }
            set { gender = value; }
        }

        public int Age {
            get { return age; }
            set { age = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }
    }
}