﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SofaDev.Class;

public class Result
{
    // Variables
    private int resultID;
    private string answer;
    private int surveyQuestionID;
    private int responseID;

    // Default Constructor
    public Result() { }

    // Constructor with all fields
    public Result(int resultID, string answer, int surveyQuestionID, int responseID)
    {
        this.resultID = resultID;
        this.answer = answer;
        this.surveyQuestionID = surveyQuestionID;
        this.responseID = responseID;
    }
    public Result(string answer, int surveyQuestionID, int responseID)
    {
        this.answer = answer;
        this.surveyQuestionID = surveyQuestionID;
        this.responseID = responseID;
    }

    // Setter methods
    public void setResultID(int resultID) { this.resultID = resultID; }
    public void setAnswer(string answer) { this.answer = answer; }
    public void setsurveyQuestionID(int surveyQuestionID) { this.surveyQuestionID = surveyQuestionID; }
    public void setResponseID(int responseID) { this.responseID = responseID; }

    // Getter methods
    public int getResultID() { return resultID; }
    public string getAnswer() { return answer; }
    public int getsurveyQuestionIDD() { return surveyQuestionID; }
    public int getResponseID() { return responseID; }

    // Retrieve result
    public ArrayList retrieveResult(int responseID)
    {
        Result res;
        ArrayList al = new ArrayList();
        string stringQuery = "SELECT resultID,title,answer " +
                                "FROM result, question, surveyQuestion " +
                                "WHERE rSurveyQuestionID = surveyQuestionID " +
                                "AND sqQuestionID = questionID " +
                                "AND responseID = " + responseID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                // Create new object to 
                /*res = new Result();
                res.setResultID(dataReader.GetInt32(dataReader.GetOrdinal("resultID")));
                res.setAnswer(dataReader.GetString(dataReader.GetOrdinal("answer")));
                res.setResponseID(dataReader.GetInt32(dataReader.GetOrdinal("responseID")));*/
                al.Add(dataReader.GetInt32(dataReader.GetOrdinal("resultID")));
                al.Add(dataReader.GetString(dataReader.GetOrdinal("title")));
                al.Add(dataReader.GetString(dataReader.GetOrdinal("answer")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return arraylist of response
        return al;
    }

    // Retrieve question title based on question ID
    public String retrieveQuestionByQuestionID(int inQuestionID)
    {
        String rQuestionTitle = "";
        string stringQuery = "SELECT title " +
                                "FROM question " +
                                "WHERE questionID = " + inQuestionID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                // Sets question title
                rQuestionTitle = dataReader.GetString(dataReader.GetOrdinal("title"));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return question title
        return rQuestionTitle;
    }

    // Retrieve result by surveyID and surveyquestionID
    public ArrayList retrieveResultBySurveyQuestionID(int inSurveyQuestionID,int inSurveyID)
    {
        ArrayList resultAl = new ArrayList();
        string stringQuery = "SELECT answer " +
                                "FROM result,surveyquestion " +
                                "WHERE rSurveyQuestionID = surveyQuestionID " +
                                "AND sqSurveyID = " + inSurveyID + " " +
                                "AND sqQuestionID = " + inSurveyQuestionID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                // Adds answers into arraylist
                resultAl.Add(dataReader.GetString(dataReader.GetOrdinal("answer")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return arraylist of answers
        return resultAl;
    }

    // Retrieves question
    public ArrayList retrieveQuestions(int inQuestionID)
    {
        ArrayList rQuestions = new ArrayList();
        string rQuestionTitle = "";
        ArrayList rQuestionOption = new ArrayList();
        Result r = new Result();
        int questionTypeID = retrieveQuestionTypeID(inQuestionID);

        string stringQuery = "SELECT title " +
                        "FROM question " +
                        "WHERE questionID = " + inQuestionID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                // Store question titles
                rQuestionTitle = dataReader.GetString(dataReader.GetOrdinal("title"));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }

        // Store question options
        if ((questionTypeID == 3) || (questionTypeID == 4) || (questionTypeID == 5))
            rQuestionOption = retrieveQuestionOption(inQuestionID);
        else
            rQuestionOption.Add(" ");

        // Store question ID and question options into questions arraylist
        rQuestions.Add(rQuestionTitle);
        rQuestions.Add(rQuestionOption);

        // Return questions arraylist
        return rQuestions;
    }

    // Retrieves question type ID
    public int retrieveQuestionTypeID(int inQuestionID)
    {
        int rQuestionTypeID = 0;
        string stringQuery = "SELECT questionTypeID " +
                        "FROM question " +
                        "WHERE questionID = " + inQuestionID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                rQuestionTypeID = dataReader.GetInt32(dataReader.GetOrdinal("questionTypeID"));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return question type ID
        return rQuestionTypeID;
    }

    // Retrieve question option
    public ArrayList retrieveQuestionOption(int inQuestionID)
    {
        ArrayList rOption = new ArrayList();
        string stringQuery = "SELECT qOption " +
                        "FROM questionoption " +
                        "WHERE questionID = " + inQuestionID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                rOption.Add(dataReader.GetString(dataReader.GetOrdinal("qOption")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return question type ID
        return rOption;
    }

    public List<int> retrieveQuestionTypes(int surveyID)
    {
        List<int> rOption = new List<int>();
        string stringQuery = "SELECT questionTypeID from question, surveyquestion " +
            "WHERE sqQuestionID = questionID AND sqSurveyID = " + surveyID;

        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                rOption.Add(dataReader.GetInt32(dataReader.GetOrdinal("questionTypeID")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return question type ID
        return rOption;
    }

    public void deleteResult(int responseID)
    {
         string stringQuery = "DELETE FROM result " +
                                "WHERE responseID = " + responseID;
        DBConnect db = new DBConnect();
        //Open connection
        if (db.openConnection() == true)
        {
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.ExecuteNonQuery();
                db.closeConnection();
            }
          
        }
    }

    public List<String> retrieveReponseResult(int responseID)
    {
        List<String> answers = new List<string>();
        string stringQuery = "SELECT answer " +
                                "FROM question, surveyquestion, result " +
                                "WHERE sqQuestionID = questionID " +
                                "AND rSurveyQuestionID = surveyQuestionID " +
                                "AND responseID = " + responseID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                answers.Add(dataReader.GetString(dataReader.GetOrdinal("answer")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return arraylist of response
        return answers;
    }

    public List<int> retrieveAllQuestionIDForThisSurvey(int surveyID)
    {
        List<int> rOption = new List<int>();
        string stringQuery = "SELECT sqquestionid FROM surveyquestion where sqsurveyID = " +surveyID;

        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                rOption.Add(dataReader.GetInt32(dataReader.GetOrdinal("sqquestionid")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return question type ID
        return rOption;
    }

    public List<String> retrieveQuestionOptions(int questionID)
    {
        List<String> qOption = new List<string>();
        string stringQuery = "Select qOption FROM questionoption, question, surveyquestion " +
                                "WHERE questionoption.questionID = question.questionID " +
                                "AND question.questionID = surveyquestion.sqquestionID  " +
                                "AND question.questionID = " + questionID;
        DBConnect db = new DBConnect();
        //Open connection
        MySqlDataReader dataReader = null;
        if (db.openConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            //Create a data reader and Execute the command
            dataReader = cmd.ExecuteReader();
            //Read the data
            while (dataReader.Read())
            {
                qOption.Add(dataReader.GetString(dataReader.GetOrdinal("qOption")));
            }
            //close Data Reader
            dataReader.Close();
            //close Connection
            db.closeConnection();
        }
        // Finally return arraylist of response
        return qOption;
    }

    public void updateResult(int questionID, int surveyID, int responseID, String answer)
    {
        string stringQuery = "UPDATE result r INNER JOIN surveyquestion s " +
                                "ON r.rSurveyQuestionID = s.surveyQuestionID " +
                                "SET r.answer = @answer WHERE s.sqSurveyID = @surveyID AND s.sqQuestionID = @questionID and r.responseID =@responseID";
        DBConnect db = new DBConnect();
        if(db.openConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@answer", answer);
            cmd.Parameters.AddWithValue("@surveyID", surveyID);
            cmd.Parameters.AddWithValue("@responseID", responseID);
            cmd.Parameters.AddWithValue("@questionID", questionID);
            try
            {
                responseID = int.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
              string error = ex.Message;
            }

       }
       db.closeConnection();  
    }

    public void createResult()
    {
        string stringQuery = "INSERT INTO result(answer,rSurveyQuestionID,responseID)";
        stringQuery += " VALUES (@answer, @rSurveyQuestionID, @responseID); SELECT LAST_INSERT_ID();";

        DBConnect db = new DBConnect();
        if (db.openConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@answer", answer);
            cmd.Parameters.AddWithValue("@rSurveyQuestionID", surveyQuestionID);
            cmd.Parameters.AddWithValue("@responseID", responseID);
            try
            {
                resultID = int.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

        }
        db.closeConnection();
    }
}