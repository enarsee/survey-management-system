using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Data;

namespace SofaDev.Class {
    public class Survey {
        // Variables
        private int surveyID;
        private DateTime startDateTime;
        private DateTime endDateTime;
        private string description;
		private string title;
		private int accountID;
        private List<Question> questionList;

        // Default Constructor
        public Survey() { }

        // Setter Methods
        public void setSurveyID(int surveyID) { this.surveyID = surveyID; }
        public void setStartDateTime(DateTime startDate) { this.startDateTime = startDate; }
        public void setEndDateTime(DateTime endDate) { this.endDateTime = endDate; }
        public void setDescription(string description) { this.description = description; }
        public void setTitle(string title) { this.title = title; }
        public void setAccountID(int accountID) { this.accountID = accountID; }
        public void setQuestionList(List<Question> newList) { this.questionList = newList; }

        // Getter Methods
        public int getSurveyID() { return surveyID; }

        public DateTime getStartDateTime() { return startDateTime; }
        public DateTime getEndDateTime() { return endDateTime; }
        public string getDescription() { return description; }
        public string getTitle() { return title; }
        public int getAccountID() { return accountID; }
        public List<Question> getQuestionList() { return questionList; }
        public int getOwnerID()
        {
            return accountID;
        }

        // Constructor with all fields
        public Survey(int surveyID, DateTime startDateTime, DateTime endDateTime, string description,
		string title, int accountID) {
            this.surveyID = surveyID;
            this.startDateTime = startDateTime;
            this.description = description;
            this.title = title;
			this.accountID = accountID;
        }

        public Survey(DateTime startDate, DateTime endDate, string description, string title,int accountID)
        {
            this.startDateTime = startDate;
            this.endDateTime = endDate;
            this.description = description;
            this.title = title;
            this.accountID = accountID;
        }
       

        public void createSurvey()
        {
            string stringQuery = "INSERT INTO Survey(startDate,endDate,description,title,accountID)";
            stringQuery += " VALUES (@startDate, @endDate, @description, @title, @accountID); SELECT LAST_INSERT_ID();";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@startDate", startDateTime);
                cmd.Parameters.AddWithValue("@endDate", endDateTime);
                cmd.Parameters.AddWithValue("@description", description);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@accountID", accountID);
                try
                {
                    surveyID = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
                db.closeConnection();
            }
        }


        // Retrieve All survey
        public ArrayList ViewSurvey() {
            Survey sur;
            ArrayList allSurvey = new ArrayList();
            string stringQuery = "SELECT * FROM survey";
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while(dataReader.Read())
                {
                    // Create new object to 
                    sur = new Survey();
                    sur.setSurveyID(dataReader.GetInt32(dataReader.GetOrdinal("surveyID")));
                    sur.setStartDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("startDate")));
                    sur.setEndDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("endDate")));
                    sur.setDescription(dataReader.GetString(dataReader.GetOrdinal("description")));
					sur.setTitle(dataReader.GetString(dataReader.GetOrdinal("title")));
					sur.setAccountID(dataReader.GetInt32(dataReader.GetOrdinal("accountID")));
                    // Add one set of survey entry into arraylist
                    allSurvey.Add(sur);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            // Finally return arraylist of survey
            return allSurvey;
        }

        public ArrayList ViewSurveyForUser(int accountID2)
        {
            Survey sur;
            ArrayList allSurvey = new ArrayList();
            string stringQuery = "SELECT * FROM survey WHERE accountID = @accountID";
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@accountID", accountID2);
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while (dataReader.Read())
                {
                    // Create new object to 
                    sur = new Survey();
                    sur.setSurveyID(dataReader.GetInt32(dataReader.GetOrdinal("surveyID")));
                    sur.setStartDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("startDate")));
                    sur.setEndDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("endDate")));
                    sur.setDescription(dataReader.GetString(dataReader.GetOrdinal("description")));
                    sur.setTitle(dataReader.GetString(dataReader.GetOrdinal("title")));
                    sur.setAccountID(dataReader.GetInt32(dataReader.GetOrdinal("accountID")));
                    // Add one set of survey entry into arraylist
                    allSurvey.Add(sur);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            // Finally return arraylist of survey
            return allSurvey;
        }

        public void deleteSurveyResponse(int surveyID)
        {
            string stringQuery = "DELETE Response FROM response, result, surveyquestion WHERE surveyquestionID = rsurveyquestionID AND result.responseID = response.responseID AND surveyquestion.sqsurveyID = @surveyID";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@surveyID", surveyID);

                cmd.ExecuteNonQuery();
            }
            db.closeConnection();
        }

        public void deleteSurveyResult(int surveyID)
        {
            string stringQuery = "DELETE Result FROM result, surveyquestion WHERE surveyquestionID = rsurveyquestionID AND surveyquestion.sqsurveyID = @surveyID";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@surveyID", surveyID);

                cmd.ExecuteNonQuery();
            }
            db.closeConnection();
        }

        public void deleteSurveyQuestion(int surveyID)
        {
            string stringQuery = "DELETE SurveyQuestion FROM surveyquestion WHERE sqsurveyID = @surveyID";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@surveyID", surveyID);

                cmd.ExecuteNonQuery();
            }
            db.closeConnection();
        }

        public void deleteSurvey(int surveyID)
        {
            string stringQuery = "DELETE FROM survey WHERE surveyID = @surveyID";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@surveyID", surveyID);

                cmd.ExecuteNonQuery();
            }
            db.closeConnection();
        }

        // Retrieve survey description based on survey ID
        // Retrieve question description based on survey ID
        public String retrieveSurveyDescription(int inSurveyID)
        {
            String rQuestionDescription = "";
            string stringQuery = "SELECT description " +
                                    "FROM survey " +
                                    "WHERE surveyID = " + inSurveyID;
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while (dataReader.Read())
                {
                    // Sets question description
                    rQuestionDescription = dataReader.GetString(dataReader.GetOrdinal("description"));
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            // Finally return question description
            return rQuestionDescription;
        }

        // Retrieve survey title based on survey ID
        public String retrieveSurveyTitle(int inSurveyID)
        {
            String rSurveyTitle = "";
            string stringQuery = "SELECT title " +
                            "FROM survey " +
                            "WHERE surveyID = " + inSurveyID;
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while (dataReader.Read())
                {
                    rSurveyTitle = dataReader.GetString(dataReader.GetOrdinal("title"));
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            // Finally return question type ID
            return rSurveyTitle;
        }
    }
}