﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using MySql.Data.MySqlClient;
using System.Data;

namespace SofaDev.Class
{
    public class Response
    {
        // Variables
        private int responseID;
        private string location;
        private string browserType;
        private DateTime startDateTime;
        private DateTime endDateTime;
        private string ipAddress;

        // Default Constructor
        public Response() { }

        // Constructor with all fields
        public Response(int responseID, string location, string browserType,
                        DateTime startTime, DateTime endTime, string ipAddress) 
        {
            this.responseID = responseID;
            this.location = location;
            this.browserType = browserType;
            this.startDateTime = startTime;
            this.endDateTime = endTime;
            this.ipAddress = ipAddress;
        }
        public Response(string location, string browserType,
                        DateTime startTime, DateTime endTime, string ipAddress)
        {
            this.location = location;
            this.browserType = browserType;
            this.startDateTime = startTime;
            this.endDateTime = endTime;
            this.ipAddress = ipAddress;
        }
        // Setter Methods
        public void setResponseID(int responseID) { this.responseID = responseID; }
        public void setLocation(string location) { this.location = location; }
        public void setBrowserType(string browserType) { this.browserType = browserType; }
        public void setStartDateTime(DateTime startTime) { this.startDateTime = startTime; }
        public void setEndDateTime(DateTime endTime) { this.endDateTime = endTime; }
        public void setIpAddress(string ipAddress) { this.ipAddress = ipAddress; }

        // Getter Methods
        public int getResponseID() { return responseID; }
        public string getLocation() { return location; }
        public string getBrowserType() { return browserType; }
        public DateTime getStartDateTime() { return startDateTime; }
        public DateTime getEndDateTime() { return endDateTime; }
        public string getIpAddress() { return ipAddress; }

        // Retrieve All response
        public ArrayList retrieveAllResponse(int surveyID)
        {
            Response res;
            ArrayList allResponse = new ArrayList();
            string stringQuery = "SELECT DISTINCT response.* " +
                                    "FROM response,result,surveyquestion " +
                                    "WHERE response.responseID = result.responseID " +
                                    "AND result.rsurveyQuestionID = surveyquestion.surveyquestionID " +
                                    "AND sqSurveyID = " + surveyID;
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while(dataReader.Read())
                {
                    // Create new object to 
                    res = new Response();
                    res.setResponseID(dataReader.GetInt32(dataReader.GetOrdinal("responseID")));
                    res.setLocation(dataReader.GetString(dataReader.GetOrdinal("location")));
                    res.setBrowserType(dataReader.GetString(dataReader.GetOrdinal("browserType")));
                    res.setStartDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("startDateTime")));
                    res.setEndDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("endDateTime")));
                    res.setIpAddress(dataReader.GetString(dataReader.GetOrdinal("ipAddress")));
                    // Add one set of response data into arraylist
                    allResponse.Add(res);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            // Finally return arraylist of response
            return allResponse;
        }

        public void deleteResponse(int responseID)
        {
            string stringQuery = "DELETE FROM response " +
                                   "WHERE responseID = " + responseID;
            DBConnect db = new DBConnect();
            //Open connection
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.ExecuteNonQuery();
                db.closeConnection();
            }
        }

        //Create Response
        public void createResponse()
        {
            string stringQuery = "INSERT INTO response(location,browserType,startDateTime,endDateTime,ipAddress)";
            stringQuery += " VALUES (@location, @browserType, @startDateTime, @endDateTime, @ipAddress); SELECT LAST_INSERT_ID();";

            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@location", location);
                cmd.Parameters.AddWithValue("@browserType", browserType);
                cmd.Parameters.AddWithValue("@startDateTime", startDateTime);
                cmd.Parameters.AddWithValue("@endDateTime", endDateTime);
                cmd.Parameters.AddWithValue("@ipAddress", ipAddress);
                try
                {
                    responseID = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

            }
            db.closeConnection();
        }

        public ArrayList retrieveSurveyQuestionID(int inSurveyID)
        {
            ArrayList rSurveyQuestionID = new ArrayList();
            string stringQuery = "SELECT surveyQuestionID, sqQuestionID, sqSurveyID "
                                + "FROM surveyquestion "
                                + "WHERE sqSurveyID = " + inSurveyID + ";";
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while (dataReader.Read())
                {
                    rSurveyQuestionID.Add(dataReader.GetInt32(dataReader.GetOrdinal("surveyQuestionID")));
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            return rSurveyQuestionID;
        }

        // Retrieve question ID based on survey ID
        public ArrayList retrieveQuestionID(int inSurveyID)
        {
            ArrayList rQuestionID = new ArrayList();
            string stringQuery = "SELECT sqQuestionID "
                                + "FROM surveyquestion "
                                + "WHERE sqSurveyID = " + inSurveyID + ";";
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                dataReader = cmd.ExecuteReader();
                //Read the data
                while (dataReader.Read())
                {
                    rQuestionID.Add(dataReader.GetInt32(dataReader.GetOrdinal("sqQuestionID")));
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
            return rQuestionID;
        }
    }

}

