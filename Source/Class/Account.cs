﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SofaDev.Class;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace SofaDev
{
    public enum AccountType {
        Admin = 0,
        Creator,
        Nil
    }

    public enum DBDataType {
        Email = 1,
        Password,
        AccType,
        Name,
        Age,
        Gender,
        Address,
        Contact,
        Nil
    }

    public class Account
    {
        // Declare variables
        private int aId;
        private string email;
        private string password;
        private AccountType accType;
        //private List<string> sqlCommands;

        // Facebook variables
        private string fbId;
        //private string fbAccessToken;
        private string fbUserName;

        protected UserInfo userInfo;

        private static string hashString = "uHIJlmRSTUtKLpNMnoO89ZFvw01234zABCVWG5xycd6XYabhijkqrsDEefgPQ7";
        
        // Default constructor
        public Account() {
            userInfo = new UserInfo();
            aId = -1;
            email = "";
            password = "";
            accType = AccountType.Nil;
            fbId = "";
            fbUserName = "";
        }
        
        /// <summary>
        /// Constructor.
        /// Creates a new account with the email and password.
        /// </summary>
        /// <param name="inEmail">Email</param>
        /// <param name="inPassword">Password</param>
        public Account(string inEmail, string inPassword) : this() {
            //sqlCommands = new List<string>();
            SelfAuthenticate(inEmail, inPassword);
        }

        public Account(string inFBId) : this()
        {
            string query = "SELECT accountID FROM account WHERE fbid = @fbid";
            DBConnect db = new DBConnect();

            //Open connection
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                cmd.Parameters.AddWithValue("@fbid", inFBId);
                try
                {
                    //ExecuteScalar will return one value, null if not found
                    Object obj = cmd.ExecuteScalar();
                    if (obj != null)
                    {
                        aId = int.Parse(obj + "");
                        this.PopulateAccountInfo();
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                db.closeConnection();
            }
        }

        /// <summary>
        /// Allows the system to login without the use of a password.
        /// This should only be done when a session is available and the
        /// email is stored.
        /// </summary>
        /// <param name="inEmail"></param>
        /// <returns></returns>
        public int LoginWithoutPW(string inEmail)
        {
            userInfo = new UserInfo();
            string query = "SELECT accountID FROM account WHERE email = @email";
            DBConnect db = new DBConnect();

            int result = 0;

            //Open connection
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                cmd.Parameters.AddWithValue("@email", inEmail);
                try
                {
                    //ExecuteScalar will return one value, null if not found
                    Object obj = cmd.ExecuteScalar();
                    if (obj != null)
                    {
                        aId = int.Parse(obj + "");
                        this.PopulateAccountInfo();
                        result = 1;
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                db.closeConnection();
            }
            return result;
        }

        /// <summary>
        /// Sets the respective information in one go instead of calling all the setters.
        /// </summary>
        /// <param name="inAID">AccountID</param>
        /// <param name="inName">Name</param>
        /// <param name="inAge">Age</param>
        /// <param name="inGender">Gender</param>
        /// <param name="inAddress">Address</param>
        /// <param name="inContact">Contact</param>
        /// <param name="inAccType">Account Type</param>
        public void setInfo(int inAID, string inName, int inAge, GenderType inGender, string inAddress, string inContact, AccountType inAccType) {
            aId = inAID;
            userInfo.Name = inName;
            userInfo.Age = inAge;
            userInfo.Gender = inGender;
            userInfo.Address = inAddress;
            userInfo.Contact = inContact;
            accType = inAccType;
        }

        /// <summary>
        /// Creates an account based on the facebook id, username, name and gender information
        /// collected from facebook. Users created using this method is automatically given Creator status.
        /// </summary>
        /// <param name="inName">Facebook display name</param>
        /// <param name="inUserName">Facebook username</param>
        /// <param name="inGender">User's gender</param>
        /// <param name="inFBID">facebook identificaiton number</param>
        /// <returns></returns>
        public int CreateFacebookAccount(string inName, string inUserName, string inEmail, string inFBID)
        {
            int result = 0;
            result = CreateAccount(inEmail, "", inName, 0, GenderType.Nil, "", "", AccountType.Creator);
            fbId = inFBID;
            fbUserName = inUserName;
            DBConnect db = new DBConnect();
            //Open connection
            if (db.openConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand("UPDATE account SET fbusername=@fbusername, fbid=@fbid WHERE accountID=" + aId, db.getConnection());
                cmd.Parameters.AddWithValue("@fbusername", fbUserName);
                cmd.Parameters.AddWithValue("@fbid", fbId);
                try
                {
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
                catch (InvalidOperationException e)
                {
                    System.Diagnostics.Debug.WriteLine("Connection error: %s", e.Message);
                    // save the command to be executed later
                    //sqlCommands.Add(cmd.CommandText);
                }
                //close connection
                db.closeConnection();
                result = 1;
            }
            return 0;
        }

        /// <summary>
        /// Creates a database account with the provided information. Should only be called if registering for an account.
        /// Age, Gender and Account Type are integer values, the others are all strings.
        /// </summary>
        /// <param name="inEmail">Name</param>
        /// <param name="inPassword">Age</param>
        /// <param name="inName">Name</param>
        /// <param name="inAge">Age</param>
        /// <param name="inGender">Gender</param>
        /// <param name="inAddress">Address</param>
        /// <param name="inContact">Contact</param>
        /// <param name="inAccType">Account Type</param>
        /// <returns>0 if error, else 1</returns>
        public int CreateAccount(string inEmail, string inPassword, string inName, int inAge, GenderType inGender, 
            string inAddress, string inContact, AccountType inAccType)
        {
            DBConnect db = new DBConnect();
            int result = 0;
            int id = -1;

            //Open connection
            if (db.openConnection() == true)
            {
                email = inEmail;
                password = Account.MakeHashString(inPassword);
                aId = id;
                FBId = "";
                fbUserName = "";
                accType = inAccType;

                string queryAcc = "INSERT INTO account (email, password, type, fbid, fbusername)" +
                            " VALUES (@email, @password, @acctype, @fbid, @fbusername)";
                string queryInfo = "INSERT INTO userinfo (address, age, contact, gender, name, userInfoAccountID)" +
                                " VALUES (@address, @age, @contact, @gender, @name, @id)";

                // check for existing email
                id = Account.GetAccountID(inEmail);
                if (id != -1)
                {
                    System.Diagnostics.Debug.WriteLine("Account exists.");
                    return 0;
                }

                MySqlCommand cmd2 = new MySqlCommand(queryAcc, db.getConnection());
                cmd2.Parameters.Clear();
                cmd2.Parameters.AddWithValue("@email", inEmail);
                cmd2.Parameters.AddWithValue("@password", Account.MakeHashString(inPassword));
                cmd2.Parameters.AddWithValue("@acctype", (int)inAccType);
                cmd2.Parameters.AddWithValue("@fbid", fbId);
                cmd2.Parameters.AddWithValue("@fbusername", fbUserName);
                try
                {
                    //Execute command
                    cmd2.ExecuteNonQuery();
                }
                catch (InvalidOperationException e)
                {
                    System.Diagnostics.Debug.WriteLine("Connection error: %s", e.Message);
                    // save the command to be executed later
                    //sqlCommands.Add(cmd.CommandText);
                }

                // get the corresponding account id from the newly created account in account table
                id = Account.GetAccountID(inEmail);

                MySqlCommand cmd3 = new MySqlCommand(queryInfo, db.getConnection());
                cmd3.Parameters.Clear();
                cmd3.Parameters.AddWithValue("@id", id);
                cmd3.Parameters.AddWithValue("@name", inName);
                cmd3.Parameters.AddWithValue("@age", inAge);
                cmd3.Parameters.AddWithValue("@gender", (int)inGender);
                cmd3.Parameters.AddWithValue("@address", inAddress);
                cmd3.Parameters.AddWithValue("@contact", inContact);

                try
                {
                    //Execute command
                    cmd3.ExecuteNonQuery();
                }
                catch (InvalidOperationException e)
                {
                    System.Diagnostics.Debug.WriteLine("Connection error: %s", e.Message);
                    // save the command to be executed later
                    //sqlCommands.Add(cmd.CommandText);
                }
                //close connection
                db.closeConnection();
                result = 1;
            }
            this.setInfo(id, inName, inAge, inGender, inAddress, inContact, inAccType);
            return result;
        }

        /// <summary>
        /// Populates account class information from database.
        /// </summary>
        /// <returns></returns>
        public int PopulateAccountInfo()
        {
            int result = 0;
            
        	if (aId == -1) return -1;
            
            DBConnect db = new DBConnect();

            string queryAcc = "SELECT * FROM account WHERE accountID="+aId;
            string queryInfo = "SELECT * FROM userinfo WHERE userInfoAccountID=" + aId;
            //Open connection
            if (db.openConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(queryAcc, db.getConnection());
                MySqlCommand cmd1 = new MySqlCommand(queryInfo, db.getConnection());

                try
                {
                    //Execute queryAcc command
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    if (!dataReader.HasRows) return result;
                    if (dataReader.Read())
                    {
                        accType = (AccountType)dataReader.GetInt32("type");
                        email = dataReader.GetString("email");
                        password = dataReader.GetString("password");
                        fbId = dataReader.GetString("fbid");
                        fbUserName = dataReader.GetString("fbusername");
                    }
                    dataReader.Close();

                    //Execute queryInfo command
                    //cmd.CommandText = queryInfo;
                    dataReader = null;
                    dataReader = cmd1.ExecuteReader();
                    if (!dataReader.HasRows) return result;
                    if (dataReader.Read())
                    {
                        userInfo.Name = dataReader.GetString("name");
                        userInfo.Age = dataReader.GetInt32("age");
                        userInfo.Gender = (GenderType)dataReader.GetInt32("gender");
                        userInfo.Address = dataReader.GetString("address");
                        userInfo.Contact = dataReader.GetString("contact");
                    }
                    result = 1;
                    dataReader.Close();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }

                //close connection
                db.closeConnection();
                result = 1;
            }
            return result;
        }

        /// <summary>
        /// Updates the various fields for the table. Field type depends on the ENUM DBDataType.
        /// All field values in string format.
        /// Specific string values for Gender: Male & Female
        /// Specific string values for AccType: Admin, Creator & Surveyor
        /// </summary>
        /// <param name="inType">Field Type</param>
        /// <param name="inStream">Field Value</param>
        /// <returns>0 if error, else 1</returns>
        public int UpdateAccount(DBDataType inType, string inStream)
        {
            int result = 0;
            int temp = 0;
            
            if (aId == -1) return -1;
            
            DBConnect db = new DBConnect();
            //Open connection
            if (db.openConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand("", db.getConnection());

                // Need to include '' for string fields, integer fields leave it without.
                switch (inType)
                {
                    case DBDataType.Email:
                        cmd.CommandText = "UPDATE account SET email=@stream WHERE accountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", inStream);
                        this.email = inStream;
                        break;
                    case DBDataType.Password:
                        if (password == inStream || password == "") break;
                        cmd.CommandText = "UPDATE account SET password=@stream WHERE accountID=" + aId;
                        string hashPassword = Account.MakeHashString(inStream);
                        cmd.Parameters.AddWithValue("@stream", hashPassword);
                        this.password = hashPassword;
                        break;
                    case DBDataType.AccType:
                        temp = (int)(inStream.Equals("admin") ? AccountType.Admin : AccountType.Creator);
                        cmd.CommandText = "UPDATE account SET type=@stream WHERE accountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", temp);
                        this.accType = (AccountType)temp;
                        break;
                    case DBDataType.Name:
                        cmd.CommandText = "UPDATE userinfo SET name=@stream WHERE userInfoAccountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", inStream);
                        this.Name = inStream;
                        break;
                    case DBDataType.Age:
                        temp = int.Parse(inStream);
                        cmd.CommandText = "UPDATE userinfo SET age=@stream WHERE userInfoAccountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", temp);
                        this.Age = temp;
                        break;
                    case DBDataType.Gender:
                        temp = (int)(inStream.Equals("Male") ? GenderType.Male : GenderType.Female);
                        cmd.CommandText = "UPDATE userinfo SET gender=@stream WHERE userInfoAccountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", temp);
                        this.Gender = (GenderType)temp;
                        break;
                    case DBDataType.Address:
                        cmd.CommandText = "UPDATE userinfo SET address=@stream WHERE userInfoAccountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", inStream);
                        this.Address = inStream;
                        break;
                    case DBDataType.Contact:
                        cmd.CommandText = "UPDATE userinfo SET contact=@stream WHERE userInfoAccountID=" + aId;
                        cmd.Parameters.AddWithValue("@stream", inStream);
                        this.Contact = inStream;
                        break;
                    default:
                        return result;
                }

                try
                {
                    //Execute command
                    cmd.ExecuteNonQuery();
                    result = 1;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                finally
                {
                    //close connection
                    db.closeConnection();
                }
            }
           
            return result;
        }

        /// <summary>
        /// Updates all the account info into database.
        /// </summary>
        /// <returns></returns>
        public int UpdateAccount()
        {
            int result = 0;

            if (aId == -1) return -1;

            DBConnect db = new DBConnect();
            //Open connection
            if (db.openConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand("UPDATE account SET "+
                    "email=@email, password=@password, type=@type, fbid=@fbid, fbusername=@fbusername"+
                    " WHERE accountID=" + aId, db.getConnection());
                cmd.Parameters.AddWithValue("@email", this.email);
                cmd.Parameters.AddWithValue("@password", this.password);
                cmd.Parameters.AddWithValue("@type", (int)this.accType);
                cmd.Parameters.AddWithValue("@fbid", this.fbId);
                cmd.Parameters.AddWithValue("@fbusername", this.fbUserName);
                MySqlCommand cmd2 = new MySqlCommand("UPDATE userinfo SET "+
                    "name=@name, gender=@gender, age=@age, address=@address, contact=@contact"+
                    " WHERE userInfoAccountID=" + aId, db.getConnection());
                cmd2.Parameters.AddWithValue("@name", this.userInfo.Name);
                cmd2.Parameters.AddWithValue("@gender", (int)this.userInfo.Gender);
                cmd2.Parameters.AddWithValue("@age", this.userInfo.Age);
                cmd2.Parameters.AddWithValue("@address", this.userInfo.Address);
                cmd2.Parameters.AddWithValue("@contact", this.userInfo.Contact);

                try
                {
                    //Execute command
                    cmd.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    result = 1;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                finally
                {
                    //close connection
                    db.closeConnection();
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes the account based on Account ID.
        /// </summary>
        /// <returns>0 if error, else 1</returns>
        public int DeleteAccount() {
            DBConnect db = new DBConnect();
            int result = 0;
            string queryAcc = "DELETE FROM account WHERE accountID=@id";
            //string queryUserInfo = "DELETE FROM userinfo WHERE userInfoAccountID=@id";
            //Open connection
            if (db.openConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(queryAcc, db.getConnection());
                cmd.Parameters.AddWithValue("@id", aId);
                //MySqlCommand cmd2 = new MySqlCommand(queryUserInfo, db.getConnection());
                //cmd2.Parameters.AddWithValue("@id", aId);

                try {
                    //Execute command
                    cmd.ExecuteNonQuery();
                    System.Diagnostics.Debug.WriteLine("Deleting account: " + aId);
                }
                catch (Exception e) {
                    System.Diagnostics.Debug.WriteLine("Unable to delete: " + e.Message);
                }

                //close connection
                db.closeConnection();
                result = 1;
            }

            return result;
        }

        /// <summary>
        /// Authenticates email and password
        /// </summary>
        /// <param name="inAccount"></param>
        /// <returns>Returns Account Id if valid, 0 if invalid.</returns>
        private int SelfAuthenticate(string inEmail, string inPassword)
        {
            string query = "SELECT accountID FROM account WHERE email = @email AND password = @password";
            DBConnect db = new DBConnect();

            string hashPassword = Account.MakeHashString(inPassword);

            //Open connection
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                cmd.Parameters.AddWithValue("@email", inEmail);
                cmd.Parameters.AddWithValue("@password", hashPassword);
                try
                {
                    //ExecuteScalar will return one value, null if not found
                    Object result = cmd.ExecuteScalar();
                    // if account exists and correct.
                    if (result != null)
                    {
                        aId = int.Parse(result + "");
                        PopulateAccountInfo();
                    }
                    else return 0;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                db.closeConnection();
            }
            return 1;
        }

        /// <summary>
        /// Checks if the account is an admin account
        /// </summary>
        /// <returns></returns>
        public bool IsAdmin()
        {
            //return false;
            return AccType == AccountType.Admin;
        }

        /// <summary>
        /// Authenticates with email and password
        /// </summary>
        /// <param name="inEmail"></param>
        /// <param name="inPassword"></param>
        /// <returns>return 0 if false 1 if true.</returns>
        public static bool IsAdmin(string inEmail)
        {
            bool result = false;
            string query = "SELECT type FROM account WHERE email = @email";
            DBConnect db = new DBConnect();

            Object obj = null;

            //Open connection
            if (db.openConnection() == true)
            {
                try
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                    cmd.Parameters.AddWithValue("@email", inEmail);
                    //ExecuteScalar will return one value, null if not found
                    obj = cmd.ExecuteScalar();
                    System.Diagnostics.Debug.WriteLine("Admin: " + obj);

                    
                    if (obj != null)
                    {
                        if (int.Parse(obj + "") == (int)AccountType.Admin) 
                            result = true;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Account " + inEmail + " not admin.\n");
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Admin check Fail: " + e.Message);
                }
                db.closeConnection();
            }
            return result;
        }

        /// <summary>
        /// Authenticates with email and password
        /// </summary>
        /// <param name="inEmail"></param>
        /// <param name="inPassword"></param>
        /// <returns>return 0 if false 1 if true.</returns>
        public static int Authenticate(string inEmail, string inPassword)
        {
            if (inPassword == "") return 0;
            int result = 0;
            string query = "SELECT accountID FROM account WHERE email = @email AND password = @password";
            //string queryFB = "SELECT fbid FROM account WHERE accountID=@accountid";
            DBConnect db = new DBConnect();

            string hashPassword = Account.MakeHashString(inPassword);
            System.Diagnostics.Debug.WriteLine("PW: " + inPassword + ", HPW: " + hashPassword);
            Object obj = null;

            //Open connection
            if (db.openConnection() == true)
            {
                try
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                    cmd.Parameters.AddWithValue("@email", inEmail);
                    cmd.Parameters.AddWithValue("@password", hashPassword);
                    //ExecuteScalar will return one value, null if not found
                    obj = cmd.ExecuteScalar();
                    System.Diagnostics.Debug.WriteLine("Obj: " + obj);
                    if (obj != null)
                    {
                        //_tempAid = int.Parse(obj + "");
                        //System.Diagnostics.Debug.WriteLine("Temp account id: "+_tempAid);
                        //cmd.CommandText = queryFB;
                        //cmd.Parameters.Clear();
                        //cmd.Parameters.AddWithValue("@accountid", _tempAid);
                        ////ExecuteScalar will return one value, null if not found
                        //obj = cmd.ExecuteScalar();
                        //// chcek if the fb id is available, if it is, password field will be empty,
                        //// so check returns false
                        //if (obj + "" != "" && inPassword != "") result = 1;
                        result = 1;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Account "+inEmail+" not found.\n");
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Auth Fail: " + e.Message);
                }
                db.closeConnection();
            }
            return result;
        }

        /// <summary>
        /// Authenticates with a facebook id number
        /// </summary>
        /// <param name="inFBID">facebook identificaiton number</param>
        /// <returns>Returns 1 if valid, 0 if invalid.</returns>
        public static int AuthenticateFB(String inFBId)
        {
            int result = 0;
            string query = "SELECT accountID FROM account WHERE fbid = @fbid";
            DBConnect db = new DBConnect();

            //Open connection
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                cmd.Parameters.AddWithValue("@fbid", inFBId);
                try
                {
                    //ExecuteScalar will return one value, null if not found
                    Object obj = cmd.ExecuteScalar();
                    if (obj != null) result = 1;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
                db.closeConnection();
            }
            return result;
        }
        
        /// <summary>
        /// Updates the password of the specified email address.
        /// </summary>
        /// <param name="inEmail"></param>
        /// <param name="inPassword"></param>
        /// <returns>1 if successful, 0 if unsuccessful</returns>
        public static int UpdatePassword(string inEmail, string inPassword) {
        	int result = 0;
            int _tempID = Account.GetAccountID(inEmail);
            if (_tempID == -1) return 0;
        	
            DBConnect db = new DBConnect();
            string hashPassword = Account.MakeHashString(inPassword);

            //Open connection
            if (db.openConnection() == true)
            {
                string query = "UPDATE account SET password=@password WHERE accountID=" + _tempID;
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, db.getConnection());
                cmd.Parameters.AddWithValue("@password", hashPassword);

				try
                {
                    //Execute command
                    cmd.ExecuteNonQuery();
                    result = 1;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    result = -1;
                }

                //close connection
                db.closeConnection();
            }
            return result;
        }

        /// <summary>
        /// Sends a password reset email to the user.
        /// </summary>
        /// <param name="inEmail"></param>
        /// <returns>0 if email does not exist in database, 1 if it does.</returns>
        public static int SendPWEmail(string inEmail)
        {
            if (Account.GetAccountID(inEmail) == -1) return 0;

            Random rand = new Random(DateTime.Now.Millisecond * DateTime.Now.Minute + DateTime.Now.Hour);
            string randomString = "";
            char[] emailArray = inEmail.ToCharArray();
            int j = 0;
            for (int i = 0; i < 15; i++)
            {
                j = rand.Next()%62;
                randomString += hashString[j];
               
            }
            System.Diagnostics.Debug.WriteLine("RS: " + randomString);
            Account.UpdatePassword(inEmail, randomString);

            string body = "** This is an automated message -- please do not reply as you will not receive a response. **\n\n" +
                "This message is sent to you from the NTU Survey System in response to your request to reset your account password. Please click the link below and follow the instructions to reset your password.\n\n" +
                "Your username is: " + inEmail + "\n" +
                "Your new password is: " + randomString + "\n" +
                "If you experience problems clicking the link above, please try copying and pasting the entire link into a browser.\n\n" +
                "For help go to:\nhttp://www.sofadev.com/help/\n\n" +
                "Thank you,\nSofaDev";
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage("ntusurveysofadev@gmail.com", inEmail, "Password Reset", body);
            message.IsBodyHtml = false;
            System.Net.NetworkCredential mailAuth = new System.Net.NetworkCredential("ntusurveysofadev@gmail.com", "cz2006sofadev");
            System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            mailClient.EnableSsl = true;
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = mailAuth;
            mailClient.Send(message);
            return 1;
        }
        
        /// <summary>
        /// Gets the account id from the specified email
        /// </summary>
        /// <param name="inEmail"></param>
        /// <returns>-1 if no such email account exists</returns>
        public static int GetAccountID(string inEmail) {
        	int result = -1;
            DBConnect db = new DBConnect();

            //Open connection
            if (db.openConnection() == true)
            {
            	string checkQuery = "SELECT accountID FROM account WHERE email = @email";
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(checkQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@email", inEmail);
				
				try {
					Object obj = cmd.ExecuteScalar();
					if (obj != null)
						result = int.Parse(obj+"");
					else 
						result = -1;
				}
				catch (Exception e) {
                    System.Diagnostics.Debug.WriteLine(e.Message);
				}
            }
            db.closeConnection();
            return result;
        }

        /// <summary>
        /// Gets the account id from the specified email
        /// </summary>
        /// <param name="inFBID">facebook identificaiton number</param>
        /// <returns>-1 if no such email account exists</returns>
        public static int GetAccountIDForFB(string inFBID)
        {
            int result = 0;
            DBConnect db = new DBConnect();

            //Open connection
            if (db.openConnection() == true)
            {
                string checkQuery = "SELECT accountID FROM account WHERE fbid = @inFBID";
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(checkQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@inFBID", inFBID);

                try
                {
                    Object obj = cmd.ExecuteScalar();
                    if (obj != null)
                        result = int.Parse(obj + "");
                    else
                        result = -1;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
            db.closeConnection();
            return result;
        }

        /// <summary>
        /// Returns a hashed string fron an input string
        /// </summary>
        /// <param name="inString"></param>
        /// <returns>hashed string</returns>
        public static string MakeHashString(string inString)
        {
            if (inString == null) return null;
            char[] charArray = inString.ToCharArray();
            string newString = "";
            for (int i = 0; i < inString.Length; i++)
            {
                    
                // for lower case
                //if (charArray[i] - 'a' <= 26)
                if (System.Text.RegularExpressions.Regex.IsMatch(charArray[i]+"", @"^[a-z]"))
                {
                    newString += hashString[charArray[i] - 'a'] + "";
                }
                // for upper case
                //else if (charArray[i] - 'A' <= 26)
                else if (System.Text.RegularExpressions.Regex.IsMatch(charArray[i]+"", @"^[A-Z]"))
                {
                    newString += hashString[charArray[i] - 'A' + 26] + "";
                }
                // for numbers
                //else if (charArray[i] - '0' <= 10)
                else if (System.Text.RegularExpressions.Regex.IsMatch(charArray[i] + "", @"^[0-9]"))
                {
                    newString += hashString[charArray[i] - '0' + 52] + "";
                }
                // for everything else
                else
                {
                    int drop = charArray[i];
                    int dropped = 0;
                    while (drop > 0)
                    {
                        dropped += drop % 10;
                        drop /= 10;
                    }
                    newString += hashString[dropped];
                }
            }
            return newString;
        }


        // Getter and Setter methods
        public int AId
        {
            get { return aId; }
            set { aId = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public AccountType AccType
        {
            get { return accType; }
            set { accType = value; }
        }

        public string FBId
        {
            get { return fbId; }
            set { fbId = value; }
        }

        public string FBUserName
        {
            get { return fbUserName; }
            set { fbUserName = value; }
        }

        public UserInfo getUserInfo()
        {
            return userInfo;
        }

        public string Name
        {
            get { return userInfo.Name; }
            set { userInfo.Name = value; }
        }

        public GenderType Gender
        {
            get { return userInfo.Gender; }
            set { userInfo.Gender = value; }
        }

        public string Contact
        {
            get { return userInfo.Contact; }
            set { userInfo.Contact = value; }
        }

        public string Address
        {
            get { return userInfo.Address; }
            set { userInfo.Address = value; }
        }

        public int Age
        {
            get { return userInfo.Age; }
            set { userInfo.Age = value; }
        }
    }
}