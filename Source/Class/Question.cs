﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace SofaDev.Class
{
    public class Question
    {
        private int questionID;
        private string title;
        private int questionTypeID;
        private List<QuestionOption> questionOption;

        // Setter Methods
        public void setQuestionID(int questionID) { this.questionID = questionID; }
        public void setTitle(string title) { this.title = title; }
        public void setQuestionTypeID(int questionTypeID) { this.questionTypeID = questionTypeID; }

        // Getter Methods
        public List<QuestionOption> getquestionOption() { return questionOption; }
        public int getQuestionID() { return questionID; }
        public string getTitle() { return title; }
        public int getquestionTypeID() { return questionTypeID; }

        public Question()
        {
            questionID = -1;
        }

        public Question(string title, int questionTypeID)
        {
            //Used for Create Survey Page
            this.title = title;
            this.questionTypeID = questionTypeID;
        }

        public Question(int questionID)
        {
            this.questionID = questionID;
            GetQuestionData();
            GetQuestionOptionData();
        }
        private void GetQuestionOptionData()
        {
            QuestionOption qnsOption;
            questionOption = new List<QuestionOption>();
            string stringQuery = "SELECT * FROM questionoption WHERE questionID= @questionID";
            DBConnect db = new DBConnect();
            //Open connection
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                //Create a data reader and Execute the command
                cmd.Parameters.AddWithValue("@questionID", questionID);
                dataReader = cmd.ExecuteReader();
                //Read the data
                while (dataReader.Read())
                {
                    // Create new object to 
                    qnsOption = new QuestionOption();
                    qnsOption.setquestionOptionID(dataReader.GetInt32(dataReader.GetOrdinal("questionOptionID")));
                    qnsOption.setqOption(dataReader.GetString(dataReader.GetOrdinal("qOption")));
                    qnsOption.setquestionID(getQuestionID());
                    questionOption.Add(qnsOption);
                }
                //close Data Reader
                dataReader.Close();
                //close Connection
                db.closeConnection();
            }
        }
        private void GetQuestionData()
        {
            string stringQuery = "Select * FROM Question WHERE questionID= @questionID";
            DBConnect db = new DBConnect();
            MySqlDataReader dataReader = null;
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@questionID", questionID);
                dataReader = cmd.ExecuteReader();
                try
                {
                    while (dataReader.Read())
                    {
                        setTitle(dataReader.GetString(dataReader.GetOrdinal("title")));
                        setQuestionTypeID(dataReader.GetInt32(dataReader.GetOrdinal("questionTypeID")));
                    }
                    //close Data Reader
                    dataReader.Close();
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

            }
            //close Connection
            db.closeConnection();
        }

        public void createQuestion()
        {
            string stringQuery = "INSERT INTO Question(title,questionTypeID)";
            stringQuery += " VALUES (@title, @questionTypeID); SELECT LAST_INSERT_ID();";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@questionTypeID", questionTypeID);
                try
                {
                    questionID = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
                db.closeConnection();
            }
        }
    }
}