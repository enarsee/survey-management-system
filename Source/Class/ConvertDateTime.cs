﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.Common;

namespace SofaDev.Class
{
    public class ConvertDateTime
    {
        public DateTime convertDate(DateTime dateTime)
        {
            return DateTime.Parse(dateTime.ToString("dd/MM/yyyy hh:mm:ss tt"));
        }
    }
}