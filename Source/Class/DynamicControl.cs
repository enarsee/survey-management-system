﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace SofaDev.Class
{
    public class DynamicControl
    {
        public CheckBox CreateCheckBox(int ID)
        {
            CheckBox check = new CheckBox();
            check.ID = "chkBox" + ID;
            return check;
        }
        public Label CreateLabel(int ID, string text)
        {
            Label label = new Label();
            label.ID = "lbl" + ID;
            label.Text = text;
            return label;
        }

        public HiddenField CreateHiddenField(int ID, int type)
        {
            HiddenField hidden = new HiddenField();
            hidden.ID = "hidden" + ID;
            hidden.Value = ID.ToString() + "_" + type.ToString();
            return hidden;
        }

        public TextBox CreateTextBox(int ID)
        {
            TextBox text = new TextBox();
            text.ID = "txtBox" + ID;
            return text;
        }
        public TextBox CreateTextArea(int ID)
        {
            TextBox text1 = new TextBox();
            text1.ID = "textArea" + ID;
            text1.TextMode = TextBoxMode.MultiLine;
            text1.Width = new Unit(260);
            text1.Height = new Unit(100);
            return text1;
        }
        public RadioButtonList CreateRadioButtonList(int ID, List<QuestionOption> questionOption)
        {
            RadioButtonList radio = new RadioButtonList();
            radio.ID = "radBtnLst" + ID;
            for (int i = 0; i < questionOption.Count; i++)
            {
                radio.Items.Add(new ListItem(questionOption[i].getqOption(), i.ToString()));
            }
            return radio;
        }

        public DropDownList CreateDropDownList(int ID, List<QuestionOption> questionOption)
        {
            DropDownList drop = new DropDownList();
            drop.ID = "ddl" + ID;
            for (int i = 0; i < questionOption.Count; i++)
            {
                drop.Items.Add(new ListItem(questionOption[i].getqOption(), i.ToString()));
            }
            return drop;
        }

        public CheckBoxList CreateCheckBoxList(int ID, List<QuestionOption> questionOption)
        {
            CheckBoxList boxlist = new CheckBoxList();
            boxlist.ID = "boxList" + ID;
            for (int i = 0; i < questionOption.Count; i++)
            {
                boxlist.Items.Add(new ListItem(questionOption[i].getqOption(), i.ToString()));
            }
            return boxlist;
        }
        public Button CreateButton(int ID, string title)
        {
            Button newButton = new Button();
            newButton.ID = "btn" + ID;
            newButton.Text = title;
            newButton.CssClass = "deletebutton";
            return newButton;
        }
    }

}