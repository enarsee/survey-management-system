﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace SofaDev.Class
{
    public class QuestionOption
    {
        private int questionOptionID;
        private string qOption;
        private int questionID;
        public QuestionOption() { }

        public QuestionOption(string qOption)
        {
            this.qOption = qOption;
        }
        public QuestionOption(int optionID, string qOption)
        {
            this.questionOptionID = optionID;
            this.qOption = qOption;

        }

        public QuestionOption(int optionID, string qOption, int questionID)
        {
            this.questionOptionID = optionID;
            this.qOption = qOption;
            this.questionID = questionID;
        }

        // Setter Methods
        public void setquestionOptionID(int questionOptionID) { this.questionOptionID = questionOptionID; }
        public void setqOption(string qOption) { this.qOption = qOption; }
        public void setquestionID(int questionID) { this.questionID = questionID; }

        // Getter Methods
        public int getquestionOptionID() { return questionOptionID; }
        public string getqOption() { return qOption; }
        public int getquestionID() { return questionID; }

        public void GetQuestionOptionData()
        {
        }

        public void createOption()
        {
            string stringQuery = "INSERT INTO questionoption(qOption,questionID)";
            stringQuery += " VALUES (@qOption, @questionID); SELECT LAST_INSERT_ID();";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {

                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@qOption", qOption);
                cmd.Parameters.AddWithValue("@questionID", questionID);
                try
                {
                    questionOptionID = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
                db.closeConnection();
            }
        }
    }
}