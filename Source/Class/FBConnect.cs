﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Security;

namespace SofaDev.Class
{
    public class FBConnect : System.Web.UI.Page
    {
        readonly static string App_ID = "126293897522734";
        readonly static string App_Secret = "42fbc84ca6306b18ef0e1173a5d3f771";
        readonly static string scope = "email";
        static string token = "";

        readonly static DataContractJsonSerializer userSerializer
            = new DataContractJsonSerializer(typeof(FacebookUser));

        public AccountController CheckFB(Page p, EventArgs e, HyperLink fbLogin, AccountController ac, string RedirectURL)
        {
            //base.OnLoad(e);
            if (!p.IsPostBack)
            {
                string code = p.Request.QueryString["code"];
                string error_description = p.Request.QueryString["error_description"];
                string originalReturnUrl = p.Request.QueryString["ReturnUrl"]; // asp.net logon param
                Uri backHereUri = p.Request.Url; // modify for dev server

                if (!User.Identity.IsAuthenticated)
                {
                    if (!string.IsNullOrEmpty(code)) // user is authenticated
                    {
                        System.Diagnostics.Debug.WriteLine("Login Facebook");
                        FacebookUser me = GetUserDetails(code, backHereUri);
                        if (me == null) return null;
                        if (ac == null) ac = new AccountController();                    
                        ac.FacebookLogin(me);
                        Session["account"] = ac;
                        p.Response.Redirect(RedirectURL);
                        FormsAuthentication.SetAuthCookie(ac.getName(), false);
                    }
                }
                else
                {
                    ac.Logout();
                    Session["account"] = null;
                    p.Response.Redirect(RedirectURL);
                    FormsAuthentication.SignOut();
                }
                if (!string.IsNullOrEmpty(error_description)) // user propably disallowed
                {
                    OnError(error_description);
                }
                //fbLogin.Visible = !User.Identity.IsAuthenticated;
                fbLogin.Visible = Session["account"] == null;
                fbLogin.NavigateUrl = string.Format(
                    CultureInfo.InvariantCulture,
                    "https://www.facebook.com/dialog/oauth?"
                    + "client_id={0}&scope={1}&redirect_uri={2}",
                    App_ID,
                    scope,
                    Uri.EscapeDataString(backHereUri.OriginalString));
            }
            return ac;
        }

        private FacebookUser GetUserDetails(string code, Uri backHereUri)
        {
            Uri getTokenUri = new Uri(
                string.Format(
                CultureInfo.InvariantCulture,
                "https://graph.facebook.com/oauth/access_token?"
                + "client_id={0}&client_secret={1}&code={2}&redirect_uri={3}",
                App_ID,
                App_Secret,
                Uri.EscapeDataString(code),
                Uri.EscapeDataString(backHereUri.OriginalString))
                );
            using (var wc = new WebClient())
            {
                try
                {
                    token = wc.DownloadString(getTokenUri);
                    //System.Diagnostics.Debug.WriteLine("Token: " + token + ", \nURI: " + getTokenUri.ToString());
                    Uri getMeUri = new Uri(
                        string.Format(
                        CultureInfo.InvariantCulture,
                        "https://graph.facebook.com/me?{0}",
                        token)
                        );
                    using (var ms = new MemoryStream(wc.DownloadData(getMeUri)))
                    {
                        var me = (FacebookUser)userSerializer.ReadObject(ms);
                        return me;
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
                return null;
            }
        }

        private void OnError(string error_description)
        {
            Controls.Add(new Label()
            {
                CssClass = "oauth-error",
                Text = error_description
            });
        }

        //public static bool isConnected()
        //{
        //    return (SessionKey != null && UserID != -1);
        //}
        //public static string ApiKey
        //{
        //    get { return App_ID; }
        //}
        //public static string SecretKey
        //{
        //    get { return App_Secret; }
        //}
        //public static string SessionKey
        //{
        //    get { return GetFacebookCookie("session_key"); }
        //}
        //public static int UserID
        //{
        //    get
        //    {
        //        int userID = -1;
        //        int.TryParse(GetFacebookCookie("user"), out userID);
        //        return userID;
        //    }
        //}
        //private static string GetFacebookCookie(string cookieName)
        //{
        //    string retString = null;
        //    string fullCookie = ApiKey + "_" + cookieName;
        //    if (HttpContext.Current.Request.Cookies[fullCookie] != null)
        //        retString = HttpContext.Current.Request.Cookies[fullCookie].Value;
        //    return retString;
        //}
    }

    [Serializable]
    public class FacebookUser
    {
        public long id;
        public string name;
        public string first_name;
        public string last_name;
        public string link;
        public string email;
        public string timezone;
        public string locale;
        public bool verified;
        public string updated_time;
    }
}