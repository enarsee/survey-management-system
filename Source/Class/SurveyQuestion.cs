﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace SofaDev.Class
{
    public class SurveyQuestion
    {
        // Variables
        private int surveryQuestionID;
        private int sqSurveyID;
        private int sqQuestionID;

        // Default Constructor
        public SurveyQuestion() { }
        //Get SurveyQuestion ID
        public SurveyQuestion(int sqSurveyID, int sqQuestionID)
        {
            this.sqSurveyID = sqSurveyID;
            this.sqQuestionID = sqQuestionID;
        }
        //Get QuestionID
        public SurveyQuestion(int sqSurveyID)
        {
            this.sqSurveyID = sqSurveyID;
        }
        // Setter Methods
        public void setsurveryQuestionID(int surveryQuestionID) { this.surveryQuestionID = surveryQuestionID; }
        public void setsqSurveyID(int sqSurveyID) { this.sqSurveyID = sqSurveyID; }
        public void setsqQuestionID(int sqQuestionID) { this.sqQuestionID = sqQuestionID; }

        // Getter Methods
        public int getsurveryQuestionID() { return surveryQuestionID; }
        public int getsqSurveyID() { return sqSurveyID; }
        public int getsqQuestionID() { return sqQuestionID; }

        //Add Survery Question
        public void IntegerCreateSurveyQuestion()
        {
            string stringQuery = "INSERT INTO surveyquestion(sqSurveyID, sqQuestionID) VALUES (@surveryID, @questionID);  SELECT LAST_INSERT_ID();";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@surveryID", sqSurveyID);
                cmd.Parameters.AddWithValue("@questionID", sqQuestionID);
                try
                {
                    surveryQuestionID = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

            }
            db.closeConnection();
        }

        public int createSurveyQuestion()
        {
            string stringQuery = "INSERT INTO surveyquestion(sqSurveyID, sqQuestionID) VALUES (@surveryID, @questionID);  SELECT LAST_INSERT_ID();";
            int validate = -1;
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@surveryID", sqSurveyID);
                cmd.Parameters.AddWithValue("@questionID", sqQuestionID);
                try
                {
                    surveryQuestionID = int.Parse(cmd.ExecuteScalar().ToString());
                    validate = 0;
                }
                catch (Exception ex)
                {
                    validate = 1;
                    string error = ex.Message;
                }

            }
            db.closeConnection();
            return validate;
        }

        public DataTable GetAllQuestionID()
        {
            string stringQuery = "Select sqQuestionID FROM surveyQuestion WHERE sqSurveyID=@surveyID";
            DBConnect db = new DBConnect();
            DataSet ds = new DataSet();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.Parameters.AddWithValue("@surveyID", sqSurveyID);
                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                try
                {
                    adapter.Fill(ds, "QuestionID");
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

            }
            db.closeConnection();
            DataTable table = ds.Tables["QuestionID"];
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow myRow = table.Rows[i];
                int MyValue = (int)myRow["sqQuestionID"];
            }
            return ds.Tables["QuestionID"];

        }
        //Get Survery Question ID base on Survey and Question ID
        public void GetSurveyQuestionID()
        {
            string stringQuery = "Select surveyQuestionID FROM surveyquestion WHERE sqSurveyID= @surveyID AND sqQuestionID= @questionID";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@surveyID", sqSurveyID);
                cmd.Parameters.AddWithValue("@questionID", sqQuestionID);
                try
                {
                    surveryQuestionID = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }

            }
            db.closeConnection();
        }

        public void DeleteSurveyQuestion()
        {
            string stringQuery = "DELETE FROM surveyquestion WHERE sqQuestionID = @questionID AND sqSurveyID = @surveyID";
            DBConnect db = new DBConnect();
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@questionID", sqQuestionID);
                cmd.Parameters.AddWithValue("@surveyID", sqSurveyID);

                cmd.ExecuteNonQuery();
            }
            db.closeConnection();
        }

        public void DeleteSurveyQuestionResults()
        {
            string stringQuery = "SELECT surveyQuestionID FROM surveyquestion WHERE sqQuestionID = @questionID AND sqSurveyID = @surveyID";
            DBConnect db = new DBConnect();
            MySqlDataReader dr = null;
            int sqID = 0;
            if (db.openConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(stringQuery, db.getConnection());
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@questionID", sqQuestionID);
                cmd.Parameters.AddWithValue("@surveyID", sqSurveyID);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    sqID = dr.GetInt32(dr.GetOrdinal("surveyQuestionID"));
                }
                dr.Close();
                stringQuery = "DELETE FROM result WHERE rSurveyQuestionID = @surveyQuestionID";

                MySqlCommand cmd1 = new MySqlCommand(stringQuery, db.getConnection());
                cmd1.CommandType = CommandType.Text;
                cmd1.Parameters.AddWithValue("@surveyQuestionID", sqID);

                cmd1.ExecuteNonQuery();
            }
            db.closeConnection();
        }
    }
}