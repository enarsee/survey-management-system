﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Security;

namespace SofaDev
{
    public partial class Login : System.Web.UI.Page
    {
        static AccountController ac1 = new AccountController();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Admin aAdmin = new Admin();
            //foreach (Account a in aAdmin.AccountList) {
            //    System.Diagnostics.Debug.WriteLine(a.Name);
            //    System.Diagnostics.Debug.WriteLine(a.Password);
            //    System.Diagnostics.Debug.WriteLine(a.Email);
            //}
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            //ac1 = new AccountController();
            // Call controller and pass in email and password
            if (ac1.Login(TextBox1.Text,TextBox2.Text) == 1)
            {
                Session["account"] = ac1;
                //Session["email"] = ac1.getEmail();
                Response.Redirect("ViewAllSurvey.aspx");
            }
            else
                Response.Redirect("Default.aspx?error=0");
        }

        protected void Forget_Click(object sender, EventArgs e)
        {
            
                Response.Redirect("Forgotpwd.aspx");
          
        }

        // FB checker function
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            FBConnect fbConnect = new FBConnect();
            fbConnect.CheckFB(this, e, fbLogin, ac1, "ViewAllSurvey.aspx");
        }
    }
}