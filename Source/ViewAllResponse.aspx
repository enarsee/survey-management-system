﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="ViewAllResponse.aspx.cs" Inherits="SofaDev.ViewAllResponse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <asp:GridView ID="GridView1" runat="server" CssClass="gridview" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField SelectText="View" ShowSelectButton="True" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblMsg" runat="server" BackColor="Transparent" ForeColor="Red" Text="There are no responses"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="viewSurveyBtn" runat="server" OnClick="viewSurveyBtn_Click" Text="Back" cssclass="delete-survey-button"/>
</asp:Content>
