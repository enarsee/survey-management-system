﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="AddQuestion.aspx.cs" Inherits="SofaDev.AddQuestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 113%;
        }
        .auto-style3 {
        }
        .auto-style4 {
            width: 249px;
        }
        .auto-style5 {
        }
        .auto-style6 {
            width: 241px;
        }
        .auto-style7 {
            width: 110px;
        }
        .auto-style8 {
            width: 62px;
        }
        .auto-style9 {
            width: 171px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
             <asp:PlaceHolder ID="phQuestions" runat="server"></asp:PlaceHolder>
    
    <br />
    
    <asp:Button ID="btnCreateSurvey" runat="server" Text="Create Survey" OnClick="btnCreateSurvey_Click" />

    <asp:Label ID="lblSurveyError" runat="server" ForeColor="Red"></asp:Label>

    <asp:Panel ID="Panel1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style7">
                    <asp:Label ID="lblqns" runat="server"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtQns" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style9">
                    <asp:Button ID="btnAddQns" runat="server" Text="Add Question" OnClick="btnAddQuestion_Click" />
                </td>
                <td>
                    <asp:Label ID="lblQnsError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style5" colspan="3">
                    <asp:DropDownList ID="ddlQuestionType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="auto-style5">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3" colspan="3">
                    <asp:PlaceHolder ID="phOptions" runat="server"></asp:PlaceHolder>
                    
                        <asp:Panel ID="Panel2" runat="server" Visible="False">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style6">
                                    <asp:TextBox ID="txtOption" runat="server"></asp:TextBox>
                                </td>
                                <td class="auto-style8">
                                    <asp:Button ID="btnAddOptions" runat="server" Text="+" OnClick="btnAddOptions_Click" />
                                </td>
                                <td>
                                    <asp:Label ID="lblOptionError" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
                </td>
                <td class="auto-style3">&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
