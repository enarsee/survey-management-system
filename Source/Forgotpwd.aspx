﻿<%@ Page Language="C#" AutoEventWireup="true"
     MasterPageFile="~/Public.Master"  
    CodeBehind="Forgotpwd.aspx.cs" 
    Inherits="SofaDev.Forgotpwd" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" runat="server">
	Password Reset
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceholder1" runat="server">

<h2>Password Reset</h2>
Enter your account information below and click continue...
    <div>
        <fieldset>
            <legend>Account Information</legend>
                
            <div class="editor-label">
                Enter email address: <asp:TextBox ID="emailBox" runat="server"></asp:TextBox>
            </div>
                                               
            <p><asp:Button ID="Button1" runat="server" onclick="Reset_Click" Text="Send Email" /></p>
        </fieldset>
    </div>
</asp:Content>
