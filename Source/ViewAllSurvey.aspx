<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="ViewAllSurvey.aspx.cs" Inherits="SofaDev.ViewAllSurvey" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    

    <asp:Button ID="btnCreate" runat="server" OnClick="btnCreate_Click" cssclass="delete-survey-button" Text="Create Survey" />
    <br />
    
    <asp:GridView ID="GridView1" runat="server" class="gridview">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="Survey ID" DataNavigateUrlFormatString="~/ViewSurvey.aspx?id={0}" Text="View" />
        </Columns>
    </asp:GridView>
        
</asp:Content>
