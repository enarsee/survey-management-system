﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Public.Master"  CodeBehind="Login.aspx.cs" Inherits="SofaDev.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table>
        <tr>
            <td>Email:</td>
            <td><asp:TextBox ID="TextBox1" runat="server"/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><asp:TextBox ID="TextBox2" runat="server" TextMode="Password"/></td>
        </tr>
        <tr style="text-align:center">
            <td colspan="2">
                <asp:Button ID="Button1" runat="server" onclick="Login_Click" Text="Login" />
                <asp:Button ID="Button2" runat="server" onclick="Forget_Click" Text="Forget Password" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <%-- FACEBOOK BUTTON --%><br />
                <br /><asp:HyperLink runat="server" Text="Log in with Facebook" id="fbLogin">
                    <img src="Images/facebook_login.png" /></asp:HyperLink>
                <br /><asp:LoginName ID="LoginName1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
