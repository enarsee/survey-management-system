﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" MasterPageFile="~/Public.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <% Session.Abandon(); Session.Clear(); %>
    
    <center>
    You have been successfully logged out !
    </center>
    <%Response.Redirect("Default.aspx"); %>
</asp:COntent>

