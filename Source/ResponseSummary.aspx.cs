﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using AjaxControlToolkit;
using System.IO;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.Data;
using SofaDev.Controller;
using SofaDev;

public partial class ResponseSummary : System.Web.UI.Page
{
    int chartCount = 1;
    int checkNum = 1;
    int checkNum1 = 1;
    string browserSurveyID = "";
    string tagging = "sid";
    ResponseController resCon = new ResponseController();

    AccountController ac1;
    protected override void OnPreInit(EventArgs e)
    {
        ac1 = (AccountController)Session["account"];
        if (ac1 == null) // Switches to public masterpage
            this.Page.MasterPageFile = "~/Public.master";
        else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
            this.Page.MasterPageFile = "~/Admin.master";
        else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
            this.Page.MasterPageFile = "~/Creator.master";
        else
            this.Page.MasterPageFile = "~/Public.master";
        base.OnPreInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["account"] == null)
        {
            Response.Redirect("Default.aspx");
        }   
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "dynamicResizeDivScript",
        //            "alert('" + Session["divResizeScript"].ToString() + "')", true);
        checkNum = 1;
        checkNum1 = 1;
        if (!IsPostBack)
        {
            if (Request.QueryString[tagging] != null)
            {
                
                DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
                DataList1.DataBind();
                Label1.Text = resCon.getSurveyTitle(int.Parse(Request.QueryString[tagging]));
                Label6.Text = resCon.getSurveyDescription(int.Parse(Request.QueryString[tagging]));
                // Clear controls
                GetUserControls1(Page.Controls);
            }
            else
                Label1.Text = "No survey selected!";
        }
        
    }

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //ResponseController resCon = new ResponseController();
        Label questionOptionLbl = (Label)e.Item.FindControl("QuestionOption");
        Label questionOptionResultLbl = (Label)e.Item.FindControl("QuestionOptionResult");
        ArrayList inQuestion = new ArrayList();
        ArrayList inQuestionCount = new ArrayList();
        String questionString = "";
        String questionCountString = "";
        // Gets question data
        questionString = questionOptionLbl.Text;
        // Gets question count
        questionCountString = questionOptionResultLbl.Text;

        // Spilt up question and questionCount
        string[] questionA = null;
        if (questionString != " ")
        {
            questionA = questionString.Split('_');
            foreach (String word in questionA)
                inQuestion.Add(word);
        }
        else
            inQuestion = null;
        string[] questionCountA = questionCountString.Split('_');
        
        foreach (String count in questionCountA)
            inQuestionCount.Add(count);

        String inChartID = "chart" + chartCount.ToString();
        ClientScript.RegisterClientScriptBlock(this.GetType(), "1BarChartScript" + chartCount.ToString(),
                                resCon.generateLeftBarChart(inQuestion, inQuestionCount, inChartID).ToString());
        
        // Increment chart count to know which chart to generate
        chartCount++;
    }

    // Make ModalPopupExtender display again upon autopostback
    public void GetUserControls(ControlCollection controls, int cCount)
    {
        foreach (Control ctl in controls)
        {
            if (ctl is ModalPopupExtender)
            {
                ModalPopupExtender mpe = (ModalPopupExtender)ctl;
                if (checkNum == cCount)
                {
                    mpe.Show();
                    checkNum++;
                }
                else
                {
                    mpe.Hide();
                    checkNum++;
                }
            }
            if (ctl.Controls.Count > 0)
                GetUserControls(ctl.Controls, cCount);
        }
    }
    public void GetPanel(ControlCollection controls)
    {
        foreach (Control ctl in controls)
        {
            if (ctl is Panel && ctl.ID.ToString().Equals("Panel1"))
            {
                Panel panel1 = (Panel)ctl;
                panel1.Visible = true;
            }
            if (ctl.Controls.Count > 0)
                GetUserControls1(ctl.Controls);
        }
    }

    // Clean ModalPopupExtender
    public void GetUserControls1(ControlCollection controls)
    {
        foreach (Control ctl in controls)
        {
            if (ctl is ModalPopupExtender)
            {
                ModalPopupExtender mpe = (ModalPopupExtender)ctl;
                mpe.Hide();
            }
            if (ctl.Controls.Count > 0)
                GetUserControls1(ctl.Controls);
        }
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string cCount = (string)Session["cCount"];
        DropDownList DropDownList1 = (DropDownList)sender;
        //ResponseController resCon = new ResponseController();
        String inChartID = "puchart" + cCount;
        ArrayList inQuestion = new ArrayList();
        ArrayList inQuestionCount = new ArrayList();
        String questionString = "";
        String questionCountString = "";
        // Gets question data
        
        //questionString = qOption.Value;
        questionString = "Yes,No";
        // Gets question count
        //questionCountString = qoResult.Value;
        questionCountString = "1,2";
        // Spilt up question and questionCount
        string[] questionA = null;
        if (questionString != " ")
        {
            questionA = questionString.Split('_');
            foreach (String word in questionA)
                inQuestion.Add(word);
        }
        else
            inQuestion = null;
        string[] questionCountA = questionCountString.Split('_');

        foreach (String count in questionCountA)
            inQuestionCount.Add(count);

        // Generate Chart based on dropdownlist selected value
        if (DropDownList1.SelectedValue == "Bar")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "puBarChartScript" + chartCount.ToString(),
                                resCon.generateLeftBarChart(inQuestion, inQuestionCount, inChartID).ToString());
        }
        else if (DropDownList1.SelectedValue == "Column")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "puColumnChartScript" + chartCount.ToString(),
                                resCon.generateBarChart(inQuestion, inQuestionCount, inChartID).ToString());
        }
        else if (DropDownList1.SelectedValue == "Line")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "puLineChartScript" + chartCount.ToString(),
                                resCon.generateLineChart(inQuestion, inQuestionCount, inChartID).ToString());
        }
        else if (DropDownList1.SelectedValue == "Pie")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "puPieChartScript" + chartCount.ToString(),
                                resCon.generatePieChart(inQuestion, inQuestionCount, inChartID).ToString());
        }

        // Re-generate datas in the back of popup
        if (Request.QueryString[tagging] != "")
        {
            DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
            DataList1.DataBind();
        }
        // Find Modal Popup Extender and show it again on auto post back
        GetUserControls(Page.Controls, int.Parse(cCount));
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        //ResponseController resCon = new ResponseController();
        // Reset cCount
        Session["cCount"] = "";
        LinkButton lb = (LinkButton)sender;
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "removeScript",
        //        "alert( \"" + lb.CommandArgument.ToString() + "\");", true);
        Session["cCount"] = lb.CommandArgument.ToString();
        // Re-generate datas in the back of popup
        if (Request.QueryString[tagging] != "")
        {
            DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
            DataList1.DataBind();
        }

        // Javascript to make Panel1 visibility = true
        ClientScript.RegisterClientScriptBlock(this.GetType(), "visibilityScript",
                    "<script>document.getElementById(\"Panel1\").style.display = \"initial\";</script>");
        
        // Display Popup chart
        GetUserControls(Page.Controls, int.Parse((string)Session["cCount"]));

        // Display panel
        GetPanel(Page.Controls);
    }

    // Do not remove, for exporting data
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    
    // Share responses link button
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        // Open new window for user to share response to facebook
        if (Request.QueryString[tagging] != "")
        {
            ClientScript.RegisterStartupScript(this.Page.GetType(), "ShareResponse",
            "window.open('SurveySummary.aspx?"+tagging+"='"+Request.QueryString[tagging].ToString()+", 'Test', 'menubar=0,resizable=0,width=500,height=400,scrollbars=1', '').moveTo(100,100);", true);
            //ClientScript.RegisterStartupScript(this.Page.GetType(), "ShareResponse",
            //"<script>javascript:pop(<script>javascript:pop(SurveySummary.aspx?" + tagging + "=" + Request.QueryString[tagging].ToString() + ");</script>);</script>", true);
            //Response.Redirect("ResponseSummary.aspx?" + tagging + "=" + Request.QueryString[tagging].ToString());
        }
    }
    // Export responses link button
    protected void LinkButton7_Click(object sender, EventArgs e)
    {
        // Open new window for user to share response to facebook
        if (Request.QueryString[tagging] != "")
        {
            ClientScript.RegisterStartupScript(this.Page.GetType(), "ShareResponse",
            "window.open('ExportResponse.aspx?" + tagging + "='" + Request.QueryString[tagging].ToString() + ", 'Test', 'menubar=0,resizable=0,width=500,height=400,scrollbars=1', '').moveTo(100,100);", true);
            //ClientScript.RegisterStartupScript(this.Page.GetType(), "ShareResponse",
            //"<script>javascript:pop(<script>javascript:pop(SurveySummary.aspx?" + tagging + "=" + Request.QueryString[tagging].ToString() + ");</script>);</script>", true);
            //Response.Redirect("ResponseSummary.aspx?" + tagging + "=" + Request.QueryString[tagging].ToString());
        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        if (Panel5.Visible == true)
            Panel5.Visible = false;
        else
            Panel5.Visible = true;
        // Re-generate datas in the back of popup
        if (Request.QueryString[tagging] != "")
        {
            DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
            DataList1.DataBind();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //ResponseController resCon = new ResponseController();
        //GetUserControls2(Page.Controls);
        string format = RadioButtonList1.SelectedValue.ToString();
        if (format == "PDF")
        {
            ExportToPDF();
        }
        else if (format == "Word")
        {
            ExportToWord();
        }
        else if (format == "CSV")
        {
            ExportToCSV();
        }
        else if (format == "Excel")
        {
            ExportToExcel();
        }
    }

    private void ExportToPDF()
    {
        // Export to PDF
        //ResponseController resCon = new ResponseController();
        DataTable dt = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
        Document document = new Document();
        PdfWriter.GetInstance(document, new FileStream(Server.MapPath("Temp/Survey_" + Request.QueryString[tagging] + "_Response.pdf"), FileMode.Create)); //在当前路径下创一个文件 　
        document.Open();

        Paragraph paragraph = new Paragraph(resCon.getSurveyTitle(int.Parse(Request.QueryString[tagging])) + "\n\n"
            + resCon.getSurveyDescription(int.Parse(Request.QueryString[tagging])) + "\n\n");
        paragraph.Alignment = Element.ALIGN_CENTER;

        PdfPTable table = new PdfPTable(dt.Columns.Count);
        // Add Headers
        for (int k = 0; k < dt.Columns.Count; k++)
        {
            table.AddCell(dt.Columns[k].ColumnName);
        }
        // Add Data
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                string data = dt.Rows[i][j].ToString();
                if (j > 1) //  ommit 1st and 2nd column
                {
                    if (data != " ") // if data is "" then its a cell without
                    {
                        string[] sd = data.Split('_'); // Spilt the string up
                        data = ""; // Clear data string
                        foreach (string d in sd) // Iterate through string array
                        {
                            data = data + "[" + d + "] ";
                        }
                        table.AddCell(data);
                    }
                    else
                        table.AddCell(""); // set questions with no options to ""
                }
                else
                {
                    table.AddCell(dt.Rows[i][j].ToString());
                }
            }
        }
        document.Add(paragraph);
        document.Add(table);　　//add table
        document.Close();

        // Re-generate datas in the back of popup
        if (Request.QueryString[tagging] != "")
        {
            DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
            DataList1.DataBind();
        }

        ClientScript.RegisterStartupScript(this.Page.GetType(), "ExportPDF",
            "window.open('Temp/Survey_" + Request.QueryString[tagging] + "_Response.pdf','_blank');", true);
    }
    private void ExportToWord()
    {
        // Export to Word
        //ResponseController resCon = new ResponseController();
        DataTable dt = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));

        // Modify datatable values for better display
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                string data = dt.Rows[i][k].ToString();
                if (k > 1) //  ommit 1st and 2nd column
                {
                    if (data != " ") // if data is "" then its a cell without
                    {
                        string[] sd = data.Split('_'); // Spilt the string up
                        data = ""; // Clear data string
                        foreach (string d in sd) // Iterate through string array
                        {
                            data = data + "[" + d + "] ";
                        }
                        dt.Rows[i][k] = data;
                    }
                    else
                        dt.Rows[i][k] = ""; // set questions with no options to ""
                }
            }
        }

        // Export to Word
        //Create a dummy GridView
        GridView GridViewD = new GridView();
        GridViewD.AllowPaging = false;
        GridViewD.DataSource = dt;
        GridViewD.DataBind();
        GridViewD.Caption = resCon.getSurveyTitle(int.Parse(Request.QueryString[tagging])) +
            "<br /><br />" + resCon.getSurveyDescription(int.Parse(Request.QueryString[tagging]));
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=Survey_" + Request.QueryString[tagging] + "_Response.doc");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-word";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridViewD.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    private void ExportToCSV()
    {
        // Export to CSV
        //ResponseController resCon = new ResponseController();
        DataTable dt = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition",
         "attachment;filename=Survey_" + Request.QueryString[tagging] + "_Response.csv");
        Response.Charset = "";
        Response.ContentType = "application/text";

        StringBuilder sb = new StringBuilder();
        for (int k = 0; k < dt.Columns.Count; k++)
        {
            //add separator
            sb.Append(dt.Columns[k].ColumnName + ',');
        }
        //append new line
        sb.Append("\r\n");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                string data = dt.Rows[i][k].ToString();
                if (k > 1) //  ommit 1st and 2nd column
                {
                    if (data != " ") // if data is "" then its a cell without
                    {
                        string[] sd = data.Split('_'); // Spilt the string up
                        data = ""; // Clear data string
                        foreach (string d in sd) // Iterate through string array
                        {
                            data = data + "[" + d + "] ";
                        }
                    }
                    else
                        data = ""; // set questions with no options to ""
                }
                //add separator
                sb.Append(data + ',');
            }
            //append new line
            sb.Append("\r\n");
        }
        Response.Output.Write(sb.ToString());
        Response.Flush();
        Response.End();
        /*//Create a dummy GridView
        GridView GridViewD = new GridView();
        GridViewD.AllowPaging = false;
        GridViewD.DataSource = dt;
        GridViewD.DataBind();
        GridViewD.Caption = resCon.getSurveyTitle(int.Parse(Request.QueryString[tagging])) + "<br /><br />" +
            resCon.getSurveyDescription(int.Parse(Request.QueryString[tagging]));
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        for (int i = 0; i < GridViewD.Rows.Count; i++)
        {

            GridViewD.Rows[i].Attributes.Add("class", "textmode");
        }
        GridViewD.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();*/
    }
    private void ExportToExcel()
    {
        // Export to Excel
        //ResponseController resCon = new ResponseController();
        DataTable dt = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));

        // Modify datatable values for better display
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                string data = dt.Rows[i][k].ToString();
                if (k > 1) //  ommit 1st and 2nd column
                {
                    if (data != " ") // if data is "" then its a cell without
                    {
                        string[] sd = data.Split('_'); // Spilt the string up
                        data = ""; // Clear data string
                        foreach (string d in sd) // Iterate through string array
                        {
                            data = data + "[" + d + "] ";
                        }
                        dt.Rows[i][k] = data;
                    }
                    else
                        dt.Rows[i][k] = ""; // set questions with no options to ""
                }
            }
        }

        //Create a dummy GridView
        GridView GridViewD = new GridView();
        GridViewD.AllowPaging = false;
        GridViewD.DataSource = dt;
        GridViewD.DataBind();
        GridViewD.Caption = resCon.getSurveyTitle(int.Parse(Request.QueryString[tagging])) + "<br /><br />" +
            resCon.getSurveyDescription(int.Parse(Request.QueryString[tagging]));

        Response.Clear();
        Response.Buffer = true;
        Response.AppendHeader("content-disposition", "attachment;filename=Survey_" + Request.QueryString[tagging] + "_Response.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        for (int i = 0; i < GridViewD.Rows.Count; i++)
        {

            GridViewD.Rows[i].Attributes.Add("class", "textmode");
        }
        GridViewD.RenderControl(hw);

        string style = @"<style> .textmode { mso-number-format:\@; } </style>";
        Response.Write(style);
        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        Panel5.Visible = false;
        // Re-generate datas in the back of popup
        if (Request.QueryString[tagging] != "")
        {
            DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
            DataList1.DataBind();
        }
    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewSurvey.aspx?id= " + Request.QueryString[tagging]);
    }
}