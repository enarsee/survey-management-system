﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Security;
namespace SofaDev
{
    public partial class Forgotpwd : System.Web.UI.Page
    {
        static AccountController ac1 = new AccountController();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            //ac1 = new AccountController();
            // Call controller and pass in email

            ac1.ResetPassword(emailBox.Text);
            Response.Redirect("PasswordResetFinal.aspx");
           
        }
    }
}