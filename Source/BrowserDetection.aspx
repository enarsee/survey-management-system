﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="BrowserDetection.aspx.cs" Inherits="SofaDev.BrowserDetection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>You are currently using</td>
            <td><asp:Label ID="lblBrowser" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Your IP Address is : </td>
            <td><asp:Label ID="lblipAddr" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>You are using Mobile Browser: </td>
            <td>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" Enabled="False" Height="16px" EnableTheming="False" EnableViewState="False" ViewStateMode="Disabled">
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</asp:Content>
