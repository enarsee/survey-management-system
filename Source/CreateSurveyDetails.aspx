﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="CreateSurveyDetails.aspx.cs" Inherits="SofaDev.SetSurveyDates" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <table class="auto-style1">
          <tr>
              <td class="auto-style2" colspan="2"><asp:Label ID="EnterTitle" runat="server" Text="Enter Survey Title:"></asp:Label> 
               
                  <asp:TextBox ID="SurveyTitleTB" runat="server" Width="566px"></asp:TextBox>
              </td>
          </tr>
          <tr>
              <td class="auto-style2" colspan="2">Enter Survey Description</td>
          </tr>
          <tr>
              <td class="auto-style2" colspan="2">
                  <asp:TextBox ID="SurveyDescriptionTB" runat="server" Height="138px" TextMode="MultiLine" Width="900px"></asp:TextBox>
              </td>
          </tr>
          <tr>
              <td>
      <asp:Label ID="Label1" runat="server" Text="Please select a Start Date for your survey"></asp:Label>
              </td>
              <td>
     <asp:Label ID="Label2" runat="server" Text="Please select a End Date for your survey"></asp:Label>
                    </td>
          </tr>
          <tr>  <asp:UpdatePanel ID="UpdatePanel1" runat="server"  UpdateMode="Conditional" ChildrenAsTriggers="false">
              <ContentTemplate>
              <td>
                
                    <asp:Calendar ID="StartDateCal" runat="server" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px" OnSelectionChanged="StartDateCal_SelectionChanged1">
                        <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                        <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                        <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                        <TodayDayStyle BackColor="#CCCCCC" />
                    </asp:Calendar>
              </td>
              <td>
                    <asp:Calendar ID="EndDateCal" runat="server" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="350px" OnSelectionChanged="EndDateCal_SelectionChanged" >
                        <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                        <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                        <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                        <TodayDayStyle BackColor="#CCCCCC" />
                    </asp:Calendar>
                  
              </td></ContentTemplate></asp:UpdatePanel>
          </tr>
          <tr>
              <td colspan="2">
                  <asp:Button ID="CreateSurvey" runat="server" Text="Create Survey" OnClick="CreateSurvey_Click" />
                  <asp:Button ID="dummyButton" runat="server" Text="" Visible="false" />

                  <asp:ModalPopupExtender ID="EnterTitle_ModalPopupExtender" runat="server" DynamicServicePath="" PopupControlID ="ErrorPanel" Enabled="True" TargetControlID="dummyButton">
                  </asp:ModalPopupExtender>

              </td>
          </tr>
      </table>
    <asp:UpdatePanel ID="ErrorPanel" runat="server"  UpdateMode="Conditional" >
               <ContentTemplate>
                     <div class="questionPanels">
                         <asp:label id="errorMsg" runat="server" ></asp:label>
                    
                     </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
