﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" Inherits="SurveySummary" Codebehind="SurveySummary.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <!-- Start import for charts -->
    <link rel="stylesheet" type="text/css" href="Scripts/jqplot/jquery.jqplot.css" />
    <link rel="stylesheet" type="text/css" href="Scripts/jqplot/jquery.jqplot.min.css" />
    <script type="text/javascript" src="Scripts/jqplot/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jqplot/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.pointLabels.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.barRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
    <!-- End import for charts -->
    <style>
        .modalBackground {
            background-color:black;
            filter:alpha(opacity=50);
            opacity:0.7;
        }
        .popupContentBackground {
            min-width:600px;
            min-height:150px;
            background:lightgrey;
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <table style="width:100%;">
        <tr>
            <td style="text-align:center"><h1><asp:Label ID="Label1" runat="server" Text=""></asp:Label></h1>
                <h2><asp:Label ID="Label2" runat="server" Text=""></asp:Label></h2></td>
        </tr>
        <tr>
            <td style="text-align:right">
                <!-- Facebook share button Start -->
                <b:if cond='data:blog.pageType != &quot;static_page&quot;'>
                <div style='float:none;padding:5px 5px 5px 0;'>
                <a expr:share_url='data:post.url' href='http://www.facebook.com/sharer.php' name='fb_share' type='button_count'>Share</a><script src='http://static.ak.fbcdn.net/connect.php/js/FB.Share' type='text/javascript'></script>
                </div>
                </b:if>
                <!-- Facebook share button End -->
            </td>
            <td style="text-align:left">
                <!-- Twitter share button Start -->
                <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
                <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
                <!-- Twitter share button End -->
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound" Width="90%" >
                    <ItemTemplate>
                       <table style="width:100%;">
                            <tr>
                                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("No")%>'></asp:Label>.</td>
                                <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("Question")%>' Width="100%"></asp:Label></td>
                           </tr>
                            <tr>    
                                <td><asp:Label ID="QuestionOption" runat="server" Text='<%# Eval("Option")%>' Visible="false" ></asp:Label></td>
                                <td><asp:Label ID="QuestionOptionResult" runat="server" Text='<%# Eval("Result")%>' Visible="false" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td id="col" runat="server" colspan="2" style="text-align:right">
                                    <div id='chart<%# Eval("No")%>' style="margin-top:20px; margin-left:20px; width:100%; height: 300px"/>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
    <div style="text-align:center"><input type="button" onclick="javaScript: window.close(); return false;" value="Close" /></div>
</asp:Content>

