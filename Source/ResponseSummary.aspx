﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeFile="ResponseSummary.aspx.cs" Inherits="ResponseSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <!-- Start import for charts -->
    <link rel="stylesheet" type="text/css" href="Scripts/jqplot/jquery.jqplot.css" />
    <link rel="stylesheet" type="text/css" href="Scripts/jqplot/jquery.jqplot.min.css" />
    <script type="text/javascript" src="Scripts/jqplot/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jqplot/jquery.jqplot.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.pointLabels.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.barRenderer.min.js"></script>
    <script language="javascript" type="text/javascript" src="Scripts/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
    <!-- End import for charts -->
    <style>
        .modalBackground {
            background-color:black;
            filter:alpha(opacity=50);
            opacity:0.7;
        }
        .popupContentBackground {
            min-width:600px;
            min-height:150px;
            background:lightgrey;
            text-align:center;
        }
        .popupContentBackground1 {
            min-width:100px;
            min-height:150px;
            background:lightgrey;
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <table style="width:100%;">
        <tr>
            <td style="text-align:center">
                <h1><asp:Label ID="Label1" runat="server" Text=""></asp:Label></h1>
                <h2><asp:Label ID="Label6" runat="server" ></asp:Label></h2>
                <p>
                    <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />
                </p>

                <% if(Request.QueryString["sid"] != null){ %>
                <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Export Responses</asp:LinkButton>
                <a href="javascript://" onclick="window.open('SurveySummary.aspx?sid=<%= Request.QueryString["sid"].ToString() %>', 'Test', 'menubar=0,resizable=0,width=1000,height=700,scrollbars=1', '').moveTo(100,100);" >Share Response</a>
                <%} %>
                <asp:Panel ID="Panel5" runat="server" Visible="false" BorderStyle="Solid">
                    <h2>Export Response</h2>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" Width="100%">
                        <asp:ListItem Selected="True" Value="PDF">PDF Format</asp:ListItem>
                        <asp:ListItem Value="Word">Word Format</asp:ListItem>
                        <asp:ListItem Value="CSV">CSV Format</asp:ListItem>
                        <asp:ListItem Value="Excel">Excel Format</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Button ID="Button1" Text="Export" runat="server" OnClick="Button1_Click" />
                    <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click">Close</asp:LinkButton>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataList ID="DataList1" runat="server" OnItemDataBound="DataList1_ItemDataBound" Width="90%" OnSelectedIndexChanged="DataList1_SelectedIndexChanged" >
                    <ItemTemplate>
                       <table style="width:100%;">
                            <tr>
                                <td><asp:Label ID="Label2" runat="server" Text='<%# Eval("No")%>'></asp:Label>.</td>
                                <td><asp:Label ID="Label3" runat="server" Text='<%# Eval("Question")%>' Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:right">
                                    <!--<asp:LinkButton ID="LinkButton1" CommandArgument='<%# Eval("No") %>' CommandName="Select" runat="server" OnClick="LinkButton1_Click">Create Chart</asp:LinkButton>-->
                                </td>
                            </tr>
                            <tr>    
                                <td><asp:Label ID="QuestionOption" runat="server" Text='<%# Eval("Option")%>' Visible="false" ></asp:Label></td>
                                <td><asp:Label ID="QuestionOptionResult" runat="server" Text='<%# Eval("Result")%>' Visible="false" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td id="col" runat="server" colspan="2" style="text-align:right">
                                    <div id='chart<%# Eval("No")%>' style="margin-top:20px; margin-left:20px; width:100%; height:300px"/>
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="Panel1" runat="server" Visible="false" >
                            <asp:updatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                <div class="popupContentBackground">
                                    <table style="width:100%">
                                        <tr>
                                            <td><h1>Create Chart</h1></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                
                                                 
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Question")%>' Width="100%"></asp:Label>
                                                 
                                                <input type="hidden" runat="server" id="qoResult" value='<%# Eval("Result")%>'  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><div id='puchart<%# Eval("No")%>' style="margin-top:20px; margin-left:20px; width:500px; height:300px;"/></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Chart Type: "></asp:Label>
                                                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                    <asp:ListItem Value="">-- Select Chart Type --</asp:ListItem>
                                                    <asp:ListItem Value="Bar">Bar Chart</asp:ListItem>
                                                    <asp:ListItem Value="Column">Column Chart</asp:ListItem>
                                                    <asp:ListItem Value="Line">Line Chart</asp:ListItem>
                                                    <asp:ListItem Value="Pie">Pie Chart</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="OkButton" runat="server" Text="Download" Visible="false" />
                                                <asp:Button ID="CloseButton" runat="server" Text="Close" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                </ContentTemplate>
                                </asp:updatePanel>   
                            </asp:Panel>
                        <asp:Button ID="btnShowPopup" runat="server" style="display:none;" />
                        
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>

