﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using AjaxControlToolkit;
using System.IO;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.Data;
using SofaDev.Controller;

public partial class SurveySummary : System.Web.UI.Page
{
    int chartCount = 1;
    int checkNum = 1;
    int checkNum1 = 1;
    string browserSurveyID = "";
    string tagging = "sid";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["account"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        ResponseController resCon = new ResponseController();
        checkNum = 1;
        checkNum1 = 1;
        if (!IsPostBack)
        {
            if (Request.QueryString[tagging] != null)
            {
                DataList1.DataSource = resCon.retrieveAnalyseResult(int.Parse(Request.QueryString[tagging]));
                DataList1.DataBind();
                Label1.Text = resCon.getSurveyTitle(int.Parse(Request.QueryString[tagging]));
                Label2.Text = resCon.getSurveyDescription(int.Parse(Request.QueryString[tagging]));
            }
            else
                Label1.Text = "No survey selected!";
        }
    }

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ResponseController resCon = new ResponseController();
        Label questionOptionLbl = (Label)e.Item.FindControl("QuestionOption");
        Label questionOptionResultLbl = (Label)e.Item.FindControl("QuestionOptionResult");
        //findlabels(e.Item.Controls);
        ArrayList inQuestion = new ArrayList();
        ArrayList inQuestionCount = new ArrayList();
        String questionString = "";
        String questionCountString = "";
        // Gets question data
        questionString = questionOptionLbl.Text;
        // Gets question count
        questionCountString = questionOptionResultLbl.Text;
        
        // Spilt up question and questionCount
        string[] questionA = null;
        if (questionString != " ")
        {
            questionA = questionString.Split('_');
            foreach (String word in questionA)
                inQuestion.Add(word);
        }
        else
            inQuestion = null;
        string[] questionCountA = questionCountString.Split('_');
        
        foreach (String count in questionCountA)
            inQuestionCount.Add(count);

        String inChartID = "chart" + chartCount.ToString();
        ClientScript.RegisterClientScriptBlock(this.GetType(), "1BarChartScript" + chartCount.ToString(),
                                resCon.generateLeftBarChart(inQuestion, inQuestionCount, inChartID).ToString());
        
        // Increment chart count to know which chart to generate
        chartCount++;
    }
}