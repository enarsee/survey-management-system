﻿using SofaDev.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Class;
using System.Text.RegularExpressions;

namespace SofaDev
{
    public partial class Default : System.Web.UI.Page
    {
        AccountController ac1;

        protected void Page_Load(object sender, EventArgs e)
        {
            Button3.CausesValidation = false;
            Button1.CausesValidation = true;
            Button4.CausesValidation = false;
            if(Session["account"] != null)
                Response.Redirect("ViewAllSurvey.aspx");
            /*if (!IsPostBack)
            {
                TextBox3.Text = "Email";
                TextBox4.Text = "Password";
                TextBox5.Text = "Name";
                TextBox6.Text = "Age";
                TextBox7.Text = "Address";
                TextBox8.Text = "Contact";
                TextBox9.Text = "Confirm Password";
            }*/
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            
            //Page.Validate
            RequiredFieldValidator1.Enabled = false;
            //RequiredFieldValidator1.IsValid = true;
            
            ac1 = new AccountController();
            // Call controller and pass in email and password
            if (ac1.Login(TextBox1.Text, TextBox2.Text) == 1)
            {
                Session["account"] = ac1;
                //Session["email"] = ac1.getEmail();
                Response.Redirect("ViewAllSurvey.aspx");
            }
            else
                Response.Redirect("Default.aspx?error=0");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ac1 = new AccountController();
            bool check = true;
            if (TextBox3.Text == "" || TextBox4.Text == "" || TextBox5.Text == "" || TextBox6.Text == "" || TextBox7.Text == "" || TextBox8.Text == ""
                || TextBox9.Text.CompareTo(TextBox4.Text) != 0 )
            {
                Label2.Text = "Please enter all fields correctly.";
                check = false;
            }
            /*// Validation
            if (TextBox3.Text.Equals("Email") || (Regex.IsMatch(TextBox3.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$")==false))
            {
                Label3.Text = "Please enter email"; check = false;
            }
            else if (TextBox4.Text.Equals("Password"))
            {
                Label4.Text = "Please enter password"; check = false;
            }
            else if (TextBox9.Text.CompareTo(TextBox4.Text) != 0)
            {
                Label5.Text = "Passwords do not match"; check = false;
            }
            else if (TextBox5.Text.Equals("Name"))
            {
                Label6.Text = "Please enter name"; check = false;
            }
            else if (TextBox6.Text.Equals("Age") || (Regex.IsMatch(TextBox6.Text, @"-?\d+(\.\d+)?") == false))
            {
                Label7.Text = "Please enter age"; check = false;
            }
            else if (TextBox7.Text.Equals("Address") || (TextBox7.Text == null))
            {
                Label8.Text = "Please enter address"; check = false;
            }
            else if ((TextBox8.Text.Equals("Contact")) || (Regex.IsMatch(TextBox4.Text, @"-?\d+(\.\d+)?") == false))
            {
                Label9.Text = "Please enter contact"; check = false;
            }*/

            // If check equals true then proceed
            //if (check)
            //{

            // Check whether email exists in database
            if (Account.GetAccountID(TextBox3.Text) == -1)
            {
                // Call controller and pass in email and password
                if (ac1.CreateAccount(TextBox3.Text, TextBox4.Text, TextBox5.Text, TextBox6.Text, male.Checked ? "1" : "0", TextBox7.Text, TextBox8.Text, "1") == 1)
                {
                    Session["account"] = ac1;
                    //Session["email"] = TextBox3.Text;
                    Response.Redirect("ViewAllSurvey.aspx");
                }
                else
                    Response.Redirect("Default.aspx");
            }
            else
                Label2.Text = "Email already exists!";
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Response.Redirect("Forgotpwd.aspx");
        }

        // FB checker function
        protected override void OnLoad(EventArgs e)
        {
            ac1 = new AccountController();
            base.OnLoad(e);
            FBConnect fbConnect = new FBConnect();
            fbConnect.CheckFB(this, e, fbLogin, ac1, "ViewAllSurvey.aspx");
        }

    }
}