﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using SofaDev.Class;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Security;

namespace SofaDev
{
    public partial class User : System.Web.UI.Page
    {
        AccountController ac1;
        static AccountController ac2 = new AccountController();
        protected override void OnPreInit(EventArgs e){
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            ac1 = (AccountController)Session["account"];
            if (ac1 != null)
            {
                TextBox1.Text = ac1.getAge().ToString();
                TextBox2.Text = ac1.getContact();
                RadioButtonList1.Items.Insert(0, "Male");
                RadioButtonList1.Items.Insert(1, "Female");
                if (ac1.getGender() == "Male")
                {
                    RadioButtonList1.SelectedIndex = 0;
                    RadioButtonList1.Items[0].Selected = true;
                }
                else if (ac1.getGender() == "Female")
                {
                    RadioButtonList1.SelectedIndex = 1;
                    RadioButtonList1.Items[1].Selected = true;
                }

                TextBox4.Text = ac1.getAddress();
                TextBox5.Text = ac1.getName();
                TextBox6.Text = ac1.getEmail();
                //TextBox7.Text = ac1.getPassword();
            }
            else
                Response.Redirect("Default.aspx?error=1");
            
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["account"] != null)
            //{
                //ac1 = (AccountController)Session["account"];
            
            //}
        }        
        protected void Button1_Click(object sender, EventArgs e)
        {
            String returnMessage = "";
            Label1.Text = "";
            if (TextBox5.Text != "")
                ac1.UpdateAccount("name", TextBox5.Text);
            else
                returnMessage += "Please enter name \n\n";
            if(TextBox4.Text!="")
                ac1.UpdateAccount("address", TextBox4.Text);
            else
                returnMessage += "Please enter address \n\n";

            if (RadioButtonList1.SelectedItem.Text == "Male")
                ac1.UpdateAccount("gender", "Male");
            else if (RadioButtonList1.SelectedItem.Text == "Female")
                ac1.UpdateAccount("gender", "Female");

            if(TextBox2.Text!="")
                ac1.UpdateAccount("contact", TextBox2.Text);
            else
                returnMessage += "Please enter contact \n\n";
            if(TextBox1.Text!="")
                ac1.UpdateAccount("age", TextBox1.Text);
            else
                returnMessage += "Please enter age \n\n";
            if (TextBox6.Text != "")
            {
                // Check whether email exists in database
                //if (Account.GetAccountID(TextBox6.Text) == -1 && ac1.getEmail() == TextBox6.Text)
                //{
                    ac1.UpdateAccount("email", TextBox6.Text);
                //}
                //else
                //    returnMessage += "Please enter a valid email \n\n";
            }
            else
                returnMessage += "Please enter email \n\n";
            if(TextBox7.Text != ac1.getPassword() && TextBox7.Text != "")
                ac1.UpdateAccount("password", TextBox7.Text);
            // Updates session account
            //ac2.Login(TextBox6.Text,ac1.getPassword());
            //Session["account"] = null;
            //Session["account"] = ac2;

            //Response.Redirect("User.aspx");
           /* if (TextBox7.Text != "" || TextBox7.Text != null)
                ac1.UpdateAccount("password", TextBox7.Text);
            else
                returnMessage += "Please enter password \n\n";*/
            /*if (!returnMessage.Equals(""))
            {
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = returnMessage;
            }*/
        }
    }
}