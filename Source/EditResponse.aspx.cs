﻿using SofaDev.Controller;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SofaDev
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (Page.IsPostBack && (Session["control"] != null))
            {
                PlaceHolder place = (PlaceHolder)Session["control"];
                GeneratePlaceHolder(place);
            }
            else
            {
                int surveyID = int.Parse(Request.QueryString["sid"]);
                SurveyQuestionController surveyQuestion = new SurveyQuestionController();
                PlaceHolder place = new PlaceHolder();
                place = surveyQuestion.getAllQuestionID(surveyID, 2);
                Session["control"] = place;
                GeneratePlaceHolder(place);

               
                int responseID = int.Parse(Request.QueryString["id"]);
                ResponseController resCon = new ResponseController();
                List<int> allTypes = new List<int>();
                List<string> allAnswers = new List<string>();
                List<int> allQuestionID = new List<int>();
                List<string> allQuestionOptions = new List<string>();

                allTypes = (List<int>)resCon.getQuestionTypes(surveyID);
                allAnswers = (List<string>)resCon.getQuestionAnswers(responseID);
                allQuestionID = (List<int>)resCon.getAllQuestionIDBySurvey(surveyID);

                for (int i = 1; i < allTypes.Count + 1; i++)
                {
                    int type = allTypes[i - 1];
                    int questionID = allQuestionID[i - 1];
                    String compare = allAnswers[i - 1];
                    int finalOption = 0;

                    if (type == 1)
                    {
                        TextBox text = (TextBox)PlaceHolder1.FindControl("txtBox" + questionID) as TextBox;
                        text.Text = allAnswers[i - 1];
                        text.Enabled = true;
                    }
                    else if (type == 2)
                    {
                        TextBox textArea = (TextBox)PlaceHolder1.FindControl("textArea" + questionID) as TextBox;
                        textArea.Text = allAnswers[i - 1];
                        textArea.Enabled = true;
                    }
                    else if (type == 3)
                    {
                        RadioButtonList radio = (RadioButtonList)PlaceHolder1.FindControl("radBtnLst" + questionID) as RadioButtonList;
                        allQuestionOptions = resCon.getQuestionOptions(questionID);
                        finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compare);
                        radio.SelectedIndex = finalOption;
                        radio.Enabled = true;
                    }
                    else if (type == 4)
                    {
                        DropDownList ddl = (DropDownList)PlaceHolder1.FindControl("ddl" + questionID) as DropDownList;
                        allQuestionOptions = resCon.getQuestionOptions(questionID);
                        finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compare);
                        ddl.SelectedIndex = finalOption;
                        ddl.Enabled = true;
                    }
                    else if (type == 5)
                    {
                        CheckBoxList check = (CheckBoxList)PlaceHolder1.FindControl("boxList" + questionID) as CheckBoxList;
                        allQuestionOptions = resCon.getQuestionOptions(questionID);

                        if (compare.Contains('|') == true)
                        {
                            string[] compareList = compare.Split('|');
                            for (int k = 0; k < compareList.Length; k++)
                            {
                                finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compareList[k]);
                                check.Items[finalOption].Selected = true;
                                check.Enabled = true;
                            }
                        }
                        else
                        {
                            finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compare);
                            check.SelectedIndex = finalOption;
                            check.Enabled = true;
                            //check.Items.SelectedIndex = finalOption;
                        }
                        
                    }

                }
            } 
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void GeneratePlaceHolder(PlaceHolder holder)
        {
            int count = holder.Controls.Count;
            for (int i = 1; i <= count; i++)
            {
                Control c = holder.Controls[0];
                PlaceHolder1.Controls.Add(c);
                holder = (PlaceHolder)Session["control"];
            }
            if (count == 0)
            {
                Label error = new Label();
                HyperLink admin = new HyperLink();
                holder.Controls.Add(new LiteralControl("<br />"));
                error.Text = "\tThere does not exist any question for this Survey. Are you sure you followed a right link?<br/>";
                error.Text += "If you believe you have followed a right link, please contact our admin @ ";
                admin.Text = "Admin";
                admin.NavigateUrl = "http://www.google.com.sg";
                PlaceHolder1.Controls.Add(error);
                PlaceHolder1.Controls.Add(admin);
            }
            Session["control"] = PlaceHolder1;
            
        }

        protected void editButton_Click(object sender, EventArgs e)
        {
            int surveyID = int.Parse(Request.QueryString["sid"]);
            int responseID = int.Parse(Request.QueryString["id"]);
            String answer = "";
            ResponseController resCon = new ResponseController();
            ResultController restCon = new ResultController();
            List<int> allTypes = new List<int>();
            List<int> allQuestionID = new List<int>();
            List<string> allQuestionOptions = new List<string>();

            allTypes = (List<int>)resCon.getQuestionTypes(surveyID);
            allQuestionID = (List<int>)resCon.getAllQuestionIDBySurvey(surveyID);

            for (int i = 1; i < allTypes.Count + 1; i++)
            {
                int type = allTypes[i - 1];
                int questionID = allQuestionID[i - 1];
                int finalOption = 0;

                if (type == 1)
                {
                    TextBox text = (TextBox)PlaceHolder1.FindControl("txtBox" + questionID);
                    answer = text.Text;
                    restCon.updateResult(questionID, surveyID, responseID, answer);
                }
                else if (type == 2)
                {
                    TextBox textArea = (TextBox)PlaceHolder1.FindControl("textArea" + questionID) as TextBox;
                    answer = textArea.Text;
                    restCon.updateResult(questionID, surveyID, responseID, answer);
                }
                else if (type == 3)
                {
                    RadioButtonList radio = (RadioButtonList)PlaceHolder1.FindControl("radBtnLst" + questionID) as RadioButtonList;
                    allQuestionOptions = resCon.getQuestionOptions(questionID);
                    finalOption = radio.SelectedIndex;
                    answer = allQuestionOptions[finalOption];
                    restCon.updateResult(questionID, surveyID, responseID, answer);
                }
                else if (type == 4)
                {
                    DropDownList ddl = (DropDownList)PlaceHolder1.FindControl("ddl" + questionID) as DropDownList;
                    allQuestionOptions = resCon.getQuestionOptions(questionID);
                    finalOption = ddl.SelectedIndex;
                    answer = allQuestionOptions[finalOption];
                    restCon.updateResult(questionID, surveyID, responseID, answer);
                }
                else if (type == 5)
                {
                    String answer2 = "";
                    CheckBoxList check = (CheckBoxList)PlaceHolder1.FindControl("boxList" + questionID) as CheckBoxList;
                    Label temp = (Label)PlaceHolder1.FindControl("error" + questionID) as Label;
                    allQuestionOptions = resCon.getQuestionOptions(questionID);

                    if (check.SelectedIndex == -1)
                    {
                        temp.Visible = true;
                        return;
                    }
                    else
                    {
                        temp.Visible = false;
                        for (int k = 0; k < check.Items.Count; k++)
                        {
                            if (check.Items[k].Selected == true)
                            {
                                answer2 += check.Items[k].Text + "|";
                            }
                        }
                        answer2 = answer2.Remove(answer2.Length - 1, 1);
                        restCon.updateResult(questionID, surveyID, responseID, answer2);
                    }
                }

            }

            Response.Redirect("ViewResponse.aspx?sid=" + Request.QueryString["sid"] + "&id=" + Request.QueryString["id"]);  

        }

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewResponse.aspx?sid=" + Request.QueryString["sid"] + "&id=" + Request.QueryString["id"]);  
        }
    }
}