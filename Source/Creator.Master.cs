﻿using SofaDev.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SofaDev
{
    public partial class Creator : System.Web.UI.MasterPage
    {
        AccountController ac = (AccountController)System.Web.HttpContext.Current.Session["account"];

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((ac == null) || (ac.getAccType() == AccountType.Admin))
                Response.Redirect("Default.aspx?error=0");
            Label1.ForeColor = System.Drawing.Color.Red;
            if (ac != null)
                Label1.Text = "Welcome " + ac.getName();
        }
    }
}