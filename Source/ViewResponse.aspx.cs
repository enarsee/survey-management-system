﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;

namespace SofaDev
{
    public partial class ViewResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            deleteButton.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this response?')");
            //ResultController rc = new ResultController();
            
        }

        protected void Page_Init(object sender, EventArgs e)
        {

            if (Page.IsPostBack && (Session["control"] != null))
            {
                PlaceHolder place = (PlaceHolder)Session["control"];
                GeneratePlaceHolder(place);
            }
            else
            {
                int surveyID = int.Parse(Request.QueryString["sid"]);
                SurveyQuestionController surveyQuestion = new SurveyQuestionController();
                PlaceHolder place = new PlaceHolder();
                place = surveyQuestion.getAllQuestionID(surveyID, 2);
                Session["control"] = place;
                GeneratePlaceHolder(place);


                int responseID = int.Parse(Request.QueryString["id"]);
                ResponseController resCon = new ResponseController();
                List<int> allTypes = new List<int>();
                List<string> allAnswers = new List<string>();
                List<int> allQuestionID = new List<int>();
                List<string> allQuestionOptions = new List<string>();

                allTypes = (List<int>)resCon.getQuestionTypes(surveyID);
                allAnswers = (List<string>)resCon.getQuestionAnswers(responseID);
                allQuestionID = (List<int>)resCon.getAllQuestionIDBySurvey(surveyID);

                for (int i = 1; i < allTypes.Count + 1; i++)
                {
                    int type = allTypes[i - 1];
                    int questionID = allQuestionID[i - 1];
                    String compare = allAnswers[i - 1];
                    int finalOption = 0;

                    if (type == 1)
                    {
                        TextBox text = (TextBox)PlaceHolder1.FindControl("txtBox" + questionID) as TextBox;
                        text.Text = allAnswers[i - 1];
                    }
                    else if (type == 2)
                    {
                        TextBox textArea = (TextBox)PlaceHolder1.FindControl("textArea" + questionID) as TextBox;
                        textArea.Text = allAnswers[i - 1];
                    }
                    else if (type == 3)
                    {
                        RadioButtonList radio = (RadioButtonList)PlaceHolder1.FindControl("radBtnLst" + questionID) as RadioButtonList;
                        allQuestionOptions = resCon.getQuestionOptions(questionID);
                        finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compare);
                        radio.SelectedIndex = finalOption;
                    }
                    else if (type == 4)
                    {
                        DropDownList ddl = (DropDownList)PlaceHolder1.FindControl("ddl" + questionID) as DropDownList;
                        allQuestionOptions = resCon.getQuestionOptions(questionID);
                        finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compare);
                        ddl.SelectedIndex = finalOption;
                    }
                    else if (type == 5)
                    {
                        CheckBoxList check = (CheckBoxList)PlaceHolder1.FindControl("boxList" + questionID) as CheckBoxList;
                        allQuestionOptions = resCon.getQuestionOptions(questionID);

                        if (compare.Contains('|') == true)
                        {
                            string[] compareList = compare.Split('|');
                            for (int k = 0; k < compareList.Length; k++)
                            {
                                finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compareList[k]);
                                check.Items[finalOption].Selected = true;
                            }
                        }
                        else
                        {
                            finalOption = resCon.checkWhatIsTheCorrectOption(allQuestionOptions, compare);
                            check.SelectedIndex = finalOption;
                            //check.Items.SelectedIndex = finalOption;
                        }

                    }

                }
            }
        }

             private void GeneratePlaceHolder(PlaceHolder holder)
        {
            int count = holder.Controls.Count;
            for (int i = 1; i <= count; i++)
            {
                Control c = holder.Controls[0];
                PlaceHolder1.Controls.Add(c);
                holder = (PlaceHolder)Session["control"];
            }
            if (count == 0)
            {
                Label error = new Label();
                HyperLink admin = new HyperLink();
                holder.Controls.Add(new LiteralControl("<br />"));
                error.Text = "\tThere does not exist any question for this Survey. Are you sure you followed a right link?<br/>";
                error.Text += "If you believe you have followed a right link, please contact our admin @ ";
                admin.Text = "Admin";
                admin.NavigateUrl = "http://www.google.com.sg";
                PlaceHolder1.Controls.Add(error);
                PlaceHolder1.Controls.Add(admin);
            }
            Session["control"] = PlaceHolder1;
            
        }
        
        protected void deleteButton_Click(object sender, EventArgs e)
        {
            ResultController rc = new ResultController();
            rc.deleteResult(Convert.ToInt32(Request.QueryString["id"]));
            ResponseController rc1 = new ResponseController();
            rc1.deleteResponse(Convert.ToInt32(Request.QueryString["id"]));
            Response.Redirect("ViewAllResponse.aspx?sid=" +Request.QueryString["sid"] );
        }

        protected void editButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("EditResponse.aspx?sid=" + Request.QueryString["sid"] + "&id=" + Request.QueryString["id"]);
        }

        protected void ViewAllResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewAllResponse.aspx?sid=" + Request.QueryString["sid"] + "&id=" + Request.QueryString["id"]);
        }

        
    }
}