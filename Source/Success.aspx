﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="Success.aspx.cs" Inherits="SofaDev.Success" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
   
    <asp:Label ID="lblMsg" runat="server" Text="Thanks for completing this survey" style="-align:middle; text-align:center;"></asp:Label>
    <br />
    <asp:Label ID="lblMsg2" runat="server" Text="Now create your own—it's free, quick &amp; easy!" style="vertical-align:middle; text-align:center;"></asp:Label>
    <br />
    <asp:Button ID="btnRegister" runat="server" OnClick="btnRegister_Click" Text="Sign Up FREE &gt;&gt;" style="vertical-align:middle; text-align:center;" />
     
</asp:Content>
