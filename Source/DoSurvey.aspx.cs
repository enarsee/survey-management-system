﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;
using SofaDev.Controller;
using WorldDomination.Net;
using System.Net;

namespace SofaDev
{
    public partial class DoSurvey1 : System.Web.UI.Page
    {
        private DateTime startDate;

       
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Page.IsPostBack && (Session["control"] != null))
            {
                PlaceHolder place = (PlaceHolder)Session["control"];
                GeneratePlaceHolder(place);
            }
            else
            {
                int surveyID = int.Parse(Request.QueryString["sid"]);
                SurveyQuestionController surveyQuestion = new SurveyQuestionController();
                PlaceHolder place = new PlaceHolder();
                place = surveyQuestion.getAllQuestionID(surveyID, 2);
                Session["control"] = place;
                GeneratePlaceHolder(place);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            startDate = DateTime.Now;
        }
        private string GetCountry()
        {
            IPAddress ipAddress;
            string country = "Singapore";
            if (IPAddress.TryParse(Request.UserHostAddress, out ipAddress))
            {
                //country = ipAddress.Country(); // return value: UNITED STATES
            }
            else
            {
                country = "Invalid IP Address. Unable to find country";
            }
            return country;
        }
        private void GeneratePlaceHolder(PlaceHolder holder)
        {
            int count = holder.Controls.Count;
            for (int i = 1; i <= count; i++)
            {
                Control c = holder.Controls[0];
                
                PlaceHolder1.Controls.Add(c);
                
                holder = (PlaceHolder)Session["control"];
            }
            if (count == 0)
            {
                Label error = new Label();
                HyperLink admin = new HyperLink();
                holder.Controls.Add(new LiteralControl("<br />"));
                error.Text = "\tThere does not exist any question for this Survey. Are you sure you followed a right link?<br/>";
                admin.NavigateUrl = "Default.aspx";
                admin.Text = "Click here to home page";
                btnSubmit.Visible = false;  
                PlaceHolder1.Controls.Add(error);
                PlaceHolder1.Controls.Add(admin);
            }
            else
            {
                btnSubmit.Visible = true;
            }
            Session["control"] = PlaceHolder1;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string ipAddr = Request.UserHostAddress.ToString();
            string browser = Request.Browser.Browser.ToString();
            int surveyID = int.Parse(Request.QueryString["sid"]);
            ResponseController resControl = new ResponseController(GetCountry(), browser, startDate, DateTime.Now, ipAddr);
            SurveyQuestionController sqControl;
            ArrayList type;
            for (int i = 0; i < PlaceHolder1.Controls.Count; i++)
            {
                Control c = PlaceHolder1.Controls[i];
                if (c.GetType().ToString().Equals("System.Web.UI.WebControls.HiddenField"))
                {
                    HiddenField hide =(HiddenField) c;
                    type= new ArrayList();
                    type=GetIDType(hide);
                    String temp = type[2].ToString();   //this is to check for no checkbox entry
                    if (temp.Equals(""))
                    {
                        return;
                    }
                    else
                    {
                        sqControl = new SurveyQuestionController(surveyID, type[0].ToString(), type[2].ToString(), resControl.getResponseID());
                    }
                }
                
            }
            
            Response.Redirect("Success.aspx");
        }
        private ArrayList GetIDType(HiddenField hide)
        {
            ArrayList list=new ArrayList();
            string typeText="";
            string[] hiding = hide.Value.Split('_');
            list.Add(hiding[0]);//QuestionID
            list.Add(hiding[1]);//QuestionType
            switch (int.Parse(hiding[1]))
            {
                case 1:
                    TextBox txtBox = (TextBox)PlaceHolder1.FindControl("txtBox" + hiding[0]);
                    typeText=txtBox.Text;
                    list.Add(typeText);
                    break;
                case 2:
                    TextBox textarea = (TextBox)PlaceHolder1.FindControl("txtArea" + hiding[0]);
                    typeText = textarea.Text;
                    list.Add(typeText);
                    break;
                case 3:
                    RadioButtonList radBtn =(RadioButtonList)PlaceHolder1.FindControl("radBtnLst"+hiding[0]);
                    typeText = radBtn.SelectedItem.Text;
                    list.Add(typeText);
                    
                    break;
                case 4:
                    DropDownList ddlList =(DropDownList)PlaceHolder1.FindControl("ddl"+hiding[0]);
                    typeText = ddlList.SelectedItem.Text;
                    list.Add(typeText);
                    break;
                case 5:
                    CheckBoxList chkBoxLst =(CheckBoxList)PlaceHolder1.FindControl("boxList"+hiding[0]);
                    Label temp = (Label)PlaceHolder1.FindControl("error" + hiding[0]) as Label;
                    if (chkBoxLst.SelectedIndex == -1)
                    {
                        temp.Visible = true;
                        list.Add(typeText);
                        break;
                    }
                    else
                    {
                        temp.Visible = false;
                        for (int i = 0; i < chkBoxLst.Items.Count; i++)
                        {
                            if (chkBoxLst.Items[i].Selected == true)
                            {
                                typeText += chkBoxLst.Items[i].Text + "|";
                            }
                        }
                        typeText = typeText.Remove(typeText.Length - 1, 1);
                        list.Add(typeText);
                        break;
                    }
            }
            return list;
        }
    }
}