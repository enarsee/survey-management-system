﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SofaDev.Controller;

namespace SofaDev
{
    public partial class ViewSurvey1 : System.Web.UI.Page
    {

        AccountController ac1;
        protected override void OnPreInit(EventArgs e)
        {
            ac1 = (AccountController)Session["account"];
            if (ac1 == null) // Switches to public masterpage
                this.Page.MasterPageFile = "~/Public.master";
            else if (ac1.getAccType() == AccountType.Admin) // Switches to admin masterpage
                this.Page.MasterPageFile = "~/Admin.master";
            else if (ac1.getAccType() == AccountType.Creator) // Switches to creator masterpage
                this.Page.MasterPageFile = "~/Creator.master";
            else
                this.Page.MasterPageFile = "~/Public.master";
            base.OnPreInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Page.IsPostBack && (Session["control"] != null))
            {
                PlaceHolder place = (PlaceHolder)Session["control"];
                GeneratePlaceHolder(place);
            }
            else
            {
                int surveyID = int.Parse(Request.QueryString["id"]);
                SurveyQuestionController surveyQuestion = new SurveyQuestionController();
                PlaceHolder place = new PlaceHolder();
                place = surveyQuestion.getAllQuestionID(surveyID, 1);
                Session["control"] = place;
                GeneratePlaceHolder(place);

                if (ac1.getAccType() == AccountType.Admin)
                {
                    DeleteSurvey.Visible = true;
                }
                else if (ac1.getAccType() == AccountType.Creator)
                {
                    DeleteSurvey.Visible = false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            DeleteSurvey.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this suvey?')");
            InitializeButtonEvent();
                       
        }
        private void InitializeButtonEvent()
        {
            for (int i = 0; i < PlaceHolder1.Controls.Count; i++)
            {
                Control c = PlaceHolder1.Controls[i];
                if (c.GetType().ToString().Equals("System.Web.UI.WebControls.Button"))
                {
                    Button newbutton = new Button();
                    newbutton = (Button)c;
                    newbutton.Click += new EventHandler(btnSubmit_Click);
                    newbutton.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this suvey?')");
                }
            }
        }
        private void GeneratePlaceHolder(PlaceHolder holder)
        {
            int count = holder.Controls.Count;
            for (int i = 1; i <= count; i++)
            {
                Control c = holder.Controls[0];
                PlaceHolder1.Controls.Add(c);
                holder = (PlaceHolder)Session["control"];
            }
            if (count == 0)
            {
                Label error = new Label();
                HyperLink admin = new HyperLink();
                holder.Controls.Add(new LiteralControl("<br />"));
                error.Text = "\tThere does not exist any question for this Survey. Are you sure you followed a right link?<br/>";
                error.Text += "If you believe you have followed a right link, please contact our admin @ ";
                admin.Text = "Admin";
                admin.NavigateUrl = "http://www.google.com.sg";
                PlaceHolder1.Controls.Add(error);
                PlaceHolder1.Controls.Add(admin);
            }
            Session["control"] = PlaceHolder1;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //delete indidvual question
            
            int surveyID = int.Parse(Request.QueryString["id"]);
            int hiddenFieldQuestion = 0;
            Button clicked = (Button)sender;

            for(int i=0;i<PlaceHolder1.Controls.Count;i++)
            {
                Control c = PlaceHolder1.Controls[i];
                if(c.ID == clicked.ID)
                {
                    HiddenField hidden = (HiddenField) PlaceHolder1.Controls[i+1];
                    hiddenFieldQuestion = int.Parse(hidden.Value.Split('_')[0].ToString());
                    break;
                }
            }

            SurveyQuestionController sqc = new SurveyQuestionController();
            sqc.deleteSurveyQuestion(surveyID, hiddenFieldQuestion);

            Response.Redirect("ViewSurvey.aspx?id=" + Request.QueryString["id"]);
        }

        protected void DeleteSurvey_Click(object sender, EventArgs e)
        {
            

            int surveyID = int.Parse(Request.QueryString["id"]);
            SurveyController sc = new SurveyController();
            sc.DeleteSurvey(surveyID);
            Response.Redirect("ViewAllSurvey.aspx");
        }

        protected void viewAllResponseBtn_Click(object sender, EventArgs e)
        {
            int surveyID = int.Parse(Request.QueryString["id"]);
            Response.Redirect("ViewAllResponse.aspx?sid= " + surveyID);
        }

        protected void responseSummaryBtn_Click(object sender, EventArgs e)
        {
            int surveyID = int.Parse(Request.QueryString["id"]);
            Response.Redirect("ResponseSummary.aspx?sid= " + surveyID);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewAllSurvey.aspx"); 
        }

        
    }
}