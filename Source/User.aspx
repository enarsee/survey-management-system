﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="SofaDev.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        height: 20px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    Edit Profile
<br />
<Table>

 <tr>
     <td>E-mail: </td>
     <td><asp:TextBox ID="TextBox6" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox6" ErrorMessage="Enter Email" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
         <br />
         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox6" ErrorMessage="Please enter a valid email" Font-Size="Smaller" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
     </td>
   </tr>
     <tr>
     <td>Name: </td>
     <td><asp:TextBox ID="TextBox5" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox5" ErrorMessage="Enter Name" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
         </td>
   </tr>
   <tr>
     <td>Address: </td>
     <td><asp:TextBox ID="TextBox4" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox4" ErrorMessage="Enter Address" Font-Size="Smaller" ForeColor="Red"></asp:RequiredFieldValidator>
       </td>
   </tr>
    <tr>
     <td>Gender: </td>
     <td>
         <asp:RadioButtonList ID="RadioButtonList1" runat="server">
         </asp:RadioButtonList>
        </td>
   </tr>
    <tr>
     <td>Contact: </td>
     <td><asp:TextBox ID="TextBox2" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Font-Size="Smaller" ControlToValidate="TextBox2" ErrorMessage="Enter Contact" ForeColor="Red"></asp:RequiredFieldValidator>
         <br />
         <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Font-Size="Smaller" ControlToValidate="TextBox2" ErrorMessage="Enter numbers only" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
        </td>
   </tr>
    <tr>
     <td>Age: </td>
     <td><asp:TextBox ID="TextBox1" runat="server" TextMode="SingleLine"></asp:TextBox>
         <br />
         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Font-Size="Smaller" ControlToValidate="TextBox2" ErrorMessage="Enter Age" ForeColor="Red"></asp:RequiredFieldValidator>
         <br />
         <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Font-Size="Smaller" ControlToValidate="TextBox1" ErrorMessage="Enter numbers only" ForeColor="Red" ValidationExpression="\d+"></asp:RegularExpressionValidator>
        </td>
   </tr>
    <tr>
     <td>Password: </td>
     <td><asp:TextBox ID="TextBox7" runat="server" TextMode="Password"></asp:TextBox>
         <br />
        </td>
   </tr>
    <tr>
        <td colspan="2" class="auto-style1"><asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td colspan="2"><asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Edit" /></td>
    </tr>
</Table>
   
</asp:Content>
